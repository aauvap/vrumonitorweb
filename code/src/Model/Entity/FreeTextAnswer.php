<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FreeTextAnswer Entity.
 *
 * @property int $id
 * @property int $incident_id
 * @property \App\Model\Entity\Incident $incident
 * @property int $free_text_field_id
 * @property \App\Model\Entity\FreeTextField $free_text_field
 * @property string $text
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class FreeTextAnswer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
