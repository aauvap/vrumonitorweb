<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SequenceCondition Entity.
 *
 * @property int $id
 * @property int $question_sequence_id
 * @property \App\Model\Entity\QuestionSequence $question_sequence
 * @property int $multiple_choice_option_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\MultipleChoiceOption $multiple_choice_option
 */
class SequenceCondition extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'multiple_choice_options_id' => false,
    ];
}
