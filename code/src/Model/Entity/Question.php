<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Question Entity.
 *
 * @property int $id
 * @property int $questionnaire_id
 * @property \App\Model\Entity\Questionnaire $questionnaire
 * @property string $question
 * @property int $min_num_choices
 * @property int $max_num_choices
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Question extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'questionnaire_id' => false,
    ];

    public function NextQuestionId($incidentId) {
        $sequences = TableRegistry::get('QuestionSequences');
        $q = $sequences->find()
                       ->contain(['SequenceConditions'])
                       ->where(['question_id' => $this->id])
                       ->order(['priority' => 'ASC']);

        $answers = TableRegistry::get('MultipleChoiceAnswers');
        foreach($q as $seq) {
            $or_condition = null;
            $passes = array();
            foreach($seq->sequence_conditions as $cond) {
                $or_condition = $cond->or_condition; // Setting this so we know for later.
                $conditionMatch = $answers->find()->where(['incident_id' => $incidentId, 'multiple_choice_option_id' => $cond->multiple_choice_option_id])->count();
                
                if($conditionMatch > 0) {
                    $passes[] = true;
                } else {
                    $passes[] = false;
                }
            }
            if(($or_condition and in_array(true, $passes, true)) or
                (!$or_condition and !in_array(false, $passes, true))) {
                return $seq->to_question_id;
            }
        }
        return null;
    }
}
