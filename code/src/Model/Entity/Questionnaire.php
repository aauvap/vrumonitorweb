<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Questionnaire Entity.
 *
 * @property int $id
 * @property string $title
 * @property string $intro_text
 * @property string $end_text
 * @property string $language
 * @property bool $restrict_access
 * @property bool $restrict_incident_creation
 * @property bool $auto_create_participants
 * @property int $email_field
 * @property int $target_questionnaire
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Question[] $questions
 */
class Questionnaire extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
    
    protected function _getFirstQuestionId() {
        $questions = TableRegistry::get('Questions');
        $q = $questions->find()
             ->select(['id'])
             ->where(['questionnaire_id' => $this->id])
             ->order(['priority' => 'ASC'])
                       ->limit(1);
        if($q->count() == 0) {
            return null;
        } else {
            return $q->first()->id;
        }
    }
}
