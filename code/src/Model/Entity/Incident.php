<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Incident Entity.
 *
 * @property int $id
 * @property int $participant_id
 * @property \App\Model\Entity\Participant $participant
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\DateAnswer[] $date_answers
 * @property \App\Model\Entity\FreeTextAnswer[] $free_text_answers
 * @property \App\Model\Entity\MapAnswer[] $map_answers
 * @property \App\Model\Entity\MultipleChoiceAnswer[] $multiple_choice_answers
 * @property \App\Model\Entity\NumberAnswer[] $number_answers
 * @property \App\Model\Entity\SensorDump[] $sensor_dumps
 */
class Incident extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
