<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NumberAnswer Entity.
 *
 * @property int $id
 * @property int $incident_id
 * @property \App\Model\Entity\Incident $incident
 * @property int $number_field_id
 * @property \App\Model\Entity\NumberField $number_field
 * @property int $number
 * @property float $float_number
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class NumberAnswer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
