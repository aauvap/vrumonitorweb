<?php
namespace App\Model\Table;

use App\Model\Entity\MultipleChoiceOption;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MultipleChoiceOptions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Questions
 */
class MultipleChoiceOptionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('multiple_choice_options');
        $this->displayField('id');
        $this->primaryKey(['id']);

        $this->addBehavior('Timestamp');

        $this->belongsTo('MultipleChoiceFields', [
            'foreignKey' => 'multiple_choice_field_id',
            'joinType' => 'INNER'
        ]);

        $this->addBehavior('Proffer.Proffer', [
            'picture' => [    // The name of your upload field
                'dir' => 'picture_dir',   // The name of the field to store the folder
                'thumbnailSizes' => [ // Declare your thumbnails
                    'square' => ['w' => 200, 'h' => 200],
                    'smallSquare' => ['w' => 20, 'h' => 20]
                ],
            //'thumbnailMethod' => 'imagick'  // Options are Imagick, Gd or Gmagick
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('caption');

        $validator->provider('proffer', 'Proffer\Model\Validation\ProfferRules');
        $validator->add('picture', 'proffer', [
            'rule' => ['extension', ['jpg', 'jpeg', 'png']],
            'message' => 'Invalid extension',
            'provider' => 'proffer'
        ]);
        $validator
            ->allowEmpty('picture');

        $validator
            ->add('priority', 'valid', ['rule' => 'numeric'])
            ->requirePresence('priority', 'create')
            ->notEmpty('priority');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['multiple_choice_field_id'], 'MultipleChoiceFields'));
        return $rules;
    }
}
