<?php
namespace App\Model\Table;

use App\Model\Entity\NumberField;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NumberFields Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Questions
 */
class NumberFieldsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('number_fields');
        $this->displayField('id');
        $this->primaryKey(['id']);

        $this->addBehavior('Timestamp');

        $this->belongsTo('Questions', [
            'foreignKey' => 'question_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('caption');

        $validator
            ->add('min_number', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('min_number');

        $validator
            ->add('max_number', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('max_number');

        $validator
            ->add('allow_floats', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('allow_floats');

        $validator
            ->add('priority', 'valid', ['rule' => 'numeric'])
            ->requirePresence('priority', 'create')
            ->notEmpty('priority');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['question_id'], 'Questions'));
        return $rules;
    }
}
