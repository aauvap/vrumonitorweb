<?php
namespace App\Model\Table;

use App\Model\Entity\MapAnswer;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MapAnswers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Incidents
 * @property \Cake\ORM\Association\BelongsTo $MapFields
 */
class MapAnswersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('map_answers');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Incidents', [
            'foreignKey' => 'incident_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('MapFields', [
            'foreignKey' => 'map_field_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('latitude', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('latitude');

        $validator
            ->add('longitude', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('longitude');

        $validator
            ->add('orientation', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('orientation');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['incident_id'], 'Incidents'));
        $rules->add($rules->existsIn(['map_field_id'], 'MapFields'));
        return $rules;
    }
}
