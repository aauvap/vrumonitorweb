<?php
namespace App\Model\Table;

use App\Model\Entity\Questionnaire;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Questionnaires Model
 *
 * @property \Cake\ORM\Association\HasMany $Incidents
 * @property \Cake\ORM\Association\HasMany $Participants
 * @property \Cake\ORM\Association\HasMany $Questions
 */
class QuestionnairesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('questionnaires');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Incidents', [
            'foreignKey' => 'questionnaire_id'
        ]);
        $this->hasMany('Participants', [
            'foreignKey' => 'questionnaire_id'
        ]);
        $this->hasMany('Questions', [
            'foreignKey' => 'questionnaire_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('intro_text');

        $validator
            ->allowEmpty('end_text');

        $validator
            ->allowEmpty('language');

        $validator
            ->add('restrict_access', 'valid', ['rule' => 'boolean'])
            ->requirePresence('restrict_access', 'create')
            ->notEmpty('restrict_access');

        $validator
            ->add('restrict_incident_creation', 'valid', ['rule' => 'boolean'])
            ->requirePresence('restrict_incident_creation', 'create')
            ->notEmpty('restrict_incident_creation');
/*
        $validator
            ->add('auto_create_participants', 'valid', ['rule' => 'boolean'])
            ->requirePresence('auto_create_participants', 'create')
            ->notEmpty('auto_create_participants');

        $validator
            ->add('email_field', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('email_field');

        $validator
            ->add('target_questionnaire', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('target_questionnaire');

        */
        return $validator;
    }
}
