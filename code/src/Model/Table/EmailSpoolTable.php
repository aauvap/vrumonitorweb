<?php
namespace App\Model\Table;

use App\Model\Entity\EmailSpool;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EmailSpool Model
 *
 */
class EmailSpoolTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('email_spool');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('recipient', 'create')
            ->notEmpty('recipient');

        $validator
            ->requirePresence('sender_address', 'create')
            ->notEmpty('sender_address');

        $validator
            ->requirePresence('sender_name', 'create')
            ->notEmpty('sender_name');

        $validator
            ->requirePresence('subject', 'create')
            ->notEmpty('subject');

        $validator
            ->requirePresence('message', 'create')
            ->notEmpty('message');

        $validator
            ->add('sent', 'valid', ['rule' => 'boolean'])
            ->requirePresence('sent', 'create')
            ->notEmpty('sent');

        return $validator;
    }
}
