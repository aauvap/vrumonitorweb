<?php
namespace App\Model\Table;

use App\Model\Entity\SequenceCondition;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SequenceConditions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $QuestionSequences
 * @property \Cake\ORM\Association\BelongsTo $MultipleChoiceOptions
 */
class SequenceConditionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sequence_conditions');
        $this->displayField('id');
        $this->primaryKey(['id']);

        $this->addBehavior('Timestamp');

        $this->belongsTo('QuestionSequences', [
            'foreignKey' => 'question_sequence_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('MultipleChoiceOptions', [
            'foreignKey' => 'multiple_choice_option_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['question_sequence_id'], 'QuestionSequences'));
        $rules->add($rules->existsIn(['multiple_choice_option_id'], 'MultipleChoiceOptions'));
        return $rules;
    }
}
