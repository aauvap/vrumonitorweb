<?php
namespace App\Model\Table;

use App\Model\Entity\Incident;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Incidents Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Participants
 * @property \Cake\ORM\Association\HasMany $DateAnswers
 * @property \Cake\ORM\Association\HasMany $FreeTextAnswers
 * @property \Cake\ORM\Association\HasMany $MapAnswers
 * @property \Cake\ORM\Association\HasMany $MultipleChoiceAnswers
 * @property \Cake\ORM\Association\HasMany $NumberAnswers
 * @property \Cake\ORM\Association\HasMany $SensorDumps
 */
class IncidentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('incidents');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Participants', [
            'foreignKey' => 'participant_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Questionnaires', [
            'foreignKey' => 'questionnaire_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('DateAnswers', [
            'foreignKey' => 'incident_id'
        ]);
        $this->hasMany('FreeTextAnswers', [
            'foreignKey' => 'incident_id'
        ]);
        $this->hasMany('MapAnswers', [
            'foreignKey' => 'incident_id'
        ]);
        $this->hasMany('MultipleChoiceAnswers', [
            'foreignKey' => 'incident_id'
        ]);
        $this->hasMany('NumberAnswers', [
            'foreignKey' => 'incident_id'
        ]);
        $this->hasMany('SensorDumps', [
            'foreignKey' => 'incident_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['participant_id'], 'Participants'));
        return $rules;
    }
}
