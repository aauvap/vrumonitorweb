<?php
namespace App\Model\Table;

use App\Model\Entity\DateAnswer;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DateAnswers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Incidents
 * @property \Cake\ORM\Association\BelongsTo $DateFields
 */
class DateAnswersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('date_answers');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Incidents', [
            'foreignKey' => 'incident_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('DateFields', [
            'foreignKey' => 'date_field_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('datetime', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('datetime');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['incident_id'], 'Incidents'));
        $rules->add($rules->existsIn(['date_field_id'], 'DateFields'));
        return $rules;
    }
}
