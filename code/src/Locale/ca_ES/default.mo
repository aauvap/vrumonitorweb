��          �      <      �     �     �  +   �      �  -     $   J  1   o  1   �     �  L   �  (   6  �   _  -   �          2  0   J  .   {  P  �     �          "     ;  .   P  "     3   �  3   �     
  G   (     p  �   �  "   $     G     a  !   z  ,   �                                           	                                               
           Dont know date Dont know time Error. You can check at most %d checkboxes. Error. You can only set %d marks Error. You must check at least %d checkboxes. Error. You must set at least %d mark Error. Your answer in %s must be greater than %s. Error. Your answer in %s must be smaller than %s. Please write a number The link you used has already been used before. Sorry for the inconvenience. The text field %d must be a valid email. This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 635895 You have answered this questionnaire already. You must give full date You must give full time Your answer must be at least %d characters long. Your answer must have less than %d characters. Project-Id-Version: PROJECT VERSION
POT-Creation-Date: 2016-09-30 15:45+0200
PO-Revision-Date: 2016-10-06 09:53+0200
Last-Translator: 
Language-Team: EMAIL@ADDRESS
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.9
 No recordo la data No recordo l’hora Escolliu %d - %d opcions Massa localitzacions Si us plau, responeu la pregunta per continuar La localització no s'ha establert Si us plau, seleccioneu un nombre entre %.0f i %.0f Si us plau, seleccioneu un nombre entre %.0f i %.0f Si us plau, esciviu un nombre L'enllaç ha estat utilitzat anteriorment. Disculpa pels inconvenients. Correu electrònic no vàlid  El projecte ha rebut finançament del programa de recerca i innovació Horizon 2020 de la Unió Europea en virtut d\'acord de subvenció núm 635.895 Ja has respost aquest qüestionari La data no s'ha establert L'hora no s'ha establert La resposta ha de ser més llarga La resposta excedeix el límit de caràcters 