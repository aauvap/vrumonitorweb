��          �      L      �     �     �  ?   �  '         G  -   h  $   �  1   �  1   �       L   5  (   �  �   �  -   8     f     ~  0   �  .   �  P  �     G     X  5   n     �     �  (   �                 2     S  ;   `     �  �   �  +   =     i     y     �     �                                            
                                       	                 Dont know date Dont know time Error. You answer in %s must be identical to your answer in %s. Error. You can check between %d and %d. Error. You can only set %d marks Error. You must check at least %d checkboxes. Error. You must set at least %d mark Error. Your answer in %s must be greater than %s. Error. Your answer in %s must be smaller than %s. Please write a number The link you used has already been used before. Sorry for the inconvenience. The text field %d must be a valid email. This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 635895 You have answered this questionnaire already. You must give full date You must give full time Your answer must be at least %d characters long. Your answer must have less than %d characters. Project-Id-Version: PROJECT VERSION
POT-Creation-Date: 2016-09-30 16:55+0200
PO-Revision-Date: 2016-10-06 09:55+0200
Last-Translator: 
Language-Team: EMAIL@ADDRESS
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.9
 Husker ikke dato Husker ikke tidspunkt Dit svar er forskelligt fra din tidligere indtastning Vælg %d - %d svarmuligheder For mange steder valgt Besvar spørgsmålet for at komme videre Sted ikke valgt Vælg et tal mellem %.0f og %.0f Vælg et tal mellem %.0f og %.0f Skriv et tal Linket er tidligere blevet brugt. Vi beklager ulejligheden. Ugyldig e-mail-adresse Dette projekt har modtaget støtte fra Den Europæiske Unions HORIZON2020 forsknings- og innovationsprogram under bevillingsnummer 635895 Du har allerede besvaret dette spørgeskema Dato ikke valgt Klokkeslæt ikke valgt Svaret skal være længere Svaret er for langt 