��          �      <      �     �     �  '   �      �  -     $   F  1   k  1   �     �  L   �  (   2  �   [  -   �          .  0   F  .   w  P  �     �          #  "   <  -   _     �  "   �  "   �     �  G   �     @  s   U     �     �                ;                                            	                                              
           Dont know date Dont know time Error. You can check between %d and %d. Error. You can only set %d marks Error. You must check at least %d checkboxes. Error. You must set at least %d mark Error. Your answer in %s must be greater than %s. Error. Your answer in %s must be smaller than %s. Please write a number The link you used has already been used before. Sorry for the inconvenience. The text field %d must be a valid email. This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 635895 You have answered this questionnaire already. You must give full date You must give full time Your answer must be at least %d characters long. Your answer must have less than %d characters. Project-Id-Version: PROJECT VERSION
POT-Creation-Date: 2016-09-30 17:11+0200
PO-Revision-Date: 2016-10-06 09:53+0200
Last-Translator: 
Language-Team: EMAIL@ADDRESS
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.9
 Jag minns inte datumet Jag minns inte tiden Välj %d - %d alternativ För många platser har indikerats Vänligen besvara frågan för att fortsätta Platsen har inte valts Välj ett tal mellan %.of och %.0f Välj ett tal mellan %.of och %.0f Skriv ett tal Länken du har använt användes redan tidigare. Beklagar olägenheten. Ogiltig e-postadress Projektet har fått stöd från EU: s HORIZON2020 forsknings- och innovationsprogram enligt bidragsavtal nr 635.895 Du har redan besvarat enkäten Datumet har inte ställts in Tiden har inte ställts in Svaret måste vara längre Svaret är för långt 