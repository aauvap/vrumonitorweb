��          �      <      �     �     �  +   �      �  -     $   J  1   o  1   �     �  L   �  (   6  �   _  -   �          2  0   J  .   {  I  �     �          .     E  ,   `     �  *   �  *   �       >        Y  �   n  (        5  !   T     v     �                                           	                                               
           Dont know date Dont know time Error. You can check at most %d checkboxes. Error. You can only set %d marks Error. You must check at least %d checkboxes. Error. You must set at least %d mark Error. Your answer in %s must be greater than %s. Error. Your answer in %s must be smaller than %s. Please write a number The link you used has already been used before. Sorry for the inconvenience. The text field %d must be a valid email. This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 635895 You have answered this questionnaire already. You must give full date You must give full time Your answer must be at least %d characters long. Your answer must have less than %d characters. Project-Id-Version: 
POT-Creation-Date: 2016-09-30 15:50+0200
PO-Revision-Date: 2016-10-06 09:55+0200
Last-Translator: 
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.9
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
 Ik weet de datum niet meer Ik weet het tijdstip niet meer Kies %d - %d antworden Te veel locaties aangeduid Beantwoord a.u.b. de vraag om verder te gaan Locatie is nog niet ingesteld Kies a.u.b. een nummer tussen %.0f en %.0f Kies a.u.b. een nummer tussen %.0f en %.0f Vul a.u.b. een nummer in Deze link werd reeds gebruikt. Onze excuses voor het ongemak.  Ongeldig e-mailadres Het project heeft financiering ontvangen van het Horizon 2020 onderzoeks- en innovatieprogramma van de Europese Unie onder de subsidieovereenkomst nr. 635895 U hebt deze vragenlijst reeds beantwoord Er werd nog geen datum gekozen Er werd nog geen tijdstip gekozen Antwoord bevat te weinig tekens Antwoord bevat te veel tekens 