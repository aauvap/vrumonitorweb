<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Date Answer'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Date Fields'), ['controller' => 'DateFields', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Date Field'), ['controller' => 'DateFields', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dateAnswers index large-9 medium-8 columns content">
    <h3><?= __('Date Answers') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('incident_id') ?></th>
                <th><?= $this->Paginator->sort('date_field_id') ?></th>
                <th><?= $this->Paginator->sort('date') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dateAnswers as $dateAnswer): ?>
            <tr>
                <td><?= $this->Number->format($dateAnswer->id) ?></td>
                <td><?= $dateAnswer->has('incident') ? $this->Html->link($dateAnswer->incident->id, ['controller' => 'Incidents', 'action' => 'view', $dateAnswer->incident->id]) : '' ?></td>
                <td><?= $dateAnswer->has('date_field') ? $this->Html->link($dateAnswer->date_field->id, ['controller' => 'DateFields', 'action' => 'view', $dateAnswer->date_field->id]) : '' ?></td>
                <td><?= h($dateAnswer->date) ?></td>
                <td><?= h($dateAnswer->created) ?></td>
                <td><?= h($dateAnswer->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dateAnswer->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dateAnswer->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dateAnswer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dateAnswer->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
