<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $dateAnswer->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $dateAnswer->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Date Answers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Date Fields'), ['controller' => 'DateFields', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Date Field'), ['controller' => 'DateFields', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dateAnswers form large-9 medium-8 columns content">
    <?= $this->Form->create($dateAnswer) ?>
    <fieldset>
        <legend><?= __('Edit Date Answer') ?></legend>
        <?php
            echo $this->Form->input('incident_id', ['options' => $incidents]);
            echo $this->Form->input('date_field_id', ['options' => $dateFields]);
            echo $this->Form->input('date', ['empty' => true, 'default' => '']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
