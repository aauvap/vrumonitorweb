<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Date Answer'), ['action' => 'edit', $dateAnswer->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Date Answer'), ['action' => 'delete', $dateAnswer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dateAnswer->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Date Answers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Date Answer'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('List Date Fields'), ['controller' => 'DateFields', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Date Field'), ['controller' => 'DateFields', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dateAnswers view large-9 medium-8 columns content">
    <h3><?= h($dateAnswer->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Incident') ?></th>
            <td><?= $dateAnswer->has('incident') ? $this->Html->link($dateAnswer->incident->id, ['controller' => 'Incidents', 'action' => 'view', $dateAnswer->incident->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Date Field') ?></th>
            <td><?= $dateAnswer->has('date_field') ? $this->Html->link($dateAnswer->date_field->id, ['controller' => 'DateFields', 'action' => 'view', $dateAnswer->date_field->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($dateAnswer->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date') ?></th>
            <td><?= h($dateAnswer->date) ?></tr>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($dateAnswer->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($dateAnswer->modified) ?></tr>
        </tr>
    </table>
</div>
