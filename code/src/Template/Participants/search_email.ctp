<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('<< Back to questionnaire'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="participants index large-9 medium-8 columns content">
    <h3><?= __('Search By Email') ?></h3>

    <?= $this->Form->create() ?>
    <fieldset>
        <?php
            echo $this->Form->input('Email');
        ?>
    <?= $this->Form->button(__('Search')) ?>
    </fieldset>

</div>
