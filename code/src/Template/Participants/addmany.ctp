<script type="text/javascript">
 function newConditionSelector(elem) {
     var currentId = elem.attr('id').replace("participant-condition", "");
     console.log(elem.val());
     if(elem.val() == 0) {
         elem.remove();
     } else {
         var nextId = (parseInt(currentId) + 1);
         elem.after(elem.clone().attr('id', 'participant-condition' + nextId));
     }
     $("input#confirmed").val("0");
     $("p#newParticipants").hide();
     $("form>button").html("<?= __("Next >>") ?>");
 }
</script>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
      <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List participants'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="participants index large-9 medium-8 columns content">
    <h3><?= __('Add participants from public questionnaire') ?></h3>
    <p><?= __('On this page you can generate participants based on replies to any public questionnaire.') ?></p>
    
    <?php
        echo $this->Form->create(null, ['id' => 'testId']);
    ?>
    <fieldset>
      <?php
      if($confirm) {
          echo "<p id='newParticipants'>".sprintf(__("The selected conditions will generate %d new participants."), $numParticipants);
          echo $this->Form->button(__('Confirm creation'));
          echo "</p>";
      }
      echo $this->Form->input('base_questionnaire',
                              ['label' => __('Base questionnaire'),
                               'options' => $publicQuestionnaires,
                               'onchange' =>
                                   'window.location = "'.
                                           $this->Url->build(['controller' => 'participants',
                                                              'action' => 'addmany']).
                                           '/" + $(this).parent().find(\'select option:selected\').val();',
                               'default' => $baseQuestionnaire]);
      
      echo $this->Form->input('email_field', ['label' => __('E-mail field'), 'type' => 'select', 'options' => $emailFields]);
      //echo "Conditions:";
      if(!array_key_exists('participant_conditions', $this->request->data)) {
          // No previous settings. Just produce an empty condition field.
          echo $this->Form->input('participant_condition0',
                                  ['type' => 'select',
                                   'label' => __('Conditions'),
                                   'class' => 'condition',
                                   'name' => 'participant_conditions[]',
                                   'options' => $participantConditions,
                                   'onchange' =>
                                       "newConditionSelector($(this));"]);
      } else {
          // Create condition fields according to those passed by the user.
          $i = 0;
          $label = __('Conditions');
          foreach($this->request->data['participant_conditions'] as $c) {
              echo $this->Form->input('participant_condition'.$i++,
                                      ['type' => 'select',
                                       'label' => $label,
                                       'class' => 'condition',
                                       'name' => 'participant_conditions[]',
                                       'options' => $participantConditions,
                                       'default' => $c,
                                       'onchange' =>
                                           "newConditionSelector($(this));"]);
              $label = ''; // No label on any field but the first.
          }
      }

      echo $this->Form->input('target_questionnaire', ['label' => __('Target questionnaire'), 'options' => $allQuestionnaires]);
      echo "<h4>".__("Activation e-mail")."</h4>";
      echo $this->Form->input('activation_email_subject', ['label' => __('Subject'), 'default' => $sampleSubject]);
      echo $this->Form->input('activation_email_sender_name', ['label' => __('Name of sender'), 'default' => $sampleSenderName]);
      echo $this->Form->input('activation_email_sender_address', ['label' => __('E-mail addresse of sender'), 'default' => $sampleSenderAddress]);
      echo $this->Form->input('activation_email', ['label' => __('Activation e-mail (must include %link% for activation link)'), 'default' => $sampleEmail, 'type' => 'textarea']);
      echo $this->Form->input('confirmed', ['value' => $confirm, 'type' => 'hidden']);
      ?>
    </fieldset>
    <?php
    if($confirm) {
        echo $this->Form->button(__('Confirm creation'));
    } else {
        echo $this->Form->button(__('Next >>'));
    }
    ?>
    <?= $this->Form->end() ?>
</div>
