<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New participant'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Add participants from questionnaire'), ['action' => 'addmany']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('E-mail queue'), ['controller' => 'EmailSpool', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('App settings'), ['controller' => 'AppSettings', 'action' => 'edit']) ?></li>
        <li><?= $this->Html->link(__('Search by Email'), ['controller' => 'participants', 'action' => 'searchEmail']) ?></li>
    </ul>
</nav>
<div class="participants index large-9 medium-8 columns content">
    <h3><?= __('Participants') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('email') ?></th>
                <th><?= $this->Paginator->sort('questionnaire_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
          <?php foreach ($participants as $participant): ?>
            
        
            <tr>
                <td><?= $this->Number->format($participant->id) . ($participant->activated ? "" : " <i>(inactive)</i>   ") ?></td>
                <td><?= h($participant->email) ?></td>
                <td><?= $this->Html->link(__($participant->questionnaire_id.': '.$participant->questionnaire->title),
                                          ['controller' => 'Questionnaires',
                                           'action' => 'edit',
                                           $participant->questionnaire_id]) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $participant->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $participant->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $participant->id], ['confirm' => __('Are you sure you want to delete # {0}?', $participant->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
