<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li> <?=$this->Html->link('<< Back', ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="participants view large-9 medium-8 columns content">
    <h3>Update Keys</h3>
    <p><?= sprintf(__("%d participants were found to be missing keys. Press confirm to create new keys for %d participants"), $participantCount, $participantCount)?></p>
    <?= $this->Form->create() ?>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
