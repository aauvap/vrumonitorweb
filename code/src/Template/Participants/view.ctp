<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Participant'), ['action' => 'edit', $participant->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Participant'), ['action' => 'delete', $participant->id], ['confirm' => __('Are you sure you want to delete # {0}?', $participant->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Participants'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Participant'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="participants view large-9 medium-8 columns content">
    <h3><?= h($participant->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($participant->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Activated') ?></th>
            <td><?= $this->Number->format($participant->activated) ? 'True' : 'False'?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($participant->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Questionnaire') ?></th>
            <td><?= $this->Html->link($participant->questionnaire_id, ['controller' => 'Questionnaires', 'action' => 'edit', $participant->questionnaire_id]) ?></td>
        </tr>
        <tr>
            <th><?= __('Participant key') ?></th>
            <td><?= h($participant->participant_key) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($participant->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($participant->modified) ?></tr>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Incidents') ?></h4>
        <?php if (!empty($participant->incidents)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Questionnaire Link')?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($participant->incidents as $incidents): ?>
            <tr>
                <td><?= h($incidents->id) ?></td>
                <td>
                <?php if(!$incidents->locked) {
                    echo $this->Html->link("Go to questionnaire", 
                        [
                            "controller" => "Questionnaires", 
                            "action" => "view", $incidents->id, 
                            "?" => ['key' => $incidents->incident_key]
                        ]);
                    } else {
                        echo "<i>locked</i>";
                    }
                ?>
                </td>
                <td><?= h($incidents->created) ?></td>
                <td><?= h($incidents->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Incidents', 'action' => 'view', $incidents->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Incidents', 'action' => 'delete', $incidents->id], ['confirm' => __('Are you sure you want to delete # {0}?', $incidents->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
        <?= $this->Html->link("Report incident...", ['controller' => 'Incidents', 'action' => 'add', $participant->id, "?" => ["key" => $participant->participant_key]]) ?>
    </div>
</div>
