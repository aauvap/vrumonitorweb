<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Sensor Dumps'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="sensorDumps form large-9 medium-8 columns content">
    <?= $this->Form->create($sensorDump) ?>
    <fieldset>
        <legend><?= __('Add Sensor Dump') ?></legend>
        <?php
            echo $this->Form->input('data');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
