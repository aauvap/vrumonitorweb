<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sensor Dump'), ['action' => 'edit', $sensorDump->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sensor Dump'), ['action' => 'delete', $sensorDump->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sensorDump->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sensor Dumps'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sensor Dump'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="sensorDumps view large-9 medium-8 columns content">
    <h3><?= h($sensorDump->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Incident') ?></th>
            <td><?= $sensorDump->has('incident') ? $this->Html->link($sensorDump->incident->id, ['controller' => 'Incidents', 'action' => 'view', $sensorDump->incident->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($sensorDump->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($sensorDump->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($sensorDump->modified) ?></tr>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Data') ?></h4>
        <?= $this->Text->autoParagraph(h($sensorDump->data)); ?>
    </div>
</div>
