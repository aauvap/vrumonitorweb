<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sensor Dump'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="sensorDumps index large-9 medium-8 columns content">
    <h3><?= __('Sensor Dumps') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('incident_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sensorDumps as $sensorDump): ?>
            <tr>
                <td><?= $this->Number->format($sensorDump->id) ?></td>
                <td><?= $sensorDump->has('incident') ? $this->Html->link($sensorDump->incident->id, ['controller' => 'Incidents', 'action' => 'view', $sensorDump->incident->id]) : '' ?></td>
                <td><?= h($sensorDump->created) ?></td>
                <td><?= h($sensorDump->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $sensorDump->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sensorDump->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sensorDump->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sensorDump->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
