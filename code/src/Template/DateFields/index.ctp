<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Date Field'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dateFields index large-9 medium-8 columns content">
    <h3><?= __('Date Fields') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('question_id') ?></th>
                <th><?= $this->Paginator->sort('caption') ?></th>
                <th><?= $this->Paginator->sort('min_date') ?></th>
                <th><?= $this->Paginator->sort('max_date') ?></th>
                <th><?= $this->Paginator->sort('priority') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dateFields as $dateField): ?>
            <tr>
                <td><?= $this->Number->format($dateField->id) ?></td>
                <td><?= $dateField->has('question') ? $this->Html->link($dateField->question->id, ['controller' => 'Questions', 'action' => 'view', $dateField->question->id]) : '' ?></td>
                <td><?= h($dateField->caption) ?></td>
                <td><?= h($dateField->min_date) ?></td>
                <td><?= h($dateField->max_date) ?></td>
                <td><?= $this->Number->format($dateField->priority) ?></td>
                <td><?= h($dateField->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dateField->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dateField->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dateField->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dateField->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
