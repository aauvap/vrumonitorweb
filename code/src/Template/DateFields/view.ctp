<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Date Field'), ['action' => 'edit', $dateField->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Date Field'), ['action' => 'delete', $dateField->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dateField->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Date Fields'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Date Field'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dateFields view large-9 medium-8 columns content">
    <h3><?= h($dateField->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Question') ?></th>
            <td><?= $dateField->has('question') ? $this->Html->link($dateField->question->id, ['controller' => 'Questions', 'action' => 'view', $dateField->question->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Caption') ?></th>
            <td><?= h($dateField->caption) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($dateField->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Priority') ?></th>
            <td><?= $this->Number->format($dateField->priority) ?></td>
        </tr>
        <tr>
            <th><?= __('Min Date') ?></th>
            <td><?= h($dateField->min_date) ?></tr>
        </tr>
        <tr>
            <th><?= __('Max Date') ?></th>
            <td><?= h($dateField->max_date) ?></tr>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($dateField->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($dateField->modified) ?></tr>
        </tr>
    </table>
</div>
