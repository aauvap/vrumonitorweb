<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li><?php
          if(!is_null($question_id)) {
              echo "<li>".$this->Html->link(__('Back to question'), ['controller' => 'Questions', 'action' => 'edit', $question_id])."</li>";
          }
       ?>
    </ul>
</nav>
<div class="dateFields form large-9 medium-8 columns content">
    <?= $this->Form->create($field) ?>
    <fieldset>
        <legend><?= __('Add Date Field') ?></legend>
        <?php
            if(is_null($question_id)) {
                echo $this->Form->input('questions', ['label' => __('Question')]);
            }
            echo $this->Form->input('caption');
            echo $this->Form->input('min_date', ['empty' => true, 'default' => '', 'minYear' => 1900]);
            echo $this->Form->input('max_date', ['empty' => true, 'default' => '', 'minYear' => 1900]);
            echo $this->Form->input('allow_time');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
