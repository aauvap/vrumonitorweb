<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Back to question'), ['controller' => 'Questions', 'action' => 'edit', $field->question_id]) ?></li>
    </ul>
</nav>
<div class="dateFields form large-9 medium-8 columns content">
    <?= $this->Form->create($field) ?>
    <fieldset>
        <legend><?= __('Edit Date Field') ?></legend>
        <?php
            echo $this->Form->input('caption');
            echo $this->Form->input('min_date', ['empty' => true, 'default' => '']);
            echo $this->Form->input('max_date', ['empty' => true, 'default' => '']);
            echo $this->Form->input('allow_time');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
