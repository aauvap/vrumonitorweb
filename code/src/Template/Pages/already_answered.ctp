<div class="index large-9 medium-8 columns content">
    <div style="height: 51px;"></div>
    <p><b>DK: </b>Linket er tidligere blevet brugt. Vi beklager ulejligheden.</p>
    <p><b>BE: </b>Deze link werd reeds gebruikt. Onze excuses voor het ongemak.</p>
    <p><b>SE: </b>Länken du har använt användes redan tidigare. Beklagar olägenheten.</p>
    <p><b>CA: </b>L’enllaç s'ha utilitzat abans. Sentim els inconvenients ocasionats.</p>
</div>
