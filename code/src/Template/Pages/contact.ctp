<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
    </ul>
</nav>
<div class="questionnaires index large-9 medium-8 columns content">
    <h3>Contact</h3>
    <p>
    	If you have any questions or want to know more about the study, you can contact Tanja K. O.Madsen via e-mail: SafeVRU@aau.dk
    </p>
</div>
