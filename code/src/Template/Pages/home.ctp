<div class="index large-9 medium-8 columns content">
    <h2><?= __('VRUMonitor') ?></h2>
    Welcome to VRUMonitor. This page is a system for sending out questionnaires related to the EU-research project InDeV. Unless you have been invited to participate in the study, there is not much to see here.
</div>
