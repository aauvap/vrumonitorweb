<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Send many e-mails'), ['action' => 'sendMany']) ?></li>
    </ul>
</nav>
<div class="emailSpool index large-9 medium-8 columns content">
    <h3><?= __('Email Spool') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('recipient') ?></th>
                <th><?= $this->Paginator->sort('sent') ?></th>
                <th><?= $this->Paginator->sort('error') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($emailSpool as $emailSpool): ?>
            <tr>
                <td><?= $this->Number->format($emailSpool->id) ?></td>
                <td><?= h($emailSpool->recipient) ?></td>
                <td><?= $emailSpool->sent ? __('Sent') : __('Not sent'); ?></td>
                <td><?= $emailSpool->error == "" ? __('No error') : __('Has error!'); ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $emailSpool->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $emailSpool->id], ['confirm' => __('Are you sure you want to delete # {0}?', $emailSpool->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
