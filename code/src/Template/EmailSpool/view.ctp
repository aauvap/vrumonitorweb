<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Email'), ['action' => 'edit', $emailSpool->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Email'), ['action' => 'delete', $emailSpool->id], ['confirm' => __('Are you sure you want to delete # {0}?', $emailSpool->id)]) ?> </li>
        <li><?= $this->Html->link(__('Email Queue'), ['action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="emailSpool view large-9 medium-8 columns content">
  <h3><?= h($emailSpool->id) ?></h3>
  <p>In case of errors, inspect the error message, correct the error, and delete the error message. Then the mail will be sent again.</p>
    <table class="vertical-table">
        <tr>
            <th><?= __('Recipient') ?></th>
            <td><?= h($emailSpool->recipient) ?></td>
        </tr>
        <tr>
            <th><?= __('Sender Address') ?></th>
            <td><?= h($emailSpool->sender_address) ?></td>
        </tr>
        <tr>
            <th><?= __('Sender Name') ?></th>
            <td><?= h($emailSpool->sender_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Subject') ?></th>
            <td><?= h($emailSpool->subject) ?></td>
        </tr>
        <tr>
            <th><?= __('Message') ?></th>
            <td><?= h($emailSpool->message) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($emailSpool->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($emailSpool->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($emailSpool->modified) ?></td>
        </tr>
        <tr>
            <th><?= __('Sent') ?></th>
            <td><?= $emailSpool->sent ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Error') ?></th>
            <td><?= h($emailSpool->error) ?></td>
        </tr>
    </table>
</div>
