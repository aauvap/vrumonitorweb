<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('E-mail queue'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="emailSpool index large-9 medium-8 columns content">
  <h3><?= __('Send many e-mails') ?></h3>
  <p><?= __('The e-mail spool contains the list of e-mails to be sent. In order to not crash the server, these are sent in small portions. Each time somebody visits or refreshes a page, 10 mails are sent.') ?></p>
  <p><?= __('This page allows admins to manually send more e-mails all at once. Specify a number of e-mails below, click send, and the specified number of mails in the queue will be sent.') ?></p>
  <p><b><?= __('Be careful!') ?></b> <?= __('Start with a low number and work your way up, until you have a feel for how many you can send without bogging down the server. If you send out too many at once, people may not be able to access the site while the mails are processed.') ?></p>
  <p><b><?= $queueLength ?></b> <?= __('e-mail(s) waiting to be sent.') ?></p>
    <?php
        echo $this->Form->create();
    ?>
    <fieldset>
      <?php

      echo $this->Form->input('num_emails', ['label' => __('Number of e-mails'), 'default' => '100', 'type' => 'number']);
      ?>
    </fieldset>
    <?= $this->Form->button(__('Send')); ?>
    <?= $this->Form->end() ?>    
</div>
