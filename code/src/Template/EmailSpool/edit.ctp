<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $emailSpool->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $emailSpool->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Email Spool'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="emailSpool form large-9 medium-8 columns content">
    <?= $this->Form->create($emailSpool) ?>
    <fieldset>
        <legend><?= __('Edit Email Spool') ?></legend>
        <?php
            echo $this->Form->input('recipient');
            echo $this->Form->input('sender_address');
            echo $this->Form->input('sender_name');
            echo $this->Form->input('subject');
            echo $this->Form->input('message', ['type' => 'textarea']);
            echo $this->Form->input('sent');
            echo $this->Form->input('error', ['type' => 'textarea']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
