<?php
$this->loadHelper('Inflector');
$this->Html->css('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.form', ['block' => true]);
$this->Html->css('questionsUI', ['block' => true]);
?>

<script type="text/javascript">
 var question_id = <?= $question->id ?>;
 var sortableSettings = [{uiElement: "#sortableFields", url: appBaseUrl+'questions/sortFields'},
                         {uiElement: "#sortableConnections", url: appBaseUrl+'questions/sortConnections'}];
</script>


<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Back to questionnaire'), ['controller' => 'Questionnaires', 'action' => 'edit', $question->questionnaire_id]) ?></li>
        <li><?= $this->Html->link(__('List Questionnaires'), ['controller' => 'Questionnaires', 'action' => 'index']) ?></li>
    </ul>
</nav>


<div class="questions form large-9 medium-8 columns content">
  
  <?= $this->Form->create($question) ?>
  <?= $this->Form->button(__('Save'), ['type' => 'submit']) ?>
  <?= $this->Html->link($this->Form->button(__('Preview'), ['type' => 'button']), ['action' => 'view', $question->id], ['escape' => false]); ?>
  
  <h2><?= __('Edit Question') ?></h2>
  <br>
  <fieldset>
    <?php
    echo $this->Form->input('question', ['type' => 'textarea']);
    ?>
  </fieldset>
  <?= $this->Form->end() ?>

  <h4><?= __('Fields') ?></h4>  
  <?php
      if (!empty($fields)): ?>
    
    <table cellpadding="0" cellspacing="0">
      <thead>
        <tr>
          <th><?= __('Caption (internal description in italic)') ?></th>
          <th><?= __('Type') ?></th>
          <th class="actions"><?= __('Actions') ?></th>
        </tr>
      </thead>
      <tbody id="sortableFields" class="clickable">
        <?php foreach ($fields as $field): ?>
          <tr id="fieldOrder_<?= $question->id.":".$field->source().":".$field->id; ?>"
              onclick="javascript:location.href='<?= $this->Url->build(['controller' => $field->source(), 'action' => 'edit', $field->id]); ?>'">
            <td><?= h($field->caption) ?> <i><?= h($field->internal_description) ?></i></td>
            <td><?= $this->Inflector->humanize($this->Inflector->underscore($this->Inflector->singularize($field->source()))) ?></td>
            <td class="actions">
              <?= $this->Form->postLink(__('Delete'), ['controller' => $field->source(), 'action' => 'delete', $field->id], ['confirm' => __('Are you sure you want to delete this field?')]) ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <?php //debug($fields); ?>
  <?php endif; ?>
  <?php
  echo $this->Form->input('add_field_select', array('type' => 'select', 'label' => '',
                                                    'empty' => 'Add field...', 'options' => $field_types, 'id' => 'add_field_select'));
  ?>

  <h4><a name="connections"></a><?= __('Connections') ?></h4>
  <?php
      if (!empty($connections)):
  ?>
    <?php
    $questionOptions = [];
    $conditionOptions = ['' => ''];
    foreach($question->questionnaire->questions as $q) {
        if($q->id > $question->id) {
            $questionOptions[$q->id] = "Q".$q->id.": ".$q->question;
        }
        if(count($q->multiple_choice_fields) > 0) {
            $opts = [];
            foreach($q->multiple_choice_fields as $f) {
                foreach($f->multiple_choice_options as $o) {
                    $opts[$o->id] = $o->caption;
                }
            }
            $conditionOptions["Q".$q->id.": ".$q->question] = $opts;
        }
    }
    $questionOptions['-1'] = "End";
    ?>
    
    <table cellpadding="0" cellspacing="0">
      <thead>
        <tr>
          <th><?= __('If') ?></th>
          <th><?= __('Go to question') ?></th>
          <th class="actions"><?= __('Actions') ?></th>
        </tr>
      </thead>
      <tbody id="sortableConnections" class="clickable">
        <?php foreach ($connections as $connection): ?>
          <tr id="connectionOrder_<?= $question->id.":".$connection->id; ?>">
            
            <td>
              <?php
              foreach($connection->sequence_conditions as $i => $condition) {
                  $conditionType = $condition->or_condition ? __("or") : __("and");
                  if($i > 0) {
                      echo "<i> ".$conditionType."</i><br />";
                  }

                  echo $this->Html->Link("Q".$condition->multiple_choice_option->multiple_choice_field->question_id, ['controller' => 'Questions', 'action' => 'edit', $condition->multiple_choice_option->multiple_choice_field->question_id], ['class' => 'hasTooltip']).": ";
                  echo "<div class='hiddenTooltip'>".$condition->multiple_choice_option->multiple_choice_field->question->question."<br/>".$condition->multiple_choice_option->multiple_choice_field->caption."</div>";
                  echo $condition->multiple_choice_option->caption;
                  echo $this->Form->postLink($this->Html->image('icons/cross.png', ['alt' => 'Delete']), ['controller' => 'SequenceConditions', 'action' => 'delete', $condition->id], ['confirm' => __('Are you sure you want to delete this condition?'), 'escape' => false, 'class' => 'editConnections deleteCondition']);
              }
              
              if(count($connection->sequence_conditions) == 0) {
                  echo "<i>".__("All cases")."</i>";
              }
              
              ?>
              <div class="editConnections addCondition">
                <?php
                echo $this->Form->create(null, [
                    'url' => ['controller' => 'SequenceConditions', 'action' => 'add']
                ]);
                if(count($connection->sequence_conditions) == 1) {
                    echo $this->Form->select('conditionType', ['and' => 'and', 'or' => 'or']);
                } elseif(count($connection->sequence_conditions) > 0)  {
                    $conditionType = $connection->sequence_conditions[0]->or_condition ? "or" : "and";
                    echo $this->Form->hidden('conditionType', ['value' => $conditionType]);
                }
                echo $this->Form->hidden('question_sequence_id', ['value' => $connection->id]);
                echo $this->Form->select('multiple_choice_option_id', $conditionOptions, ['onchange' => '$(this).parent().ajaxSubmit(); location.reload();']);
                echo $this->Form->end();
                ?>
              </div>
            </td>

            <td class="displayConnections"><?php
                if($connection->to_question_id === null) {
                    echo __("End");
                } else {
                    echo $this->Html->Link("Q".$connection->to_question_id, ['controller' => 'Questions', 'action' => 'edit', $connection->to_question_id]).": ".$connection->to_question->question;
                }
                ?>
            </td>

            <td class="editConnections">
              <?php
              echo $this->Form->create(null, [
                  'url' => ['controller' => 'QuestionSequences', 'action' => 'edit', $connection->id]
              ]);
              if($connection->to_question_id === null) {
                  $toId = -1;
              } else {
                  $toId = $connection->to_question_id;
              }
              echo $this->Form->select('to_question_id',
                                       $questionOptions,
                                       ['default' => $toId,
                                        'onchange' => '$(this).parent().ajaxSubmit(); location.reload();']);
              
              echo $this->Form->end();
              ?>
            </td>
            
            <td class="actions">
              <?= $this->Form->postLink(__('Delete'), ['controller' => 'QuestionSequences', 'action' => 'delete', $connection->id], ['confirm' => __('Are you sure you want to delete this connection?')]) ?>
            </td>
          </tr>

          
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endif; ?>
  <?php
  echo $this->Html->Link(__('Add connection...'), ['controller' => 'Questionnaires', 'action' => 'edit', $question->questionnaire_id]);
  ?>
  <p style="font-size: 80%; margin-top: 1em;">Connections will be resolved in the order specified above: The first matching condition will be followed. If no condition is satisfied, the questionnaire will end.</p>
  
  <div class="related">
    <h4><?= __("Information") ?></h4>
    <table class="vertical-table">
      <tr>
        <th><?= __('Question Id') ?></th>
        <td><?= $this->Number->format($question->id) ?></td>
      </tr>
      <tr>
        <th><?= __('Questionnaire') ?></th>
        <td><?= $question->has('questionnaire') ? $this->Html->link($question->questionnaire->id, ['controller' => 'Questionnaires', 'action' => 'view', $question->questionnaire->id]) : '' ?></td>
      </tr>
      <tr>
        <th><?= __('Created') ?></th>
        <td><?= h($question->created) ?></tr>
        </tr>
        <tr>
          <th><?= __('Modified') ?></th>
          <td><?= h($question->modified) ?></tr>
        </tr>
    </table>
  </div>
  <?= $this->Html->Link($this->Form->button(__('Clone question')), ['action' => 'duplicate', $question->id], ['escape' => false]) ?>
</div>

<?= $this->Html->script("questionsUI.js"); ?>
<?= $this->Html->script("sortable.js"); ?>
