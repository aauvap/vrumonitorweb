<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Questionnaires'), ['controller' => 'Questionnaires', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="questions index large-9 medium-8 columns content">
    <h3><?= __('Questions') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('questionnaire_id') ?></th>
                <th><?= $this->Paginator->sort('question') ?></th>
                <th><?= $this->Paginator->sort('min_num_choices') ?></th>
                <th><?= $this->Paginator->sort('max_num_choices') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody class="clickable">
            <?php foreach ($questions as $question): ?>
              <tr onclick="javascript:location.href='<?= $this->Url->build(['action' => 'edit', $question->id]); ?>'">
                <td><?= $this->Number->format($question->id) ?></td>
                <td><?= $question->has('questionnaire') ? $this->Html->link($question->questionnaire->id, ['controller' => 'Questionnaires', 'action' => 'view', $question->questionnaire->id]) : '' ?></td>
                <td><?= h($question->question) ?></td>
                <td><?= $this->Number->format($question->min_num_choices) ?></td>
                <td><?= $this->Number->format($question->max_num_choices) ?></td>
                <td><?= h($question->created) ?></td>
                <td><?= h($question->modified) ?></td>
                <td class="actions">
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $question->id], ['confirm' => __('Are you sure you want to delete # {0}?', $question->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
