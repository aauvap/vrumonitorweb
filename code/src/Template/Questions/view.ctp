

<div class="questions view large-9 medium-8 columns content">
  <!-- <h4><?= h($question->priority+1) ?></h4>  -->
  <?php
  $pattern = '#\Q%\E(.+)\Q%\E#';
  $hasLoop = preg_match($pattern, $question->question) == 1;
  if($hasLoop){
    $question->question = preg_replace('#\Q%\E(.+)\Q%\E#', 
                  $this->Html->link(__('\1'), [
                    'action' => 'loop', 
                    'controller' => 'questionnaires',
                    '?' => [
                      'key' => $incidentKey
                    ],
                    $incidentId])
              , $question->question);

  }
  ?>
  <h5><?= $this->Markdown->transform($question->question) ?></h5>
  <?php
  echo $this->Form->create(null, [
          'url' => ['controller' => 'Questions', 'action' => 'saveAnswers', $question->id, $incidentId],//, "?" => ["key" => $key]
          'id' => 'answerForm',
          'onsubmit' => 'return validateAnswers();'
      ]);
  if (!empty($fields)) {
      $fieldCounts = [
          'FreeTextFields' => 0,
          'NumberFields' => 0,
          'DateFields' => 0,
          'MultipleChoiceFields' => 0,
          'MapFields' => 0
      ];
      foreach ($fields as $field) {
          echo $this->cell('Field', ['question' => $question, 'field' => $field, 'fieldCounts' => $fieldCounts]);
          $fieldCounts[$field->source()]++;
      }    
  }
  
  if($allowAnswer) {
      if($previousQuestion > 0) {
          /*echo '<input type="button" onclick="location.href=\'';
          echo $this->Url->build(['controller' => 'Questions', 'action' => 'view', $previousQuestion]);
          echo '\';" value="Back" />';*/

          echo '<button type="button" style="margin-right: 1em;" onclick="location.href=\'';
          echo $this->Url->build(['controller' => 'Questions', 'action' => 'view', $previousQuestion]);
          echo '\';">';
          echo $this->Html->image('icons/arrow-180.png');
          echo '</button>';
          
          /*echo $this->Html->link($this->Form
                                      ->button(
                                          $this->Html->image('icons/arrow-180.png'),
                                          ['type' => 'button', 'escape' => false, 'style' => 'margin-right: 1em']),
                                 ['action' => 'view',
                                  $previousQuestion],
                                 ['escape' => false])*/;
      }
      if($hasLoop == false) {
        echo $this->Form->button($this->Html->image('icons/arrow.png'), ['escape' => false]);
      }
  } else {
      echo $this->Form->button($this->Html->image('icons/arrow-180.png'), ['type' => 'button', 'disabled' => true, 'escape' => false, 'style' => 'margin-right: 1em']);
      echo $this->Form->button($this->Html->image('icons/arrow.png'), ['disabled' => true, 'escape' => false]);
  }
  echo $this->Form->end();
  $this->Flash->render("test");
  ?>
      
</div>
<script type="text/javascript">
 function validateAnswers() {
     var success = true;
     $('#answerForm fieldset').each(function(i) {
         if($("input:checkbox", this).length > 0) {
             var fieldId = $("input:checkbox", this).first().attr('id').split('-')[1];
             var checkedBoxes = $("input:checkbox:checked", this).length;
             if(checkedBoxes < fieldRequirements.min[fieldId]) {
                 $('#answerForm').after('<div class="message error" onclick="this.classList.add(\'hidden\');">'+minError+'</div>');
                 success = false;
             }
             if(checkedBoxes > fieldRequirements.max[fieldId]) {
                 $('#answerForm').after('<div class="message error" onclick="this.classList.add(\'hidden\');">'+maxError+'</div>');
                 success = false;
             }
         }

         if($("input:radio", this).length > 0) {
             var fieldId = $("input:radio", this).first().attr('id').split('-')[1];
             var checkedBoxes = $("input:radio:checked", this).length;
             if(checkedBoxes < fieldRequirements.min[fieldId]) {
                 $('#answerForm').after('<div class="message error" onclick="this.classList.add(\'hidden\');">'+minError+'</div>');
                 success = false;
             }
         }
     });
     return success;
 }
</script>
<?php
echo $this->Html->script('viewQuestionsUI');
?>
