<div class="questionnaires view large-9 medium-8 columns content">
    <h3><?= h($questionnaire->title) ?></h3>
    <p><?= $this->Markdown->transform($questionnaire->end_text) ?></p>

</div>
