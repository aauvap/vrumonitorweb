<?php
$this->Html->css('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.form', ['block' => true]);
$this->Html->css('questionnairesUI', ['block' => true]);
$this->Html->script('snap.svg-min', ['block' => true]);
?>

<script type="text/javascript">
 var sortableSettings = [{
     uiElement: "#sortableQuestions",
     url: appBaseUrl+'questionnaires/sort-questions',
     updateFunction:
     function (event, ui) {
         var data = $(this).sortable('serialize');
         //var orderedIds = data.split(/&?questionOrder\[\]=[0-9]+:/).slice(1);
         //console.log(orderedIds);
         $.ajax({
             data: data,
             type: 'POST',
             url: appBaseUrl+'questionnaires/sort-questions'
         });
         location.reload();
     }
 }];
</script>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List questionnaires'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Send new incident to registered participants'), ['action' => 'sendIncident', $questionnaire->id]) ?></li>
        <li><?= $this->Html->link(__('Send new incident to specified participants'), ['action' => 'sendIncidentSpecific', $questionnaire->id])?></li>
        <li><?= $this->Html->link(__('Send reminder about missing answers'), ['action' => 'sendReminder', $questionnaire->id])?></li>
        <li><?= $this->Html->link(__('Clear unanswered incidents'), ['action' => 'clearIncidents', $questionnaire->id]) ?></li>
        <li><?= $this->Html->link(__('Send activation reminder'), ['action' => 'sendActivationReminder', $questionnaire->id]) ?></li>
        <li><?= $this->Html->link(__('Create Incident Links'), ['action' => 'createLinks', $questionnaire->id]) ?></li>
        <li><?= $this->Html->link(__('View Answers'), ['action' => 'viewAnswers_detailed', $questionnaire->id]) ?></li>
        <li><?= $this->Html->link(__('View Questions'), ['action' => 'viewQuestions', $questionnaire->id]) ?></li>
        <li><?= $this->Html->link(__('View Participants'), ['action' => 'viewParticipants', $questionnaire->id]) ?></li>
    </ul>
</nav>
<div class="questionnaires form large-9 medium-8 columns content">
  <?= $this->Form->create($questionnaire) ?>
  <?= $this->Form->button(__('Save')) ?>

  <h2><?= __('Edit Questionnaire') ?></h2>
    <fieldset>
        <?php
        echo $this->Form->input('title');
        echo $this->Form->input('intro_text', ['type' => 'textarea', 'maxlength' => '65536']);
        echo $this->Form->input('end_text', ['type' => 'textarea', 'maxlength' => '65536']);
        echo $this->Form->select('language', $languageCodes);
        echo $this->Form->input('active', ['default' => true]);
        echo $this->Form->input('inactive_message', ['type' => 'textarea']);
        echo $this->Form->input('restrict_access', ['label' => __('Restrict access to invited users only.')]);
        if(!$questionnaire['restrict_access']) {
            $url = $this->Url->build(['action' => 'newParticipant', $questionnaire->id], true);
            echo "<p>".__("This questionnaire is publicly accessible. Share it using this link: ").$this->Html->link($url, $url)."</p>";
            /*echo $this->Form->input('auto_create_participants', ['label' => __('Auto-create participants when answering via app.')]);
            echo $this->Form->input('email_field', ['label' => __('E-mail field'), 'type' => 'select', 'options' => $emailFields]);
            echo $this->Form->input('target_questionnaire', ['label' => __('Target questionnaire'), 'options' => $allQuestionnaires]);*/
        }
        echo $this->Form->input('restrict_incident_creation', ['label' => __('Restrict participants from creating their own incidents.')]);
        ?>
    </fieldset>
    <?= $this->Form->end() ?>

    <h4><?= __('Questions') ?></h4>
    
    <?php if (!empty($questions)): ?>

      <div id="questionsContainer">
       
        <svg id="branchingTree" style="float: left;">
          <?= $svg; ?>
        </svg>
        <?= $this->Form->create(null, [
            'url' => ['controller' => 'QuestionSequences', 'action' => 'add'],
            'id' => 'addQuestionSequence'
        ]) ?>
        <?= $this->Form->hidden('question_id'); ?>
        <?= $this->Form->hidden('to_question_id'); ?>
        <?= $this->Form->hidden('priority', ['value' => '0']); ?>
        <?= $this->Form->end() ?>

        <script type="text/javascript">
         var branchingTreeWidth = <?= $treeWidth ?>;
         var connectionConditionsTitle =
         <?php
         echo "'";
         echo __("Connection conditions");
         echo "'";
         ?>;
         var connectionConditionsTextStart =
         <?php
         echo "'";
         echo "<ul>";
         echo "'";
         ?>;
         
         var connectionConditionsTextEnd =
         <?php
         echo "'";
         echo '<hr /><form name="delete_edge_%edgeId%" method="post" action="/question_sequences/delete/%edgeId%"><input name="_method" value="POST" type="hidden"><a href="#" class="actionLink"  onclick="if (confirm(\\\'';
         echo __('Are you sure you want to delete this connection?');
         echo '\\\')) { $(this).closest(\\\'form\\\').ajaxSubmit(); location.reload(); return false; } event.returnValue = false; return false;">'.__('Delete connection').'</a></form>';
         echo "'";
         //$(\\"form[name=\\\'delete_edge_%edgeId%\\\']\\").submit(); 
         ?>;
 
         var connectionConditions = [];
         var connectionPriority = [];
         var connectionEditLink = [];
         <?php
         foreach($questions as $question) {
             if(empty($question->question_sequences)) {
                 // If there are no associated question sequences, there are no edges and we do nothing.
                 continue;
             }
             /*if(count($question->question_sequences) == 1) {
                // If there is just one connection, it must be followed in all cases.
                echo "connectionConditions[".$question->question_sequences[0]->id."] = ['<li>".__("All cases")."</li>'];";
                echo "connectionPriority[".$question->question_sequences[0]->id."] = '</ul>';";
                echo "connectionEditLink[".$question->question_sequences[0]->id."] = '';";
                continue;
                }*/
             foreach($question->question_sequences as $seq) {
                 // 
                 echo "connectionConditions[".$seq->id."] = [";
                 foreach($seq->sequence_conditions as $i => $cond) {
                     echo "'<li>";
                     if($i > 0) {
                         $type = $cond->or_condition ? __("or") : __("and");
                         echo "<i>".$type."</i> ";
                     }
                     echo "Q".$cond->multiple_choice_option->multiple_choice_field->question_id.": ";
                     echo addslashes($cond->multiple_choice_option->caption);
                     echo "</li>',";
                 }
                 if(count($seq->sequence_conditions) == 0) {
                     echo "'<li>".__("No conditions")."</li>',";
                 }
                 echo "];";
                 echo "connectionPriority[".$seq->id."] = 'Priority: <b>".($seq->priority+1)."</b></ul>';\n";
                 echo "connectionEditLink[".$seq->id."] = '".$this->Html->link(__('Edit conditions...'), ['controller' => 'Questions', 'action' => 'edit', $question->id, '#' => 'connections'], ['class' => 'actionLink'])."';\n";
             }
         }
         ?>
         
         var s = Snap("#branchingTree");
        </script>

        <div style="overflow: hidden;">
        <table cellpadding="0" cellspacing="0" id="questionsTable" class="clickable">
          <thead>
            <tr>
              <th><?= __('Question') ?></th>
              <th class="actions deleteAction"><?= __('Actions') ?></th>
            </tr>
          </thead>
          <tbody id="sortableQuestions" class="clickable">
            <?php foreach ($questions as $question): ?>
              <tr id="questionOrder_<?= $questionnaire->id.":".$question->id; ?>" onclick="javascript:location.href='<?= $this->Url->build(['controller' => 'Questions', 'action' => 'edit', $question->id]); ?>'">
                <td>Q<?= $question->id ?>: <?= $this->Text->truncate(h($question->question), 45) ?></td>
                <td class="actions">
                  <?= $this->Form->postLink(__('Delete'), ['controller' => 'Questions', 'action' => 'delete', $question->id], ['confirm' => __('Are you sure you want to delete # {0}?', $question->id)]) ?>
                  <?= $this->Html->Link(__('Clone'), ['controller' => 'Questions', 'action' => 'duplicate', $question->id]) ?>

                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        </div>
      </div>
    <?php endif; ?>
    
    <?php
    //debug($routes);
    echo $this->Html->Link(__('Add question...'), ['controller' => 'Questions', 'action' => 'add', $questionnaire->id]);
    ?>

    <div class="related">
      <h4><?= __("Information") ?></h4>
      <table class="vertical-table">
        <tr>
          <th><?= __('Questionnaire Id') ?></th>
          <td><?= $this->Number->format($questionnaire->id) ?></td>
        </tr>
        <tr>
          <th><?= __('Created') ?></th>
          <td><?= h($questionnaire->created) ?></tr>
            </tr>
            <tr>
              <th><?= __('Modified') ?></th>
              <td><?= h($questionnaire->modified) ?></tr>
            </tr>
            <tr>
              <th><?= __('Number of participants') ?></th>
              <td><?= h($numParticipants) ?></tr>
            </tr>
            <tr>
              <th><?= __('Number of activated participants') ?></th>
              <td><?= h($numActivations) ?></tr>
            </tr>
            <tr>
              <th><?= __('Number of completed answers') ?></th>
              <td><?= h($numAnswered) ?></tr>
            </tr>
      </table>
    </div>
    
    <?= $this->Html->Link($this->Form->button(__('Clone questionnaire')), ['action' => 'duplicate', $questionnaire->id], ['escape' => false]) ?>
</div>

<?= $this->Html->script("questionnairesUI.js"); ?>
<?= $this->Html->script("sortable.js"); ?>
