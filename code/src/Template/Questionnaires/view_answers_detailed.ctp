<?php
$this->Html->css('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.form', ['block' => true]);
$this->Html->css('questionnairesUI', ['block' => true]);
$this->Html->script('snap.svg-min', ['block' => true]);

$pages = $count / $maxEntriesPerOutput;
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('<< Back to questionnaire'), ['action' => 'edit', $questionnaireId]) ?></li>
    </ul>
</nav>
<div class="questionnaires form large-9 medium-8 columns content">
  <h3><?= __('View Answers') ?></h3>
  <table cellpadding="0" cellspacing="0">

        <tbody class="clickable">
            <?php for($i = 0; $i < $pages; $i++): ?>
            <tr>
                <td>Page <?=$i+1?> [number of incidents: <?=min(5000, $count - $maxEntriesPerOutput*$i)?>]</td>
                <td><?=$this->Html->link(__('Download'), ['action' => 'viewAnswers', $questionnaireId, $i+1])?></td>
            </tr>
            <?php endfor; ?>
        </tbody>
    </table>
</div>
