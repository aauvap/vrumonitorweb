  <?php
$this->Html->css('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.form', ['block' => true]);
$this->Html->css('questionnairesUI', ['block' => true]);
$this->Html->script('snap.svg-min', ['block' => true]);
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('<< Back to questionnaire'), ['action' => 'edit', $questionnaireId]) ?></li>
    </ul>
</nav>
<div class="questionnaires form large-9 medium-8 columns content">
    <h3><?= __('Send a new incident to participants') ?></h3>
    <p><?= __('Send out a questionnaire to the participants of this questionnaire.')?></p>
    <?php 
    echo $this->Form->create(null, ['id' => 'testId']);
    ?>
    <fieldset>
      <?php
      echo $this->Form->input('confirm', [
        'type' => 'hidden',
        'value' => $confirm]);

      echo $this->Form->input('participants', [
        'type' => 'hidden',
        'value' => isset($participants) ? implode(" ", $participants) : null
        ]);        
      
      if($confirm) {
        echo "<p>" . __("Please check that the following list of participant ids are the ones you want to send to. Then press confirm below.")."</p><ul>";
        foreach ($participants as $key => $p) {
          $participantEmail = $participantsEmails[$p];
          echo "<li>".$p." (".$participantEmail.")</li>";
        }
        echo "</ul>";
      } 
      
      echo $this->Form->input('participantIds', [
        'label' => __('Participant Ids. (One per Line)'), 
        'type' => $confirm ? 'hidden' :'textarea']);      
      
      echo $this->Form->input('email_subject', [
        'label' => __('Subject'), 
        'default' => $sampleSubject]);
      echo $this->Form->input('email_sender_name', [
        'label' => __('Name of sender'), 
        'default' => $sampleSenderName]);
      echo $this->Form->input('email_sender_address', [
        'label' => __('E-mail addresse of sender'), 
        'default' => $sampleSenderAddress]);
      echo $this->Form->input('email', [
        'label' => __('Content (must include %link% where questionnaire link is placed)'), 
        'default' => $sampleEmail, 
        'type' => 'textarea']);
      echo $this->Form->input('which_questionnaire', [
        'default' => $questionnaireId, 
        'label' => __('Which questionnaire should be sent to these participants?'), 
        'type' => 'number']);
      ?>
    </fieldset>
    <?php
    $buttonText = $confirm ? __('Confirm') : __('Generate Links');
    echo $this->Form->button($buttonText);
    ?>
    <?= $this->Form->end() ?>
</div>
