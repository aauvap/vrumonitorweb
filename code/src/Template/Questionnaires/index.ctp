<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
      <li class="heading"><?= __('Actions') ?></li>
      <li><?= $this->Html->link(__('Add Questionnaire'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="questionnaires index large-9 medium-8 columns content">
    <h3><?= __('Questionnaires') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('title') ?></th>
                <th><?= $this->Paginator->sort('language') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody class="clickable">
            <?php foreach ($questionnaires as $questionnaire): ?>
            <tr onclick="javascript:location.href='<?= $this->Url->build(['action' => 'edit', $questionnaire->id]); ?>'">
                <td><?= $this->Number->format($questionnaire->id) ?></td>
                <td><?= h($questionnaire->title) ?></td>
                <td><?= h($questionnaire->language) ?></td>
                <td><?= h($questionnaire->created) ?></td>
                <td><?= h($questionnaire->modified) ?></td>
                <td class="actions">
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $questionnaire->id], ['confirm' => __('Are you sure you want to delete # {0}?', $questionnaire->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
