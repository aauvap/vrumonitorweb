<?php
$this->Html->css('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.form', ['block' => true]);
$this->Html->css('questionnairesUI', ['block' => true]);
$this->Html->script('snap.svg-min', ['block' => true]);
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('<< Back to questionnaire'), ['action' => 'edit', $questionnaireId]) ?></li>
    </ul>
</nav>
<div class="questionnaires form large-9 medium-8 columns content">
    <h3><?= __('Create Incident Links') ?></h3>
    <p><?= __('Create links to send to participants in mails')?></p>
    <?php 
    echo $this->Form->create(null, ['id' => 'testId']);
    ?>
    <fieldset>
      <?= $this->Form->input('number', ['type' => 'number', 'label' => 'Number of links to create']); ?>
    </fieldset>
    <?php
    echo $this->Form->button(__('Generate Links'));

    echo $textArea;
    ?>
    <?= $this->Form->end() ?>
</div>
