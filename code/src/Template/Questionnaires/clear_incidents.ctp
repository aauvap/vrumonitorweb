<?php
$this->Html->css('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.form', ['block' => true]);
$this->Html->css('questionnairesUI', ['block' => true]);
$this->Html->script('snap.svg-min', ['block' => true]);
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('<< Back to questionnaire'), ['action' => 'edit', $questionnaireId]) ?></li>
    </ul>
</nav>
<div class="questionnaires form large-9 medium-8 columns content">
    <h3>Clear Incidents</h3>

    <p><?= __('Pressing the button below will lock all previously created incidents. This means that participants of those incidents will not be reminded in future answer reminders. This action is irreversable, so be careful with pressing.')?></p> 

    <p><?= sprintf(__('%d incidents will be locked.'), $incidentNumber) ?></p>

    <?php 
    echo $this->Form->create(null, ['id' => 'testId']);
    echo $this->Form->button(__('Clear Incidents'));
    ?>
    <?= $this->Form->end() ?>
</div>
