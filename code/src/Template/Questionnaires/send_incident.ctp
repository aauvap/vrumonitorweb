<?php
$this->Html->css('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.form', ['block' => true]);
$this->Html->css('questionnairesUI', ['block' => true]);
$this->Html->script('snap.svg-min', ['block' => true]);
?>

<script type="text/javascript">
  var activatedParticipants = <?= $numIncidents?>;
  var totalParticipants = <?= $totalIncidents?>;

  function updateText (val) {

    if(val) {
      $('#newIncidents').text("The selected condition will generate " + totalParticipants + " new incidents");
    }else {
      $('#newIncidents').text("The selected condition will generate " + activatedParticipants + " new incidents");
    }
  }

  $(document).ready(function() {
    $('#include_non_activated_checkbox').click(function() {
      updateText($('#include_non_activated_checkbox').is(":checked"));
    });
    updateText (false);
  });
</script>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('<< Back to questionnaire'), ['action' => 'edit', $questionnaireId]) ?></li>
    </ul>
</nav>
<div class="questionnaires form large-9 medium-8 columns content">
    <h3><?= __('Send a new incident to participants') ?></h3>
    <p><?= __('Send out a questionnaire to the participants of this questionnaire.')?></p>
    <?php 
    echo $this->Form->create(null, ['id' => 'testId']);
    ?>
    <fieldset>
      <?php
      echo $this->Form->input('email_subject', [
        'label' => __('Subject'), 
        'default' => $sampleSubject]);
      echo $this->Form->input('email_sender_name', [
        'label' => __('Name of sender'), 
        'default' => $sampleSenderName]);
      echo $this->Form->input('email_sender_address', [
        'label' => __('E-mail addresse of sender'), 
        'default' => $sampleSenderAddress]);
      echo $this->Form->input('email', [
        'label' => __('Content (must include %link% where questionnaire link is placed)'), 
        'default' => $sampleEmail, 
        'type' => 'textarea']);
      echo $this->Form->input('which_questionnaire', [
        'default' => $questionnaireId, 
        'label' => __('Which questionnaire should be sent to these participants?'), 
        'type' => 'number']);
      echo $this->Form->input('include_non_activated', [
        'type' => 'checkbox', 
        'label' => __('Should non-activated participants be included?'),
        'id' => 'include_non_activated_checkbox']);
      ?>
    </fieldset>
    <p id='newIncidents'></p><!-- Uses jQuery, see above -->
    <?php
    echo $this->Form->button(__('Generate Links'));
    ?>
    <?= $this->Form->end() ?>
</div>
