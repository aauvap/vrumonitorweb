<?php

echo "--" . $questionnaire->title . "--" . "\n\n";
foreach ($questionnaire->questions as $i => $q) {
    $txt = trim(preg_replace('/\s\s+/', ' ', $q->question)); #Replace newline with single-space
    echo $txt ." (id" . $q->id . ")\n";
    $allFields = array_merge($q['free_text_fields'],
                             $q['number_fields'],
                             $q['date_fields'],
                             $q['multiple_choice_fields'],
                             $q['map_fields']);
    usort($allFields, function($a, $b) {return $a->priority - $b->priority;});
    foreach ($allFields as $j => $f) {
        $txt = trim(preg_replace('/\s\s+/', ' ', $f->caption)); #Replace newline with single-space

        $class = explode('\\', get_class($f));
        $class = end($class);

        if($txt == "") {
            $txt = $class;
        }
        echo "\t" . $txt;

        if($class == "NumberField") {
            echo " Range: " . $f->min_number . "-" . $f->max_number;
        }
        else if($class == "MultipleChoiceField") {
            echo ", options:";
            foreach ($f->multiple_choice_options as $k => $o) {
                echo "\n\t\t" . $o->caption;
                if($o->internal_description != "") {
                    echo " [" . $o->internal_description . "]";
                }
            }
        }
        echo "\n\n";
    }
}
?>
