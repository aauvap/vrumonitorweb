<?php
$this->Html->css('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.qtip.min', ['block' => true]);
$this->Html->script('jquery.form', ['block' => true]);
$this->Html->css('questionnairesUI', ['block' => true]);
$this->Html->script('snap.svg-min', ['block' => true]);
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('<< Back to questionnaire'), ['action' => 'edit', $questionnaireId]) ?></li>
    </ul>
</nav>
<div class="questionnaires form large-9 medium-8 columns content">
  <h3><?= __('Send activation reminder') ?></h3>
  <p>From this page you can send a reminder to those participants who have not yet activated their account.</p>
    <?php 
    echo $this->Form->create(null, ['id' => 'testId']);
    ?>
    <fieldset>
      <?php
      echo $this->Form->input('email_subject', ['label' => __('Subject'), 'default' => $sampleSubject]);
      echo $this->Form->input('email_sender_name', ['label' => __('Name of sender'), 'default' => $sampleSenderName]);
      echo $this->Form->input('email_sender_address', ['label' => __('E-mail addresse of sender'), 'default' => $sampleSenderAddress]);
      echo $this->Form->input('email', ['label' => __('Content (must include %link% where activation link is placed)'), 'default' => $sampleEmail, 'type' => 'textarea']);
      ?>
    </fieldset>
    <?php
    echo "<p id='newIncidents'>".sprintf(__("The selected conditions will generate %d reminder e-mails."), $numReminders)."</p>";
    echo $this->Form->button(__('Send the mails'));
    ?>
    <?= $this->Form->end() ?>
</div>
