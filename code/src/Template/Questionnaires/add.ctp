<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Questionnaires'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="questionnaires form large-9 medium-8 columns content">
    <?= $this->Form->create($questionnaire) ?>
    <fieldset>
        <legend><?= __('Add Questionnaire') ?></legend>
        <?php
            echo $this->Form->input('title');
            echo $this->Form->input('intro_text', ['type' => 'textarea']);
            echo $this->Form->input('end_text', ['type' => 'textarea']);
            echo $this->Form->select('language', $languageCodes);
            echo $this->Form->input('restrict_access', ['default' => true, 'label' => __('Restrict access to invited users only.')]); // Uncheck this to make a completely public questionnaire, e.g. for signing up future participants.
            echo $this->Form->input('restrict_incident_creation', ['default' => true, 'label' => __('Restrict participants from creating their own incidents.')]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
