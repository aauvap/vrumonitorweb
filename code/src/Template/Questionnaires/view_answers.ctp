<?php
echo "ParticipantId;IncidentId;Locked;StartTime;EndTime";
$fields = array();
foreach($questionnaire->questions as $i => $q) {
    $allFields = array_merge($q['free_text_fields'],
                             $q['number_fields'],
                             $q['date_fields'],
                             $q['multiple_choice_fields'],
                             $q['map_fields']);
    usort($allFields, function($a, $b) {return $a->priority - $b->priority;}); // Sort fields by priority.
    foreach($allFields as $j => $f) {
        $class = explode('\\', get_class($f));
        $questionTxt = cleanString($q->question);
        echo ";". $questionTxt . "(" . $f->id . ")";
        $fields[] = end($class).$f->id;        
    }
}

function cleanString($string){
    $res = preg_replace('/\\Q**\\E(.*)\\Q**\\E/', '\\1', $string);
    $res = preg_replace('/\s\s+/', ' ', $res);
    $res = preg_replace('/\n/', ' ', $res);
    if(preg_match('/;/', $res) > 0) {
        $res = "\"" . $res . "\"";
    }
    return trim($res);
}

function compDates($a, $b) {
    if($a->modified < $b->modified) {
        return -1;
    } else return 1;    
}

function dateString($incident) {
    $allAnswers = array_merge($incident['free_text_answers'],
                             $incident['number_answers'],
                             $incident['date_answers'],
                             $incident['multiple_choice_answers'],
                             $incident['map_answers']);
    if(empty($allAnswers)) {
        $first = "not answered";
        $last = "";
    } else {
        usort($allAnswers, "compDates");
        $first = $allAnswers[0]->modified;
        $last = end($allAnswers)->modified;
    }

    echo $first.";".$last.";";
}

foreach($incidents as $incident) {
    echo "\n".$incident->participant->id.";".$incident->id.";".$incident->locked.";";
    dateString($incident); //firstAnswer;lastAnswer;
    $answers = array_fill(0, count($fields), '');
    foreach($incident->free_text_answers as $a) {
        $txt = cleanString($a->text);
        $answers[array_search('FreeTextField'.$a->free_text_field_id, $fields)] = $txt;
    }
    foreach($incident->number_answers as $a) {
        $answers[array_search('NumberField'.$a->number_field_id, $fields)] = $a->number;
    }
    foreach($incident->date_answers as $a) {
        $answers[array_search('DateField'.$a->date_field_id, $fields)] = $a->datetime;
    }
    foreach($incident->multiple_choice_answers as $a) {
        $intDes = $a->multiple_choice_option->internal_description;
        $key = array_search('MultipleChoiceField'.$a->multiple_choice_option->multiple_choice_field_id, $fields);
        if($intDes != "") {
            $answers[$key] .= $intDes;
        } else {
            $answers[$key] .= $a->multiple_choice_option->caption;
        }
    }
    foreach($incident->map_answers as $a) {
        $answers[array_search('MapField'.$a->map_field_id, $fields)] = '('.$a->latitude.','.$a->longitude.')';
    }
    echo implode(";",$answers);
}
?>
