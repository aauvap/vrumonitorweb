<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Map Field'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="mapFields index large-9 medium-8 columns content">
    <h3><?= __('Map Fields') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('question_id') ?></th>
                <th><?= $this->Paginator->sort('caption') ?></th>
                <th><?= $this->Paginator->sort('min_num_marks') ?></th>
                <th><?= $this->Paginator->sort('max_num_marks') ?></th>
                <th><?= $this->Paginator->sort('priority') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($mapFields as $mapField): ?>
            <tr>
                <td><?= $this->Number->format($mapField->id) ?></td>
                <td><?= $mapField->has('question') ? $this->Html->link($mapField->question->id, ['controller' => 'Questions', 'action' => 'view', $mapField->question->id]) : '' ?></td>
                <td><?= h($mapField->caption) ?></td>
                <td><?= $this->Number->format($mapField->min_num_marks) ?></td>
                <td><?= $this->Number->format($mapField->max_num_marks) ?></td>
                <td><?= $this->Number->format($mapField->priority) ?></td>
                <td><?= h($mapField->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $mapField->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mapField->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mapField->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mapField->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
