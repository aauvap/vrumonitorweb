<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Map Field'), ['action' => 'edit', $mapField->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Map Field'), ['action' => 'delete', $mapField->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mapField->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Map Fields'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Map Field'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="mapFields view large-9 medium-8 columns content">
    <h3><?= h($mapField->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Question') ?></th>
            <td><?= $mapField->has('question') ? $this->Html->link($mapField->question->id, ['controller' => 'Questions', 'action' => 'view', $mapField->question->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Caption') ?></th>
            <td><?= h($mapField->caption) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($mapField->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Min Num Marks') ?></th>
            <td><?= $this->Number->format($mapField->min_num_marks) ?></td>
        </tr>
        <tr>
            <th><?= __('Max Num Marks') ?></th>
            <td><?= $this->Number->format($mapField->max_num_marks) ?></td>
        </tr>
        <tr>
            <th><?= __('Priority') ?></th>
            <td><?= $this->Number->format($mapField->priority) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($mapField->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($mapField->modified) ?></tr>
        </tr>
    </table>
</div>
