<?php
$this->Html->css('map.css', ['block' => true]);
$this->Html->css('leaflet.css', ['block' => true]);
$this->Html->script('leaflet.js', ['block' => true]);
$this->Html->script('https://maps.google.com/maps/api/js?v=3', ['block' => true]);
$this->Html->script('Google', ['block' => true]);
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Back to question'), ['controller' => 'Questions', 'action' => 'edit', $field->question_id]) ?></li>
    </ul>
</nav>
<div class="mapFields form large-9 medium-8 columns content">
    <?= $this->Form->create($field) ?>
    <fieldset>
        <legend><?= __('Edit Map Field') ?></legend>
        <?php
            echo $this->Form->input('caption');
            echo $this->Form->input('min_num_marks');
        echo $this->Form->input('max_num_marks');
        ?>
        Initial map position:
        <?php
        $defaultSelection = 'auto';
        if(!is_null($field->map_bounding_box) && !empty($field->map_bounding_box)) {
            $defaultSelection = 'defined';
        }
        echo $this->Form->radio('mapSettings',
                                ['auto' => 'Detect automatically from app.',
                                 'defined' => 'Define map position and zoom.'],
                                ['default' => $defaultSelection]                                
        );
        echo $this->Form->hidden('map_bounding_box');
        ?>        
        <div id="map"></div>
    </fieldset>
    
    
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<script type="text/javascript">
 <?php
 $defaultSelection = 'auto';
 if(!is_null($field->map_bounding_box) && !empty($field->map_bounding_box)) {
 ?>
    var map_bounding_box = [<?= $field->map_bounding_box ?>];
 <?php
} else {
?>
    var map_bounding_box = null;
<?php
}
?>            
</script>
<?= $this->Html->script('mapUI'); ?>
