<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Free Text Field'), ['action' => 'edit', $freeTextField->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Free Text Field'), ['action' => 'delete', $freeTextField->id], ['confirm' => __('Are you sure you want to delete # {0}?', $freeTextField->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Free Text Fields'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Free Text Field'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="freeTextFields view large-9 medium-8 columns content">
    <h3><?= h($freeTextField->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Question') ?></th>
            <td><?= $freeTextField->has('question') ? $this->Html->link($freeTextField->question->id, ['controller' => 'Questions', 'action' => 'view', $freeTextField->question->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Caption') ?></th>
            <td><?= h($freeTextField->caption) ?></td>
        </tr>
        <tr>
            <th><?= __('Sample Text') ?></th>
            <td><?= h($freeTextField->sample_text) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($freeTextField->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Min Length') ?></th>
            <td><?= $this->Number->format($freeTextField->min_length) ?></td>
        </tr>
        <tr>
            <th><?= __('Max Length') ?></th>
            <td><?= $this->Number->format($freeTextField->max_length) ?></td>
        </tr>
        <tr>
            <th><?= __('Priority') ?></th>
            <td><?= $this->Number->format($freeTextField->priority) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($freeTextField->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($freeTextField->modified) ?></tr>
        </tr>
    </table>
</div>
