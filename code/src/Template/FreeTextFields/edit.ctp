<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Back to question'), ['controller' => 'Questions', 'action' => 'edit', $field->question_id]) ?></li>
    </ul>
</nav>
</nav>
<div class="freeTextFields form large-9 medium-8 columns content">
    <?= $this->Form->create($field) ?>
    <fieldset>
        <legend><?= __('Edit Free Text Field') ?></legend>
        <?php
            echo $this->Form->input('caption');
            echo $this->Form->input('sample_text');
            echo $this->Form->input('min_length');
            echo $this->Form->input('max_length');
            echo $this->Form->input('enforce_unique', ['label' => __('Enforce unique (used for e.g. e-mail addresses which must be unique across participants - use with care!)')]);
            echo $this->Form->input('validate_as_email', ['label' => __('Validate as Email?')]);
            echo $this->Form->input('identical_to', ['options' => $freeTextFields, 'empty' => '[None]', 'value' => $field->identical_to, 'label' => __('Identical to (use to validate e-mails - must refer to previous field in this question)')]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
