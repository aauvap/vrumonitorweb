<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li><?php
          if(!is_null($question_id)) {
              echo "<li>".$this->Html->link(__('Back to question'), ['controller' => 'Questions', 'action' => 'edit', $question_id])."</li>";
          }
       ?>
    </ul>
</nav>
<div class="freeTextFields form large-9 medium-8 columns content">
    <?= $this->Form->create($field) ?>
    <fieldset>
        <legend><?= __('Add Free Text Field') ?></legend>
        <?php
            if(is_null($question_id)) {
                echo $this->Form->input('questions', ['label' => __('Question')]);
            }
            echo $this->Form->input('caption');
            echo $this->Form->input('sample_text');
            echo $this->Form->input('min_length');
            echo $this->Form->input('max_length');
            echo $this->Form->input('enforce_unique', ['label' => __('Enforce unique (used for e.g. e-mail addresses which must be unique across participants - use with care!)')]);
            echo $this->Form->input('validate_as_email', ['label' => __('Validate as Email?')]);
            echo $this->Form->input('identical_to', ['options' => $freeTextFields, 'empty' => '[None]', 'label' => __('Identical to (use to validate e-mails)')]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
