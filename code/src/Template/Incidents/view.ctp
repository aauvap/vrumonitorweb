<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Incident'), ['action' => 'edit', $incident->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Incident'), ['action' => 'delete', $incident->id], ['confirm' => __('Are you sure you want to delete # {0}?', $incident->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Incidents'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('List Participants'), ['controller' => 'Participants', 'action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="incidents view large-9 medium-8 columns content">
    <h3>Incident #<?= h($incident->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Questionnaire') ?></th>
            <td><?= $this->Html->link($incident->questionnaire_id, ['controller' => 'Questionnaires', 'action' => 'view', $incident->questionnaire_id]) ?></td>
        </tr>
        <tr>
            <th><?= __('Participant') ?></th>
            <td><?= $incident->has('participant') ? $this->Html->link($incident->participant->id, ['controller' => 'Participants', 'action' => 'view', $incident->participant->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($incident->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Incident key') ?></th>
            <td><?= $incident->incident_key ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($incident->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($incident->modified) ?></tr>
        </tr>
    </table>

    <?= $this->Html->link("Go to questionnaire", ["controller" => "Questionnaires", "action" => "view", $incident->id, "?" => ['key' => $incident->incident_key]]) ?>
    
    <div class="related">
        <h4><?= __('Related Date Answers') ?></h4>
        <?php if (!empty($incident->date_answers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Incident Id') ?></th>
                <th><?= __('Date Field Id') ?></th>
                <th><?= __('Datetime') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($incident->date_answers as $dateAnswers): ?>
            <tr>
                <td><?= h($dateAnswers->id) ?></td>
                <td><?= h($dateAnswers->incident_id) ?></td>
                <td><?= h($dateAnswers->date_field_id) ?></td>
                <td><?= h($dateAnswers->datetime) ?></td>
                <td><?= h($dateAnswers->created) ?></td>
                <td><?= h($dateAnswers->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DateAnswers', 'action' => 'view', $dateAnswers->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'DateAnswers', 'action' => 'edit', $dateAnswers->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DateAnswers', 'action' => 'delete', $dateAnswers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dateAnswers->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Free Text Answers') ?></h4>
        <?php if (!empty($incident->free_text_answers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Incident Id') ?></th>
                <th><?= __('Free Text Field Id') ?></th>
                <th><?= __('Text') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($incident->free_text_answers as $freeTextAnswers): ?>
            <tr>
                <td><?= h($freeTextAnswers->id) ?></td>
                <td><?= h($freeTextAnswers->incident_id) ?></td>
                <td><?= h($freeTextAnswers->free_text_field_id) ?></td>
                <td><?= h($freeTextAnswers->text) ?></td>
                <td><?= h($freeTextAnswers->created) ?></td>
                <td><?= h($freeTextAnswers->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'FreeTextAnswers', 'action' => 'view', $freeTextAnswers->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'FreeTextAnswers', 'action' => 'edit', $freeTextAnswers->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'FreeTextAnswers', 'action' => 'delete', $freeTextAnswers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $freeTextAnswers->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Map Answers') ?></h4>
        <?php if (!empty($incident->map_answers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Incident Id') ?></th>
                <th><?= __('Map Field Id') ?></th>
                <th><?= __('Latitude') ?></th>
                <th><?= __('Longitude') ?></th>
                <th><?= __('Orientation') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($incident->map_answers as $mapAnswers): ?>
            <tr>
                <td><?= h($mapAnswers->id) ?></td>
                <td><?= h($mapAnswers->incident_id) ?></td>
                <td><?= h($mapAnswers->map_field_id) ?></td>
                <td><?= h($mapAnswers->latitude) ?></td>
                <td><?= h($mapAnswers->longitude) ?></td>
                <td><?= h($mapAnswers->orientation) ?></td>
                <td><?= h($mapAnswers->created) ?></td>
                <td><?= h($mapAnswers->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'MapAnswers', 'action' => 'view', $mapAnswers->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'MapAnswers', 'action' => 'edit', $mapAnswers->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MapAnswers', 'action' => 'delete', $mapAnswers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mapAnswers->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Multiple Choice Answers') ?></h4>
        <?php if (!empty($incident->multiple_choice_answers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Incident Id') ?></th>
                <th><?= __('Multiple Choice Option Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($incident->multiple_choice_answers as $multipleChoiceAnswers): ?>
            <tr>
                <td><?= h($multipleChoiceAnswers->id) ?></td>
                <td><?= h($multipleChoiceAnswers->incident_id) ?></td>
                <td><?= h($multipleChoiceAnswers->multiple_choice_option_id) ?></td>
                <td><?= h($multipleChoiceAnswers->created) ?></td>
                <td><?= h($multipleChoiceAnswers->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'MultipleChoiceAnswers', 'action' => 'view', $multipleChoiceAnswers->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'MultipleChoiceAnswers', 'action' => 'edit', $multipleChoiceAnswers->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MultipleChoiceAnswers', 'action' => 'delete', $multipleChoiceAnswers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $multipleChoiceAnswers->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Number Answers') ?></h4>
        <?php if (!empty($incident->number_answers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Incident Id') ?></th>
                <th><?= __('Number Field Id') ?></th>
                <th><?= __('Number') ?></th>
                <th><?= __('Float Number') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($incident->number_answers as $numberAnswers): ?>
            <tr>
                <td><?= h($numberAnswers->id) ?></td>
                <td><?= h($numberAnswers->incident_id) ?></td>
                <td><?= h($numberAnswers->number_field_id) ?></td>
                <td><?= h($numberAnswers->number) ?></td>
                <td><?= h($numberAnswers->float_number) ?></td>
                <td><?= h($numberAnswers->created) ?></td>
                <td><?= h($numberAnswers->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'NumberAnswers', 'action' => 'view', $numberAnswers->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'NumberAnswers', 'action' => 'edit', $numberAnswers->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'NumberAnswers', 'action' => 'delete', $numberAnswers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $numberAnswers->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Sensor Dumps') ?></h4>
        <?php if (!empty($incident->sensor_dumps)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Incident Id') ?></th>
                <th><?= __('Data') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($incident->sensor_dumps as $sensorDumps): ?>
            <tr>
                <td><?= h($sensorDumps->id) ?></td>
                <td><?= h($sensorDumps->incident_id) ?></td>
                <td><?= h($sensorDumps->data) ?></td>
                <td><?= h($sensorDumps->created) ?></td>
                <td><?= h($sensorDumps->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SensorDumps', 'action' => 'view', $sensorDumps->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'SensorDumps', 'action' => 'edit', $sensorDumps->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SensorDumps', 'action' => 'delete', $sensorDumps->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sensorDumps->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
