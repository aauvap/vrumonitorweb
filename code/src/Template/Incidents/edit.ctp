<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $incident->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $incident->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Participants'), ['controller' => 'Participants', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Participant'), ['controller' => 'Participants', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Date Answers'), ['controller' => 'DateAnswers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Date Answer'), ['controller' => 'DateAnswers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Free Text Answers'), ['controller' => 'FreeTextAnswers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Free Text Answer'), ['controller' => 'FreeTextAnswers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Map Answers'), ['controller' => 'MapAnswers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Map Answer'), ['controller' => 'MapAnswers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Multiple Choice Answers'), ['controller' => 'MultipleChoiceAnswers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Multiple Choice Answer'), ['controller' => 'MultipleChoiceAnswers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Number Answers'), ['controller' => 'NumberAnswers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Number Answer'), ['controller' => 'NumberAnswers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sensor Dumps'), ['controller' => 'SensorDumps', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sensor Dump'), ['controller' => 'SensorDumps', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="incidents form large-9 medium-8 columns content">
    <?= $this->Form->create($incident) ?>
    <fieldset>
        <legend><?= __('Edit Incident') ?></legend>
        <?php
            echo $this->Form->input('participant_id', ['options' => $participants]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
