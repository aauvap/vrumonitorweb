<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Participants'), ['controller' => 'Participants', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="incidents form large-9 medium-8 columns content">
    <?= $this->Form->create($incident, ['url' => [$participantId, "?" => ["key" => $participantKey]]]) ?>
    <fieldset>
        <legend><?= __('Add Incident') ?></legend>
    </fieldset>
    <?= $this->Form->button(__('Add')) ?>
    <?= $this->Form->end() ?>
</div>
