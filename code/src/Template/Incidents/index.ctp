<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Participants'), ['controller' => 'Participants', 'action' => 'index']) ?></li>
</nav>
<div class="incidents index large-9 medium-8 columns content">
    <h3><?= __('Incidents') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('participant_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($incidents as $incident): ?>
            <tr>
                <td><?= $this->Number->format($incident->id) ?></td>
                <td><?= $this->Html->link($incident->participant_id, ['controller' => 'Participants', 'action' => 'view', $incident->participant_id]) ?></td>
                <td><?= h($incident->created) ?></td>
                <td><?= h($incident->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $incident->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $incident->id]) ?>
                  <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $incident->id], ['confirm' => __('Are you sure you want to delete # {0}?', $incident->id)]) ?>
                      <?= $this->Html->link("Questionnaire", ["controller" => "Questionnaires", "action" => "view", $incident->id, "?" => ["key" => $incident->incident_key]]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
