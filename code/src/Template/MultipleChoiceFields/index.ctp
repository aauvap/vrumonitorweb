<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Multiple Choice Field'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Multiple Choice Options'), ['controller' => 'MultipleChoiceOptions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Multiple Choice Option'), ['controller' => 'MultipleChoiceOptions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="multipleChoiceFields index large-9 medium-8 columns content">
    <h3><?= __('Multiple Choice Fields') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('question_id') ?></th>
                <th><?= $this->Paginator->sort('caption') ?></th>
                <th><?= $this->Paginator->sort('min_num_choices') ?></th>
                <th><?= $this->Paginator->sort('max_num_choices') ?></th>
                <th><?= $this->Paginator->sort('priority') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($multipleChoiceFields as $multipleChoiceField): ?>
            <tr>
                <td><?= $this->Number->format($multipleChoiceField->id) ?></td>
                <td><?= $multipleChoiceField->has('question') ? $this->Html->link($multipleChoiceField->question->id, ['controller' => 'Questions', 'action' => 'view', $multipleChoiceField->question->id]) : '' ?></td>
                <td><?= h($multipleChoiceField->caption) ?></td>
                <td><?= $this->Number->format($multipleChoiceField->min_num_choices) ?></td>
                <td><?= $this->Number->format($multipleChoiceField->max_num_choices) ?></td>
                <td><?= $this->Number->format($multipleChoiceField->priority) ?></td>
                <td><?= h($multipleChoiceField->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $multipleChoiceField->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $multipleChoiceField->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $multipleChoiceField->id], ['confirm' => __('Are you sure you want to delete # {0}?', $multipleChoiceField->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
