<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Multiple Choice Field'), ['action' => 'edit', $multipleChoiceField->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Multiple Choice Field'), ['action' => 'delete', $multipleChoiceField->id], ['confirm' => __('Are you sure you want to delete # {0}?', $multipleChoiceField->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Multiple Choice Fields'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Multiple Choice Field'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Multiple Choice Options'), ['controller' => 'MultipleChoiceOptions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Multiple Choice Option'), ['controller' => 'MultipleChoiceOptions', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="multipleChoiceFields view large-9 medium-8 columns content">
    <h3><?= h($multipleChoiceField->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Question') ?></th>
            <td><?= $multipleChoiceField->has('question') ? $this->Html->link($multipleChoiceField->question->id, ['controller' => 'Questions', 'action' => 'view', $multipleChoiceField->question->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Caption') ?></th>
            <td><?= h($multipleChoiceField->caption) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($multipleChoiceField->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Min Num Choices') ?></th>
            <td><?= $this->Number->format($multipleChoiceField->min_num_choices) ?></td>
        </tr>
        <tr>
            <th><?= __('Max Num Choices') ?></th>
            <td><?= $this->Number->format($multipleChoiceField->max_num_choices) ?></td>
        </tr>
        <tr>
            <th><?= __('Priority') ?></th>
            <td><?= $this->Number->format($multipleChoiceField->priority) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($multipleChoiceField->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($multipleChoiceField->modified) ?></tr>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Multiple Choice Options') ?></h4>
        <?php if (!empty($multipleChoiceField->multiple_choice_options)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Multiple Choice Field Id') ?></th>
                <th><?= __('Caption') ?></th>
                <th><?= __('Picture') ?></th>
                <th><?= __('Priority') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($multipleChoiceField->multiple_choice_options as $multipleChoiceOptions): ?>
            <tr>
                <td><?= h($multipleChoiceOptions->id) ?></td>
                <td><?= h($multipleChoiceOptions->multiple_choice_field_id) ?></td>
                <td><?= h($multipleChoiceOptions->caption) ?></td>
                <td><?= h($multipleChoiceOptions->picture) ?></td>
                <td><?= h($multipleChoiceOptions->priority) ?></td>
                <td><?= h($multipleChoiceOptions->created) ?></td>
                <td><?= h($multipleChoiceOptions->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'MultipleChoiceOptions', 'action' => 'view', $multipleChoiceOptions->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'MultipleChoiceOptions', 'action' => 'edit', $multipleChoiceOptions->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MultipleChoiceOptions', 'action' => 'delete', $multipleChoiceOptions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $multipleChoiceOptions->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
