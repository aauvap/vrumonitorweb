<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li><?php
          if(!is_null($question_id)) {
              echo "<li>".$this->Html->link(__('Back to question'), ['controller' => 'Questions', 'action' => 'edit', $question_id])."</li>";
          }
       ?>
    </ul>
</nav>
<div class="multipleChoiceFields form large-9 medium-8 columns content">
    <?= $this->Form->create($field) ?>
    <fieldset>
        <legend><?= __('Add Multiple Choice Field') ?></legend>
        <?php
            if(is_null($question_id)) {
                echo $this->Form->input('questions', ['label' => __('Question')]);
            }
        echo $this->Form->input('caption', ['label' => __('Caption (leave empty if no sub-question)')]);
        echo $this->Form->input('internal_description');
            echo $this->Form->input('min_num_choices');
            echo $this->Form->input('max_num_choices');
            echo $this->Form->input('max_image_size');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
