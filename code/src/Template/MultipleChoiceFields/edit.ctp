<script type="text/javascript">
 var sortableSettings = [{uiElement: "#sortableOptions", url: appBaseUrl+'multipleChoiceFields/sortOptions/'}];
</script>
<?= $this->Html->script('questionsUI'); ?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
      <li class="heading"><?= __('Actions') ?></li>
      <li><?= $this->Html->link(__('Back to questionnaire'), ['controller' => 'Questionnaires', 'action' => 'edit', $field->question->questionnaire_id]) ?></li>
        <li><?= $this->Html->link(__('Back to question'), ['controller' => 'Questions', 'action' => 'edit', $field->question_id]) ?></li>
    </ul>
</nav>
<div class="multipleChoiceFields form large-9 medium-8 columns content">
  <?= $this->Form->create($field) ?>
  
    <?= $this->Form->button(__('Save')) ?>
    <fieldset>
        <legend><?= __('Edit Multiple Choice Field') ?></legend>
        <?php
            echo $this->Form->input('caption');
            echo $this->Form->input('internal_description');
            echo $this->Form->input('min_num_choices');
            echo $this->Form->input('max_num_choices');
            echo $this->Form->input('max_image_size');
        ?>
    </fieldset>
    <?= $this->Form->end() ?>

    <h4><?= __('Options') ?></h4>  
    <?php
      if (!empty($options)): ?>
        
        <table cellpadding="0" cellspacing="0">
          <thead>
            <tr>
              <th style="width: 40px;"></th>
          <th><?= __('Caption (internal description in italic)') ?></th>
              <th class="actions"><?= __('Actions') ?></th>
            </tr>
          </thead>
          <tbody id="sortableOptions" class="clickable">
            <?php foreach ($options as $option): ?>
              <tr id="optionOrder_<?= $field->id.":".$option->id; ?>"
                  onclick="javascript:location.href='<?= $this->Url->build(['controller' => $option->source(), 'action' => 'edit', $option->id]); ?>'">
                <td><?php
                    if(!empty($option->picture)) {
                        echo $this->Html->image('../files/multiplechoiceoptions/picture/'.
                                                $option->picture_dir.
                                                '/smallSquare_'.$option->picture);
                    }
                ?></td>
                <td><?= h($option->caption) ?> <i><?= h($option->internal_description) ?></i></td>
                <td class="actions">
                  <?= $this->Form->postLink(__('Delete'), ['controller' => $option->source(), 'action' => 'delete', $option->id], ['confirm' => __('Are you sure you want to delete this field?')]) ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        <?php //debug($options); ?>
      <?php endif; ?>
      <?php
          echo $this->Html->Link(__('Add option...'), ['controller' => 'MultipleChoiceOptions', 'action' => 'add', $field->id]);
              ?>
    
    <div class="related">
    <h4><?= __("Information") ?></h4>
    <table class="vertical-table">
        <tr>
            <th><?= __('Field Id') ?></th>
            <td><?= $this->Number->format($field->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Question') ?></th>
            <td><?= $this->Html->link($field->question_id, ['controller' => 'Questions', 'action' => 'edit', $field->question_id]) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($field->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($field->modified) ?></tr>
        </tr>
    </table>
    </div>
</div>
