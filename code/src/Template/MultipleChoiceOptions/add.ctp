<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li><?php
          if(!is_null($field_id)) {
              echo "<li>".$this->Html->link(__('Back to field'), ['controller' => 'MultipleChoiceFields', 'action' => 'edit', $field_id])."</li>";
          }
       ?>
    </ul>
</nav>
<div class="multipleChoiceOptions form large-9 medium-8 columns content">
  <?php if(is_null($field_id)) {echo "Field id not set";} ?>
    <?= $this->Form->create($option, ['type' => 'file']) ?>
    <fieldset>
        <legend><?= __('Add Multiple Choice Option') ?></legend>
        <?php
            if(is_null($field_id)) {
                // echo $this->Form->input('incident_id', []);
                echo $this->Form->input('field_id', ['label' => __('Field'), 'options' => $multipleChoiceFields]);
            }
            echo $this->Form->input('caption');
            echo $this->Form->input('internal_description');
            echo $this->Form->input('allows_move_on');
        
            if(!($multiple_choice_field->min_num_choices == 1 and $multiple_choice_field->max_num_choices == 1)) {
                echo $this->Form->input('exclusive', ['label' => __('Exclusive option (does not allow selection of other options)')]);
            }
            echo $this->Form->input('picture', ['type' => 'file']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
