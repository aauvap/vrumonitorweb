<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Multiple Choice Option'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="multipleChoiceOptions index large-9 medium-8 columns content">
    <h3><?= __('Multiple Choice Options') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('question_id') ?></th>
                <th><?= $this->Paginator->sort('caption') ?></th>
                <th><?= $this->Paginator->sort('picture') ?></th>
                <th><?= $this->Paginator->sort('priority') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($multipleChoiceOptions as $multipleChoiceOption): ?>
            <tr>
                <td><?= $this->Number->format($multipleChoiceOption->id) ?></td>
                <td><?= $multipleChoiceOption->has('question') ? $this->Html->link($multipleChoiceOption->question->id, ['controller' => 'Questions', 'action' => 'view', $multipleChoiceOption->question->id]) : '' ?></td>
                <td><?= h($multipleChoiceOption->caption) ?></td>
                <td><?= h($multipleChoiceOption->picture) ?></td>
                <td><?= $this->Number->format($multipleChoiceOption->priority) ?></td>
                <td><?= h($multipleChoiceOption->created) ?></td>
                <td><?= h($multipleChoiceOption->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $multipleChoiceOption->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $multipleChoiceOption->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $multipleChoiceOption->id], ['confirm' => __('Are you sure you want to delete # {0}?', $multipleChoiceOption->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
