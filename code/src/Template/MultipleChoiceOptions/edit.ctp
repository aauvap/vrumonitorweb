<?php
$this->loadHelper('Inflector');
$this->Html->script('multipleChoiceOptionsUI', ['block' => true]);
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $option->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $option->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('<<Back to field'), ['action' => 'edit', 'controller' => 'multiple_choice_fields', $field_id]) ?></li>
    </ul>
</nav>
<div class="multipleChoiceOptions form large-9 medium-8 columns content">
    <?= $this->Form->create($option, ['type' => 'file']) ?>
    <fieldset>
        <legend><?= __('Edit Multiple Choice Option') ?></legend>
        <?php
        echo $this->Form->input('caption');
        echo $this->Form->input('internal_description');
        echo $this->Form->input('allows_move_on');
        if(!($option->multiple_choice_field->min_num_choices == 1 and $option->multiple_choice_field->max_num_choices == 1)) {
            echo $this->Form->input('exclusive', ['label' => __('Exclusive option (does not allow selection of other options)')]);
        }
        echo $this->Form->input('picture', ['type' => 'file']);
        if(!empty($option->picture)) {
            echo $this->Form->hidden('keepPicture', ['value' => 'true']);
            echo $this->Html->link($this->Html->image('icons/cross.png').__("Remove picture"), '#', ['escape' => false, 'id' => 'removePicture', 'onclick' => "javascript:removePicture('".__("Are you sure you want to delete the picture?")."');"]);
            echo "<br />";
            echo $this->Html->image('../files/multiplechoiceoptions/picture/'.$option->picture_dir.'/square_'.$option->picture, ['id' => 'optionPicture']);
        } else {
            echo $this->Form->hidden('keepPicture', ['value' => 'file']);
        }
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
