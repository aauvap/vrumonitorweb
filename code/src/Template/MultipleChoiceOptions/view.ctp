<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Multiple Choice Option'), ['action' => 'edit', $multipleChoiceOption->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Multiple Choice Option'), ['action' => 'delete', $multipleChoiceOption->id], ['confirm' => __('Are you sure you want to delete # {0}?', $multipleChoiceOption->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Multiple Choice Options'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Multiple Choice Option'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="multipleChoiceOptions view large-9 medium-8 columns content">
    <h3><?= h($multipleChoiceOption->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Question') ?></th>
            <td><?= $multipleChoiceOption->has('question') ? $this->Html->link($multipleChoiceOption->question->id, ['controller' => 'Questions', 'action' => 'view', $multipleChoiceOption->question->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Caption') ?></th>
            <td><?= h($multipleChoiceOption->caption) ?></td>
        </tr>
        <tr>
            <th><?= __('Picture') ?></th>
            <td><?= h($multipleChoiceOption->picture) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($multipleChoiceOption->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Priority') ?></th>
            <td><?= $this->Number->format($multipleChoiceOption->priority) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($multipleChoiceOption->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($multipleChoiceOption->modified) ?></tr>
        </tr>
    </table>
</div>
