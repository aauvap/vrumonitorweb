<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$appTitle = 'VRUMonitor';
if($_SERVER['HTTP_HOST'] == 'vru.create.aau.dk') {
  $appTitle = 'InDeV';
} else if($_SERVER['HTTP_HOST'] == 'trafikforskning.aau.dk') {
  $appTitle = 'Trafikforskning';
}
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $appTitle ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('vruDefault.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <script type="text/javascript">var appBaseUrl = '<?= $this->Url->build('/', true); ?>';</script>
    <?= $this->Html->script('jquery-2.1.4.min'); ?>
    <?= $this->Html->script('jquery-ui.min'); ?>
    <?= $this->Html->script('vruDefault'); ?>
    <?= $this->fetch('script') ?>

</head>
<body>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <h1><a href="">
                  <?php 
                    $action = $this->request->params['action'];
                    if(($action == 'edit' or $action == 'add' or $action == 'index')) {
                      echo $this->fetch('title');
                    }
                  ?>
                </a></h1>
            </li>
        </ul>
        <section class="top-bar-section">
            <ul class="right">
              <?php
              if($this->request->session()->read('Auth.User')):
               ?>
              <li><?= $this->Html->link('Admin users', ['controller' => 'Users', 'action' => 'index']); ?></li>
              <li><?= $this->Html->link('Questionnaires', ['controller' => 'Questionnaires', 'action' => 'index']); ?></li>
              <li><?= $this->Html->link('Participants', ['controller' => 'Participants', 'action' => 'index']); ?></li>
              <li><?= $this->Html->link('Log out', ['controller' => 'Users', 'action' => 'logout']); ?></li>
              <?php
              endif;
              ?>
            </ul>
        </section>
    </nav>
    <?= $this->Flash->render() ?>
    <section class="container clearfix">
        <?= $this->fetch('content') ?>
    </section>
    <footer>
      <?= $this->Html->image('logo.png', ['style' => 'width: 200px; float: center;']) ?>
      <?php 
      if($_SERVER['HTTP_HOST'] == 'vru.create.aau.dk'): ?>
      <p class = "footer">
        <?= __("This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 635895")?>
      </p>
      <?php endif ?>
    </footer>
</body>
</html>
