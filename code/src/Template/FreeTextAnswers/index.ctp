<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Free Text Answer'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Free Text Fields'), ['controller' => 'FreeTextFields', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Free Text Field'), ['controller' => 'FreeTextFields', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="freeTextAnswers index large-9 medium-8 columns content">
    <h3><?= __('Free Text Answers') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('incident_id') ?></th>
                <th><?= $this->Paginator->sort('free_text_field_id') ?></th>
                <th><?= $this->Paginator->sort('text') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($freeTextAnswers as $freeTextAnswer): ?>
            <tr>
                <td><?= $this->Number->format($freeTextAnswer->id) ?></td>
                <td><?= $freeTextAnswer->has('incident') ? $this->Html->link($freeTextAnswer->incident->id, ['controller' => 'Incidents', 'action' => 'view', $freeTextAnswer->incident->id]) : '' ?></td>
                <td><?= $freeTextAnswer->has('free_text_field') ? $this->Html->link($freeTextAnswer->free_text_field->id, ['controller' => 'FreeTextFields', 'action' => 'view', $freeTextAnswer->free_text_field->id]) : '' ?></td>
                <td><?= h($freeTextAnswer->text) ?></td>
                <td><?= h($freeTextAnswer->created) ?></td>
                <td><?= h($freeTextAnswer->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $freeTextAnswer->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $freeTextAnswer->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $freeTextAnswer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $freeTextAnswer->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
