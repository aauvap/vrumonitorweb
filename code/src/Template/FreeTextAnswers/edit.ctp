<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $freeTextAnswer->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $freeTextAnswer->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Free Text Answers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Free Text Fields'), ['controller' => 'FreeTextFields', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Free Text Field'), ['controller' => 'FreeTextFields', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="freeTextAnswers form large-9 medium-8 columns content">
    <?= $this->Form->create($freeTextAnswer) ?>
    <fieldset>
        <legend><?= __('Edit Free Text Answer') ?></legend>
        <?php
            echo $this->Form->input('incident_id', ['options' => $incidents]);
            echo $this->Form->input('free_text_field_id', ['options' => $freeTextFields]);
            echo $this->Form->input('text');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
