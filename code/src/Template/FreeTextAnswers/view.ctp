<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Free Text Answer'), ['action' => 'edit', $freeTextAnswer->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Free Text Answer'), ['action' => 'delete', $freeTextAnswer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $freeTextAnswer->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Free Text Answers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Free Text Answer'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('List Free Text Fields'), ['controller' => 'FreeTextFields', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Free Text Field'), ['controller' => 'FreeTextFields', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="freeTextAnswers view large-9 medium-8 columns content">
    <h3><?= h($freeTextAnswer->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Incident') ?></th>
            <td><?= $freeTextAnswer->has('incident') ? $this->Html->link($freeTextAnswer->incident->id, ['controller' => 'Incidents', 'action' => 'view', $freeTextAnswer->incident->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Free Text Field') ?></th>
            <td><?= $freeTextAnswer->has('free_text_field') ? $this->Html->link($freeTextAnswer->free_text_field->id, ['controller' => 'FreeTextFields', 'action' => 'view', $freeTextAnswer->free_text_field->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Text') ?></th>
            <td><?= h($freeTextAnswer->text) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($freeTextAnswer->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($freeTextAnswer->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($freeTextAnswer->modified) ?></tr>
        </tr>
    </table>
</div>
