<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
    </ul>
</nav>
<div class="questionSequences index large-9 medium-8 columns content">
    <h3><?= __('Question Sequences') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('question_id') ?></th>
                <th><?= $this->Paginator->sort('to_question_id') ?></th>
                <th><?= $this->Paginator->sort('priority') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($questionSequences as $questionSequence): ?>
            <tr>
                <td><?= $this->Number->format($questionSequence->id) ?></td>
                <td><?= $this->Number->format($questionSequence->question_id) ?></td>
                <td><?= $questionSequence->has('question') ? $this->Html->link($questionSequence->question->id, ['controller' => 'Questions', 'action' => 'view', $questionSequence->question->id]) : '' ?></td>
                <td><?= $this->Number->format($questionSequence->priority) ?></td>
                <td><?= h($questionSequence->created) ?></td>
                <td><?= h($questionSequence->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $questionSequence->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $questionSequence->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $questionSequence->id], ['confirm' => __('Are you sure you want to delete # {0}?', $questionSequence->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
