<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Question Sequence'), ['action' => 'edit', $questionSequence->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Question Sequence'), ['action' => 'delete', $questionSequence->id], ['confirm' => __('Are you sure you want to delete # {0}?', $questionSequence->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Question Sequences'), ['action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="questionSequences view large-9 medium-8 columns content">
    <h3><?= h($questionSequence->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Question') ?></th>
            <td><?= $questionSequence->has('question') ? $this->Html->link($questionSequence->question->id, ['controller' => 'Questions', 'action' => 'view', $questionSequence->question->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($questionSequence->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Question Id') ?></th>
            <td><?= $this->Number->format($questionSequence->question_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Priority') ?></th>
            <td><?= $this->Number->format($questionSequence->priority) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($questionSequence->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($questionSequence->modified) ?></tr>
        </tr>
    </table>
</div>
