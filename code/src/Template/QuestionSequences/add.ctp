<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Question Sequences'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="questionSequences form large-9 medium-8 columns content">
    <?= $this->Form->create($questionSequence) ?>
    <fieldset>
        <legend><?= __('Add Question Sequence') ?></legend>
        <?php
            echo $this->Form->input('question_id');
            echo $this->Form->input('to_question_id', ['options' => $questions, 'empty' => true]);
            echo $this->Form->input('priority');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
