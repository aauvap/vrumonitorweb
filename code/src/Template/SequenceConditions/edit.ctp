<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $sequenceCondition->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $sequenceCondition->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Sequence  Conditions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Question Sequences'), ['controller' => 'QuestionSequences', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Multiple Choice Options'), ['controller' => 'MultipleChoiceOptions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Multiple Choice Option'), ['controller' => 'MultipleChoiceOptions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sequenceConditions form large-9 medium-8 columns content">
    <?= $this->Form->create($sequenceCondition) ?>
    <fieldset>
        <legend><?= __('Edit Sequence  Condition') ?></legend>
        <?php
            echo $this->Form->input('question_sequence_id', ['options' => $questionSequences]);
            echo $this->Form->input('multiple_choice_option_id', ['options' => $multipleChoiceOptions]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
