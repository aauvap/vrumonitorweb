<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sequence  Condition'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Question Sequences'), ['controller' => 'QuestionSequences', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Multiple Choice Options'), ['controller' => 'MultipleChoiceOptions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Multiple Choice Option'), ['controller' => 'MultipleChoiceOptions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sequenceConditions index large-9 medium-8 columns content">
    <h3><?= __('Sequence  Conditions') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('question_sequence_id') ?></th>
                <th><?= $this->Paginator->sort('multiple_choice_option_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sequenceConditions as $sequenceCondition): ?>
            <tr>
                <td><?= $this->Number->format($sequenceCondition->id) ?></td>
                <td><?= $sequenceCondition->has('question_sequence') ? $this->Html->link($sequenceCondition->question_sequence->id, ['controller' => 'QuestionSequences', 'action' => 'view', $sequenceCondition->question_sequence->id]) : '' ?></td>
                <td><?= $sequenceCondition->has('multiple_choice_option') ? $this->Html->link($sequenceCondition->multiple_choice_option->id, ['controller' => 'MultipleChoiceOptions', 'action' => 'view', $sequenceCondition->multiple_choice_option->id]) : '' ?></td>
                <td><?= h($sequenceCondition->created) ?></td>
                <td><?= h($sequenceCondition->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $sequenceCondition->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sequenceCondition->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sequenceCondition->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sequenceCondition->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
