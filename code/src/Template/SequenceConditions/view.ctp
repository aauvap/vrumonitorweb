<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sequence Condition'), ['action' => 'edit', $sequenceAndCondition->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sequence Condition'), ['action' => 'delete', $sequenceCondition->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sequenceCondition->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sequence  Conditions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sequence  Condition'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Question Sequences'), ['controller' => 'QuestionSequences', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('List Multiple Choice Options'), ['controller' => 'MultipleChoiceOptions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Multiple Choice Option'), ['controller' => 'MultipleChoiceOptions', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="sequenceConditions view large-9 medium-8 columns content">
    <h3><?= h($sequenceCondition->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Question Sequence') ?></th>
            <td><?= $sequenceCondition->has('question_sequence') ? $this->Html->link($sequenceCondition->question_sequence->id, ['controller' => 'QuestionSequences', 'action' => 'view', $sequenceCondition->question_sequence->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Multiple Choice Option') ?></th>
            <td><?= $sequenceCondition->has('multiple_choice_option') ? $this->Html->link($sequenceCondition->multiple_choice_option->id, ['controller' => 'MultipleChoiceOptions', 'action' => 'view', $sequenceCondition->multiple_choice_option->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($sequenceCondition->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($sequenceCondition->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($sequenceCondition->modified) ?></tr>
        </tr>
    </table>
</div>
