<div class="questionnaires view large-9 medium-8 columns content">
    <h3><?= h($questionnaire->title) ?></h3>
    <p><?= $this->Markdown->transform($questionnaire->intro_text) ?></p>
    <?= $this->Form->create() ?>
    <?= $this->Form->hidden('begin', ['value' => true]);?>
    <?= $this->Form->button($this->Html->image('icons/arrow.png'), ['escape' => false]) ?>
    <?= $this->Form->end() ?>
</div>
