<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Number Answer'), ['action' => 'edit', $numberAnswer->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Number Answer'), ['action' => 'delete', $numberAnswer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $numberAnswer->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Number Answers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Number Answer'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('List Number Fields'), ['controller' => 'NumberFields', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Number Field'), ['controller' => 'NumberFields', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="numberAnswers view large-9 medium-8 columns content">
    <h3><?= h($numberAnswer->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Incident') ?></th>
            <td><?= $numberAnswer->has('incident') ? $this->Html->link($numberAnswer->incident->id, ['controller' => 'Incidents', 'action' => 'view', $numberAnswer->incident->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Number Field') ?></th>
            <td><?= $numberAnswer->has('number_field') ? $this->Html->link($numberAnswer->number_field->id, ['controller' => 'NumberFields', 'action' => 'view', $numberAnswer->number_field->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($numberAnswer->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Number') ?></th>
            <td><?= $this->Number->format($numberAnswer->number) ?></td>
        </tr>
        <tr>
            <th><?= __('Float Number') ?></th>
            <td><?= $this->Number->format($numberAnswer->float_number) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($numberAnswer->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($numberAnswer->modified) ?></tr>
        </tr>
    </table>
</div>
