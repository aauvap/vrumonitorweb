<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Number Answer'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Number Fields'), ['controller' => 'NumberFields', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Number Field'), ['controller' => 'NumberFields', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="numberAnswers index large-9 medium-8 columns content">
    <h3><?= __('Number Answers') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('incident_id') ?></th>
                <th><?= $this->Paginator->sort('number_field_id') ?></th>
                <th><?= $this->Paginator->sort('number') ?></th>
                <th><?= $this->Paginator->sort('float_number') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($numberAnswers as $numberAnswer): ?>
            <tr>
                <td><?= $this->Number->format($numberAnswer->id) ?></td>
                <td><?= $numberAnswer->has('incident') ? $this->Html->link($numberAnswer->incident->id, ['controller' => 'Incidents', 'action' => 'view', $numberAnswer->incident->id]) : '' ?></td>
                <td><?= $numberAnswer->has('number_field') ? $this->Html->link($numberAnswer->number_field->id, ['controller' => 'NumberFields', 'action' => 'view', $numberAnswer->number_field->id]) : '' ?></td>
                <td><?= $this->Number->format($numberAnswer->number) ?></td>
                <td><?= $this->Number->format($numberAnswer->float_number) ?></td>
                <td><?= h($numberAnswer->created) ?></td>
                <td><?= h($numberAnswer->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $numberAnswer->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $numberAnswer->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $numberAnswer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $numberAnswer->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
