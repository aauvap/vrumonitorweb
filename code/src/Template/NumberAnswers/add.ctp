<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Number Answers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Number Fields'), ['controller' => 'NumberFields', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Number Field'), ['controller' => 'NumberFields', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="numberAnswers form large-9 medium-8 columns content">
    <?= $this->Form->create($numberAnswer) ?>
    <fieldset>
        <legend><?= __('Add Number Answer') ?></legend>
        <?php
            echo $this->Form->input('incident_id', ['options' => $incidents]);
            echo $this->Form->input('number_field_id', ['options' => $numberFields]);
            echo $this->Form->input('number');
            echo $this->Form->input('float_number');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
