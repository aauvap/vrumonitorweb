<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Back to Participants'), ['controller' => 'Participants']) ?></li>
    </ul>
</nav>
<div class="appSettings form large-9 medium-8 columns content">
    <?= $this->Form->create($appSetting) ?>
    <fieldset>
        <legend><?= __('Edit App Settings') ?></legend>
        <?php
            echo $this->Form->input('settings', ['type' => 'textarea']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
