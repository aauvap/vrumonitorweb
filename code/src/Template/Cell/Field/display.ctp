<style type="text/css">
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        /* display: none; <- Crashes Chrome on hover */
        -webkit-appearance: none;
        margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
    }
</style>
<?php
$options = ['legend' => $caption];
if($map) {
    $options['fieldset'] = false;
    echo "<fieldset>\n";
    echo $this->Html->css('map')."\n";
    echo $this->Html->css('leaflet')."\n";
    echo $this->Html->script('leaflet')."\n";
    echo $this->Html->script('https://maps.google.com/maps/api/js?v=3')."\n";
    echo $this->Html->script('Google')."\n";
    ?>
    <legend><?= $caption ?></legend>
    <div id="map"></div>
    <script type="text/javascript">
    <?php
    if(!is_null($map_bounding_box) && !empty($map_bounding_box)) {
        ?>
        var map_bounding_box = [<?= $map_bounding_box ?>];
        <?php
    } else {
        ?>
        var map_bounding_box = null;
        <?php
    }
    ?>            
    </script>
    <?= $this->Html->script('mapUI')."\n"; ?>
    </fieldset>
<?php
}
echo '<script type="text/javascript">';
echo 'if (typeof fieldRequirements === "undefined") {';
echo 'var fieldRequirements = {"min": [], "max": []};';
echo '}';
foreach($fields as $i => $f) {
    if(strpos($i, "MultipleChoiceFields") !== false) {
        //echo 'var fieldRequirements = {};';
        $min = null;
        $max = null;
        if($f['min_num_choices'] != null) {
            echo "fieldRequirements.min[".explode("[", $i)[1]." = ".$f['min_num_choices'].";";
            $min = $f['min_num_choices'];
        }
        if($f['max_num_choices'] != null) {
            echo "fieldRequirements.max[".explode("[", $i)[1]." = ".$f['max_num_choices'].";";
            $max = $f['max_num_choices'];
        }
        if($min != null) {
            echo 'var minError = "' . __('Error. You must check at least %d checkboxes.').'";';
        }
        if($max != null) {
            echo 'var maxError = "' . sprintf(__('Error. You can check between %d and %d.'),$min, $max).'";';
        }
        foreach($f['options'] as $j => $o) {
            $fields[$i]['options'][$j]['text'] = str_replace("<p>", "", str_replace("</p>", "", $this->Markdown->transform($o['text'])));
        }
    }
}
echo '</script>';
echo $this->Form->inputs($fields, $options, ['escape' => false])."\n";
?>
