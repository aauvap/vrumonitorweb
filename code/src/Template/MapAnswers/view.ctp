<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Map Answer'), ['action' => 'edit', $mapAnswer->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Map Answer'), ['action' => 'delete', $mapAnswer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mapAnswer->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Map Answers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Map Answer'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('List Map Fields'), ['controller' => 'MapFields', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Map Field'), ['controller' => 'MapFields', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="mapAnswers view large-9 medium-8 columns content">
    <h3><?= h($mapAnswer->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Incident') ?></th>
            <td><?= $mapAnswer->has('incident') ? $this->Html->link($mapAnswer->incident->id, ['controller' => 'Incidents', 'action' => 'view', $mapAnswer->incident->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Map Field') ?></th>
            <td><?= $mapAnswer->has('map_field') ? $this->Html->link($mapAnswer->map_field->id, ['controller' => 'MapFields', 'action' => 'view', $mapAnswer->map_field->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($mapAnswer->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Latitude') ?></th>
            <td><?= $this->Number->format($mapAnswer->latitude) ?></td>
        </tr>
        <tr>
            <th><?= __('Longitude') ?></th>
            <td><?= $this->Number->format($mapAnswer->longitude) ?></td>
        </tr>
        <tr>
            <th><?= __('Orientation') ?></th>
            <td><?= $this->Number->format($mapAnswer->orientation) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($mapAnswer->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($mapAnswer->modified) ?></tr>
        </tr>
    </table>
</div>
