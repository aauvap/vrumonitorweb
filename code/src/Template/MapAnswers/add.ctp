<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Map Answers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Map Fields'), ['controller' => 'MapFields', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Map Field'), ['controller' => 'MapFields', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="mapAnswers form large-9 medium-8 columns content">
    <?= $this->Form->create($mapAnswer) ?>
    <fieldset>
        <legend><?= __('Add Map Answer') ?></legend>
        <?php
            echo $this->Form->input('incident_id', ['options' => $incidents]);
            echo $this->Form->input('map_field_id', ['options' => $mapFields]);
            echo $this->Form->input('latitude');
            echo $this->Form->input('longitude');
            echo $this->Form->input('orientation');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
