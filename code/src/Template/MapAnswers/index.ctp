<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Map Answer'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Map Fields'), ['controller' => 'MapFields', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Map Field'), ['controller' => 'MapFields', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="mapAnswers index large-9 medium-8 columns content">
    <h3><?= __('Map Answers') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('incident_id') ?></th>
                <th><?= $this->Paginator->sort('map_field_id') ?></th>
                <th><?= $this->Paginator->sort('latitude') ?></th>
                <th><?= $this->Paginator->sort('longitude') ?></th>
                <th><?= $this->Paginator->sort('orientation') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($mapAnswers as $mapAnswer): ?>
            <tr>
                <td><?= $this->Number->format($mapAnswer->id) ?></td>
                <td><?= $mapAnswer->has('incident') ? $this->Html->link($mapAnswer->incident->id, ['controller' => 'Incidents', 'action' => 'view', $mapAnswer->incident->id]) : '' ?></td>
                <td><?= $mapAnswer->has('map_field') ? $this->Html->link($mapAnswer->map_field->id, ['controller' => 'MapFields', 'action' => 'view', $mapAnswer->map_field->id]) : '' ?></td>
                <td><?= $this->Number->format($mapAnswer->latitude) ?></td>
                <td><?= $this->Number->format($mapAnswer->longitude) ?></td>
                <td><?= $this->Number->format($mapAnswer->orientation) ?></td>
                <td><?= h($mapAnswer->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $mapAnswer->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mapAnswer->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mapAnswer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mapAnswer->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
