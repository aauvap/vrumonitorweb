<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Number Field'), ['action' => 'edit', $numberField->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Number Field'), ['action' => 'delete', $numberField->id], ['confirm' => __('Are you sure you want to delete # {0}?', $numberField->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Number Fields'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Number Field'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="numberFields view large-9 medium-8 columns content">
    <h3><?= h($numberField->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Question') ?></th>
            <td><?= $numberField->has('question') ? $this->Html->link($numberField->question->id, ['controller' => 'Questions', 'action' => 'view', $numberField->question->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Caption') ?></th>
            <td><?= h($numberField->caption) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($numberField->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Min Number') ?></th>
            <td><?= $this->Number->format($numberField->min_number) ?></td>
        </tr>
        <tr>
            <th><?= __('Max Number') ?></th>
            <td><?= $this->Number->format($numberField->max_number) ?></td>
        </tr>
        <tr>
            <th><?= __('Priority') ?></th>
            <td><?= $this->Number->format($numberField->priority) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($numberField->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($numberField->modified) ?></tr>
        </tr>
        <tr>
            <th><?= __('Allow Floats') ?></th>
            <td><?= $numberField->allow_floats ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
</div>
