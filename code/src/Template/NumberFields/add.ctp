<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li><?php
          if(!is_null($question_id)) {
              echo "<li>".$this->Html->link(__('Back to question'), ['controller' => 'Questions', 'action' => 'edit', $question_id])."</li>";
          }
       ?>
    </ul>
</nav>
<div class="numberFields form large-9 medium-8 columns content">
    <?= $this->Form->create($field) ?>
    <fieldset>
        <legend><?= __('Add Number Field') ?></legend>
        <?php
            if(is_null($question_id)) {
                echo $this->Form->input('questions', ['label' => __('Question')]);
            }
            echo $this->Form->input('caption');
            echo $this->Form->input('min_number');
            echo $this->Form->input('max_number');
            echo $this->Form->input('dropdown');
            echo $this->Form->input('allow_floats');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
