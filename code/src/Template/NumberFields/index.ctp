<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Number Field'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="numberFields index large-9 medium-8 columns content">
    <h3><?= __('Number Fields') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('question_id') ?></th>
                <th><?= $this->Paginator->sort('caption') ?></th>
                <th><?= $this->Paginator->sort('min_number') ?></th>
                <th><?= $this->Paginator->sort('max_number') ?></th>
                <th><?= $this->Paginator->sort('allow_floats') ?></th>
                <th><?= $this->Paginator->sort('priority') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($numberFields as $numberField): ?>
            <tr>
                <td><?= $this->Number->format($numberField->id) ?></td>
                <td><?= $numberField->has('question') ? $this->Html->link($numberField->question->id, ['controller' => 'Questions', 'action' => 'view', $numberField->question->id]) : '' ?></td>
                <td><?= h($numberField->caption) ?></td>
                <td><?= $this->Number->format($numberField->min_number) ?></td>
                <td><?= $this->Number->format($numberField->max_number) ?></td>
                <td><?= h($numberField->allow_floats) ?></td>
                <td><?= $this->Number->format($numberField->priority) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $numberField->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $numberField->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $numberField->id], ['confirm' => __('Are you sure you want to delete # {0}?', $numberField->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
