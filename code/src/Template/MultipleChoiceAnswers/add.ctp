<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Multiple Choice Answers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Multiple Choice Options'), ['controller' => 'MultipleChoiceOptions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Multiple Choice Option'), ['controller' => 'MultipleChoiceOptions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="multipleChoiceAnswers form large-9 medium-8 columns content">
    <?= $this->Form->create($multipleChoiceAnswer) ?>
    <fieldset>
        <legend><?= __('Add Multiple Choice Answer') ?></legend>
        <?php
            echo $this->Form->input('incident_id', ['options' => $incidents]);
            echo $this->Form->input('multiple_choice_option_id', ['options' => $multipleChoiceOptions]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
