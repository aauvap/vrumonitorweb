<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Multiple Choice Answer'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Multiple Choice Options'), ['controller' => 'MultipleChoiceOptions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Multiple Choice Option'), ['controller' => 'MultipleChoiceOptions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="multipleChoiceAnswers index large-9 medium-8 columns content">
    <h3><?= __('Multiple Choice Answers') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('incident_id') ?></th>
                <th><?= $this->Paginator->sort('multiple_choice_option_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($multipleChoiceAnswers as $multipleChoiceAnswer): ?>
            <tr>
                <td><?= $this->Number->format($multipleChoiceAnswer->id) ?></td>
                <td><?= $multipleChoiceAnswer->has('incident') ? $this->Html->link($multipleChoiceAnswer->incident->id, ['controller' => 'Incidents', 'action' => 'view', $multipleChoiceAnswer->incident->id]) : '' ?></td>
                <td><?= $multipleChoiceAnswer->has('multiple_choice_option') ? $this->Html->link($multipleChoiceAnswer->multiple_choice_option->id, ['controller' => 'MultipleChoiceOptions', 'action' => 'view', $multipleChoiceAnswer->multiple_choice_option->id]) : '' ?></td>
                <td><?= h($multipleChoiceAnswer->created) ?></td>
                <td><?= h($multipleChoiceAnswer->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $multipleChoiceAnswer->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $multipleChoiceAnswer->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $multipleChoiceAnswer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $multipleChoiceAnswer->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
