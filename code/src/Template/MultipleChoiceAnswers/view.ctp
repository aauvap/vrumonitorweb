<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Multiple Choice Answer'), ['action' => 'edit', $multipleChoiceAnswer->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Multiple Choice Answer'), ['action' => 'delete', $multipleChoiceAnswer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $multipleChoiceAnswer->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Multiple Choice Answers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Multiple Choice Answer'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('List Multiple Choice Options'), ['controller' => 'MultipleChoiceOptions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Multiple Choice Option'), ['controller' => 'MultipleChoiceOptions', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="multipleChoiceAnswers view large-9 medium-8 columns content">
    <h3><?= h($multipleChoiceAnswer->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Incident') ?></th>
            <td><?= $multipleChoiceAnswer->has('incident') ? $this->Html->link($multipleChoiceAnswer->incident->id, ['controller' => 'Incidents', 'action' => 'view', $multipleChoiceAnswer->incident->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Multiple Choice Option') ?></th>
            <td><?= $multipleChoiceAnswer->has('multiple_choice_option') ? $this->Html->link($multipleChoiceAnswer->multiple_choice_option->id, ['controller' => 'MultipleChoiceOptions', 'action' => 'view', $multipleChoiceAnswer->multiple_choice_option->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($multipleChoiceAnswer->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($multipleChoiceAnswer->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($multipleChoiceAnswer->modified) ?></tr>
        </tr>
    </table>
</div>
