<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * FreeTextField cell
 */
class FieldCell extends Cell
{
    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($question, $field, $fieldCounts)
    {
        //$htmlHelper = $this->loadHelper('Html');
        $this->set('caption', $field->caption);
        $map = false;
        $fields = [];
        $enMonths = [__("January"),__("February"),__("March"),__("April"),__("May"),__("June"),__("July"),__("August"),__("September"),__("October"),__("November"),__("December")];
        $daMonths = [__("Januar"),__("Februar"),__("Marts"),__("April"),__("Maj"),__("Juni"),__("Juli"),__("August"),__("September"),__("Oktober"),__("November"),__("December")];
        $nlMonths = [__("Januari"),__("Februari"),__("Maart"),__("April"),__("Mei"),__("Juni"),__("Juli"),__("Augustus"),__("September"),__("Oktober"),__("November"),__("December")];
        $caMonths = [__("Gener"),__("Febrer"),__("Març"),__("Abril"),__("Maig"),__("Juny"),__("Juliol"),__("Agost"),__("Setembre"),__("Octubre"),__("Novembre"),__("Desembre")];
        $seMonths = [__("Januari"),__("Februari"),__("Mars"),__("April"),__("Maj"),__("Juni"),__("Juli"),__("Augusti"),__("September"),__("Oktober"),__("November"),__("December")];
        switch($field->source()) {
            case "FreeTextFields":
                $fields = [$field->source()."[".$fieldCounts[$field->source()]."][text]" => [
                               "type" => 'textarea',
                               "label" => ''
                           ],
                           $field->source()."[".$fieldCounts[$field->source()]."][free_text_field_id]" => [
                               "type" => 'hidden',
                               "value" => $field->id
                           ]
                           
                ];
                break;
            case "NumberFields":
                $minNumber = !is_null($field->min_number) ? $field->min_number : 0;
                $maxNumber = !is_null($field->max_number) ? $field->max_number : 100;
                $numType = $field->allow_floats == 1 ? "float_number" : "number";
                $type = 'number';
                $options = [];
                if($field->dropdown) {
                    $type = 'select';
                    $options = array_merge([""], range($minNumber, $maxNumber));
                }
                $fields = [$field->source()."[".$fieldCounts[$field->source()]."][".$numType."]" => [
                               "label" => '',
                               "type" => $type,
                               "options" => $options
                    
                           ],
                           $field->source()."[".$fieldCounts[$field->source()]."][number_field_id]" => [
                               "type" => 'hidden',
                               "value" => $field->id
                           ]
                           
                ];
                break;
            case "DateFields":
                $dateFieldType = $field->allow_time == 1 ? "datetime" : "date";
                $field->q;
                $months = $enMonths;
                switch ($this -> getLocale($question)) {
                  case "da":
                    $months = $daMonths;
                    break;
                  case "nl":
                    $months = $nlMonths;
                    break;
                  case "ca":
                    $months = $caMonths;
                    break;
                  case "se":
                    $months = $seMonths;
                }
                $fields = [$field->source()."[".$fieldCounts[$field->source()]."][datetime]" => [
                              "type" => $dateFieldType,
                              "monthNames" => $months,
                              "label" => '',
                              "empty" => true,
                              "interval" => 5
                           ],
                           $field->source()."[".$fieldCounts[$field->source()]."][date_field_id]" => [
                               "type" => 'hidden',
                               "value" => $field->id
                           ],
                           $field->source()."[".$fieldCounts[$field->source()]."][know_date]" => [
                              "type" => 'checkbox',
                              "label" => __('Dont know date')
                           ],
                           $field->source()."[".$fieldCounts[$field->source()]."][know_time]" => [
                              "type" => $field->allow_time == 1 ? 'checkbox' : "hidden",
                              "label" => __('Dont know time')
                           ]
                ];
                if(!is_null($field->min_date)) {
                    $fields[$field->source()."[".$fieldCounts[$field->source()]."][datetime]"]["minYear"] = $field->min_date->year;
                }
                if(!is_null($field->max_date)) {
                    $fields[$field->source()."[".$fieldCounts[$field->source()]."][datetime]"]["maxYear"] = $field->max_date->year;
                }
                
                break;
            case "MapFields":
                $map = true;
                $this->set('map_bounding_box', $field->map_bounding_box);
                $fields = [$field->source().'['.$fieldCounts[$field->source()].'][min_num_marks]' => ['type' => 'hidden',
                                               "value" => $field->min_num_marks],
                           $field->source().'['.$fieldCounts[$field->source()].'][max_num_marks]' => ['type' => 'hidden',
                                                                                                      "value" => $field->max_num_marks],
                           $field->source().'['.$fieldCounts[$field->source()].'][map_field_id]' => ['type' => 'hidden',
                                                                                                      "value" => $field->id]
                ];
                break;
            case "MultipleChoiceFields":
                $options = [];
                $max_image_size = $field->max_image_size;
                foreach($field->multiple_choice_options as $option) {
                    $class = '';
                    if($option->exclusive) {
                        $class = 'exclusive';
                    }
                    $text = '';
                    $isPic = false;
                    if(!empty($option->picture)) {
                        $isPic = true;
                        //$text .= "<img src=\"../..";
                        $text .= "<img src=\"";
                        //$text .= $this->Url->build('/', true);
                        $text .= "/files/multiplechoiceoptions/picture/".$option->picture_dir;
                        $text .= "/".$option->picture;
                       //"src='".$this->request->webroot."/files/multiplechoiceoptions/picture/'".                                                $option->picture_dir.                                                "/smallSquare_".$option->picture."'";
                        $text .= "\" class = \"question_image\"";
                        if($max_image_size != null) {
                          $text.= " style=\"";
                          $text.= "max-width:" . $max_image_size . "px;";
                          $text.= "max-height:" . $max_image_size . "px;\"";
                        }
                        $text .= "/><br>";
                    }
                    $max_text_size = 300;
                    if($max_image_size != null) {
                      $max_text_size = $max_image_size;;
                    }
                    if(!empty($option->picture)) {
                      $text .= "<div style=max-width:". $max_text_size ."px>".$option->caption."</div>";
                    } else {
                      $text .= $option->caption;
                    }
                    #$text .= $option->caption;
                    if($isPic) {
                      $text .= "<hr>";
                    }
                    $options[] = ['value' => $option->id, 'text' => $text, 'class' => $class];
                }
                if($field->min_num_choices == 1 and $field->max_num_choices == 1) {
                    $fields = [$field->source()."[".$fieldCounts[$field->source()]."][multiple_choice_option_id]" => [
                        "type" => "radio",
                        "label" => '',
                        "options" => $options,
                        "escape" => false,
                        "min_num_choices" => $field->min_num_choices,
                        "max_num_choices" => $field->max_num_choices,
                    ]];
                } else {
                    $fields = [$field->source()."[".$fieldCounts[$field->source()]."][multiple_choice_option_id]" => [
                        "type"=>"select",
                        "multiple"=>"checkbox",
                        "min_num_choices" => $field->min_num_choices,
                        "max_num_choices" => $field->max_num_choices,
                        "label" => "",
                        "options" => $options,
                        "escape" => false
                    ]];
                }
                break;
        }
        //debug($fields);
        $this->set('fields', $fields);
        $this->set('map', $map);
    }

    public function getLocale($question) {
      $questionnaire_id = $question->get("questionnaire_id");
      $questionnaires = TableRegistry :: get("questionnaires");
      $questionnaire = $questionnaires -> get ($question->get("questionnaire_id"));
      $res = $questionnaire->get('language');
      return $res;
    }
}
