<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Utility\Inflector;

class InflectorHelper extends Helper
{
    public function singularize($input)
    {
        return Inflector::singularize($input);
    }
    
    public function pluralize($input)
    {
        return Inflector::pluralize($input);
    }
    
    public function camelize($input)
    {
        return Inflector::camelize($input);
    }
    
    public function underscore($input)
    {
        return Inflector::underscore($input);
    }
    
    public function humanize($input)
    {
        return Inflector::humanize($input);
    }
    
    public function tableize($input)
    {
        return Inflector::tableize($input);
    }
    
    public function classify($input)
    {
        return Inflector::classify($input);
    }
    
    public function variable($input)
    {
        return Inflector::variable($input);
    }
    
    public function slug($input)
    {
        return Inflector::slug($input);
    }
}

?>
