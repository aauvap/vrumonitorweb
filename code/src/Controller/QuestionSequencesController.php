<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * QuestionSequences Controller
 *
 * @property \App\Model\Table\QuestionSequencesTable $QuestionSequences
 */
class QuestionSequencesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    /*public function index()
    {
        $this->paginate = [
            'contain' => ['Questions']
        ];
        $this->set('questionSequences', $this->paginate($this->QuestionSequences));
        $this->set('_serialize', ['questionSequences']);
    }*/

    /**
     * View method
     *
     * @param string|null $id Question Sequence id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    /*public function view($id = null)
    {
        $questionSequence = $this->QuestionSequences->get($id, [
            'contain' => ['Questions']
        ]);
        $this->set('questionSequence', $questionSequence);
        $this->set('_serialize', ['questionSequence']);
    }*/

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $questionSequence = $this->QuestionSequences->newEntity();
        if ($this->request->is('post')) {
            $questionSequence = $this->QuestionSequences->patchEntity($questionSequence, $this->request->data);
            $questionSequence->priority = 0;
            $q = $this->QuestionSequences->find()
                      ->select(['priority'])
                      ->where(['question_id =' => $questionSequence->question_id])
                      ->order(['priority' => 'DESC'])
                      ->limit(1)
                      ->first();
            if($q != null) {
                $questionSequence->priority = $q->priority+1;
            }
            if ($this->QuestionSequences->save($questionSequence)) {
                $this->Flash->success(__('The question sequence has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The question sequence could not be saved. Please, try again.'));
            }
        }
        $questions = $this->QuestionSequences->Questions->find('list', ['limit' => 200]);
        $this->set(compact('questionSequence', 'questions'));
        $this->set('_serialize', ['questionSequence']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Question Sequence id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $questionSequence = $this->QuestionSequences->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $questionSequence = $this->QuestionSequences->patchEntity($questionSequence, $this->request->data);
            if($questionSequence->to_question_id == -1) {
                $questionSequence->to_question_id = null;
            }
            if ($this->QuestionSequences->save($questionSequence)) {
                $this->Flash->success(__('The question sequence has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The question sequence could not be saved. Please, try again.'));
            }
        }
        $questions = $this->QuestionSequences->Questions->find('list', ['limit' => 200]);
        $this->set(compact('questionSequence', 'questions'));
        $this->set('_serialize', ['questionSequence']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Question Sequence id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $questionSequence = $this->QuestionSequences->get($id);
        $questionId = $questionSequence->question_id;
        if ($this->QuestionSequences->delete($questionSequence)) {
            $this->Flash->success(__('The question sequence has been deleted.'));
        } else {
            $this->Flash->error(__('The question sequence could not be deleted. Please, try again.'));
        }
        return $this->redirect(['controller' => 'Questions', 'action' => 'edit', $questionId]);
    }
}
