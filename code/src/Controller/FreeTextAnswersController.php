<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FreeTextAnswers Controller
 *
 * @property \App\Model\Table\FreeTextAnswersTable $FreeTextAnswers
 */
class FreeTextAnswersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Incidents', 'FreeTextFields']
        ];
        $this->set('freeTextAnswers', $this->paginate($this->FreeTextAnswers));
        $this->set('_serialize', ['freeTextAnswers']);
    }

    /**
     * View method
     *
     * @param string|null $id Free Text Answer id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $freeTextAnswer = $this->FreeTextAnswers->get($id, [
            'contain' => ['Incidents', 'FreeTextFields']
        ]);
        $this->set('freeTextAnswer', $freeTextAnswer);
        $this->set('_serialize', ['freeTextAnswer']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $freeTextAnswer = $this->FreeTextAnswers->newEntity();
        if ($this->request->is('post')) {
            $freeTextAnswer = $this->FreeTextAnswers->patchEntity($freeTextAnswer, $this->request->data);
            if ($this->FreeTextAnswers->save($freeTextAnswer)) {
                $this->Flash->success(__('The free text answer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The free text answer could not be saved. Please, try again.'));
            }
        }
        $incidents = $this->FreeTextAnswers->Incidents->find('list', ['limit' => 200]);
        $freeTextFields = $this->FreeTextAnswers->FreeTextFields->find('list', ['limit' => 200]);
        $this->set(compact('freeTextAnswer', 'incidents', 'freeTextFields'));
        $this->set('_serialize', ['freeTextAnswer']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Free Text Answer id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $freeTextAnswer = $this->FreeTextAnswers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $freeTextAnswer = $this->FreeTextAnswers->patchEntity($freeTextAnswer, $this->request->data);
            if ($this->FreeTextAnswers->save($freeTextAnswer)) {
                $this->Flash->success(__('The free text answer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The free text answer could not be saved. Please, try again.'));
            }
        }
        $incidents = $this->FreeTextAnswers->Incidents->find('list', ['limit' => 200]);
        $freeTextFields = $this->FreeTextAnswers->FreeTextFields->find('list', ['limit' => 200]);
        $this->set(compact('freeTextAnswer', 'incidents', 'freeTextFields'));
        $this->set('_serialize', ['freeTextAnswer']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Free Text Answer id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $freeTextAnswer = $this->FreeTextAnswers->get($id);
        if ($this->FreeTextAnswers->delete($freeTextAnswer)) {
            $this->Flash->success(__('The free text answer has been deleted.'));
        } else {
            $this->Flash->error(__('The free text answer could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
