<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Datasource\ConnectionManager;

/**
 * Participants Controller
 *
 * @property \App\Model\Table\ParticipantsTable $Participants
 */
class ParticipantsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('International');    
        $this->loadComponent('QuestionAuth');

        $this->Auth->allow('activate');
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('participants', $this->paginate($this->Participants->find('all', ['contain' => ['Questionnaires']])));
        $this->set('_serialize', ['participants']);
    }

    /**
     * View method
     *
     * @param string|null $id Participant id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $participant = $this->Participants->get($id, [
            'contain' => ['Incidents']
        ]);
        $this->set('participant', $participant);
        $this->set('_serialize', ['participant']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $participant = $this->Participants->newEntity();
        if ($this->request->is('post')) {
            $participant = $this->Participants->patchEntity($participant, $this->request->data);
            $participant->participant_key = $this->QuestionAuth->randomKey();
            $participant->questionnaire_id = $this->request->data['questionnaires'];
            if ($this->Participants->save($participant)) {
                $this->Participants->save($participant);
                $this->Flash->success(__('The participant has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The participant could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('participant'));
        $questionnaires = $this->Participants->Questionnaires->find('list', ['limit' => 200]);
        $this->set(compact('questionnaires'));
        $this->set('countryCodes', $this->International->countryCodes());
        $this->set('languageCodes', $this->International->languageCodes());
        $this->set('_serialize', ['participant']);
    }

    public function addmany($baseQuestionnaire = null)
    {
        $confirm = false;
        if ($this->request->is('post')) {
            if(strpos($this->request->data['activation_email'], '%link%') === false) {
                $this->Flash->error(__('The activation e-mail must contain the placehoder %link%.')); // Due to the chr() used in the condition generation in the foreach below.
                return $this->redirect(['action' => 'addmany']);
            }

            // Remove the empty condition at the end of the array.
            $participantConditions = array_slice($this->request->data['participant_conditions'],0,-1);


            if(count($participantConditions) > 26) {
                $this->Flash->error(__('You cannot have more than 26 conditions.')); // Due to the chr() used in the condition generation in the foreach below.
                return $this->redirect(['action' => 'addmany']);
            }

            $joinSql  = "INNER JOIN incidents Incidents ";
            $joinSql .= "  ON Participants.id = Incidents.participant_id ";
            $joinSql .= "INNER JOIN free_text_answers FreeTextAnswers ";
            $joinSql .= "  ON FreeTextAnswers.free_text_field_id = ";
            $joinSql .=         $this->request->data['email_field'];
            $joinSql .= "  AND Incidents.id = (FreeTextAnswers.incident_id) ";
            $i = 0;
            foreach($participantConditions as $c) {
                $alias = chr($i+97);
                $joinSql .= "INNER JOIN multiple_choice_answers ".$alias." ";
                $joinSql .= "  ON ".$alias.".multiple_choice_option_id = ".$c." ";
                $joinSql .= "  AND Incidents.id = (".$alias.".incident_id) ";
                $i++;
            }
            $whereSql  = "WHERE ( ";
            $whereSql .= "    Participants.questionnaire_id = ";
            $whereSql .=        $this->request->data['base_questionnaire'];
            $whereSql .= "    AND email = 'anonymous')";

            $selectSql  = "SELECT FreeTextAnswers.text AS email_address,";
            $selectSql .= "       Participants.id AS participant_id,";
            $selectSql .= "       Participants.participant_key ";
            $selectSql .= "FROM participants AS Participants ";
            $selectSql .= $joinSql;
            $selectSql .= $whereSql;
            
            $connection = ConnectionManager::get('default');
            $newParticipants = $connection->execute($selectSql);
            
            if($this->request->data['confirmed']) {
                $this->loadModel('EmailSpool');
                foreach($newParticipants as $p) {
                    $email = $this->EmailSpool->newEntity();
                    $email->recipient = $p['email_address'];
                    $email->sender_address = trim($this->request->data['activation_email_sender_address']);
                    $email->sender_name = $this->request->data['activation_email_sender_name'];
                    $email->subject = $this->request->data['activation_email_subject'];
                    $email->message = str_replace('%link%',
                                                  Router::url(['controller' => 'Participants',
                                                              'action' => 'activate',
                                                              $p['participant_id'],
                                                              '?' => ['key' => $p['participant_key']]], true)." ",
                                                  $this->request->data['activation_email']);
                    $this->EmailSpool->save($email);
                }
                
                $updateSql  = "UPDATE participants AS Participants ";
                $updateSql .= $joinSql;
                $updateSql .= "SET Participants.email = FreeTextAnswers.text, ";
                $updateSql .= "    Participants.activated = 0, ";
                $updateSql .= "    Participants.questionnaire_id = ";
                $updateSql .= $this->request->data['target_questionnaire']." ";
                $updateSql .= $whereSql;
                $connection = ConnectionManager::get('default');
                $numParticipants = $connection->execute($updateSql)->count();
            
                if($numParticipants == 0) {
                    $this->Flash->error(__('No participants passed the given conditions. No new participants created.'));
                } else {
                    $this->Flash->success(__(sprintf('%d participant(s) created.', $numParticipants)));
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $confirm = true;
                $this->set("numParticipants", $newParticipants->count());
            }
        }

        $query = $this->Participants->Questionnaires->find()->select(['id', 'title'])->where(['restrict_access = ' => 0])->toArray();
        $publicQuestionnaires[''] = '';
        foreach($query as $q) {
            $publicQuestionnaires[$q['id']] = $q['id'] . ": " . $q['title'];
        }
        $emailFields = ['' => ''];
        $participantConditions = ['' => '<no condition>'];
        if($baseQuestionnaire !== null) {
            $this->loadModel('Questionnaires');
            $questionnaire = $this->Questionnaires->get($baseQuestionnaire, [
                'contain' => ['Questions.FreeTextFields', 'Questions.MultipleChoiceFields.MultipleChoiceOptions']
            ]);
            foreach($questionnaire->questions as $q) {
                foreach($q['free_text_fields'] as $f) {
                    $emailFields["Q".$q->id.": ".$q->question][$f->id] = $f->caption;
                }
                foreach($q['multiple_choice_fields'] as $f) {
                    foreach($f['multiple_choice_options'] as $o) {
                        $participantConditions["Q".$q->id.": ".$q->question][$f->caption." [".$f->internal_description."]"][$o->id] = $o->caption;
                    }
                }
            }
        }
        
        $query = $this->Participants->Questionnaires->find()->select(['id', 'title'])->toArray();
        foreach($query as $q) {
            $allQuestionnaires[$q['id']] = $q['id'] . ": " . $q['title'];
        }

        $this->set('sampleSubject',
                   __('Request for participation in study'));
        $this->set('sampleSenderName',
                   __('John Doe, Famous University'));
        $this->set('sampleSenderAddress',
                   __('john.doe@famuni.edu'));
        $this->set('sampleEmail',
                   __('Hi,
You have been enrolled in our study. Please click the link %link% to confirm future participation. By clicking this link, you agree to receive questionnaires from us in the future.
Thank you!'));

        $this->set(compact('baseQuestionnaire', 'publicQuestionnaires', 'emailFields', 'participantConditions', 'allQuestionnaires', 'confirm'));
    }

    public function activate($id) {
        $participant = $this->Participants->get($id);
        if($this->QuestionAuth->checkParticipantAccess($participant, $this->request)) {
            
            $participant->activated = true;
            $this->Participants->save($participant);

            $this->loadModel("Incidents");
            $this->loadModel("Questionnaires");
            $questionnaire = $this->Questionnaires->get($participant->questionnaire_id);

            // Get the most recent incident and redirect:
            $currentIncident = $this->Incidents
                                    ->find()
                                    ->where(['participant_id' => $participant->id,
                                             'questionnaire_id' =>
                                                 $participant->questionnaire_id])
                                    ->order(['created' =>'DESC'])
                                    ->limit(1);

            if($currentIncident->count() == 0) {
                return $this->redirect(['controller' => 'Questionnaires',
                                        'action' => 'newIncident',
                                        $participant->id,
                                        true]);
            } else {
                $incident = $currentIncident->first();
                return $this->redirect(['controller' => 'Questionnaires',
                                        'action' => 'view',
                                        $incident->id,
                                        "?" => ["key" => $incident->incident_key]]);
            }
            
            
            //return $this->redirect('/');
        } else {
            $this->Flash->error(__('Could not activate participant'));
        }
        //return $this->redirect('/');
    }

    /**
     * Edit method
     *
     * @param string|null $id Participant id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $participant = $this->Participants->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $participant = $this->Participants->patchEntity($participant, $this->request->data);
            $participant->questionnaire_id = $this->request->data['questionnaires'];
            if ($this->Participants->save($participant)) {
                $this->Flash->success(__('The participant has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The participant could not be saved. Please, try again.'));
            }
        }
        $questionnaires = $this->Participants->Questionnaires->find('list', ['limit' => 200]);
        $this->set(compact('participant', 'questionnaires'));
        $this->set('_serialize', ['participant']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Participant id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $participant = $this->Participants->get($id);
        if ($this->Participants->delete($participant)) {
            $this->Flash->success(__('The participant has been deleted.'));
        } else {
            $this->Flash->error(__('The participant could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function updateKeys() {
        $participants = $this->Participants->find('all')->where(['participant_key' => 0]);
        $participantCount = $participants->count();
        if($this->request->is(['patch', 'post', 'put'])) {
            foreach ($participants as $key => $p) {
                $p->participant_key = $this->QuestionAuth->randomKey();
                if(!$this->Participants->save($p)) {
                    $this->Flash->error("Error when saving, during update keys");
                    break;
                }
            }
            $this->Flash->success("Keys Successfully made");
        }
        $this->set(compact('participantCount'));
    }

    public function searchEmail() {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $result = $this->Participants->find()->where(['email' => $this->request->data['Email']])->first();            
            if(!is_null($result)) {
                return $this->redirect(['action' => 'edit', $result->id]);
            } else {
                $this->Flash->error(__("Participant with given mail not found (Check Spelling?)"));
            }
        }
    }
}
