<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SensorDumps Controller
 *
 * @property \App\Model\Table\SensorDumpsTable $SensorDumps
 */
class SensorDumpsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Incidents']
        ];
        $this->set('sensorDumps', $this->paginate($this->SensorDumps));
        $this->set('_serialize', ['sensorDumps']);
    }

    /**
     * View method
     *
     * @param string|null $id Sensor Dump id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sensorDump = $this->SensorDumps->get($id, [
            'contain' => ['Incidents']
        ]);
        $this->set('sensorDump', $sensorDump);
        $this->set('_serialize', ['sensorDump']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sensorDump = $this->SensorDumps->newEntity();
        if ($this->request->is('post')) {
            $sensorDump = $this->SensorDumps->patchEntity($sensorDump, $this->request->data);
            if ($this->SensorDumps->save($sensorDump)) {
                $this->Flash->success(__('The sensor dump has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sensor dump could not be saved. Please, try again.'));
            }
        }
        $incidents = $this->SensorDumps->Incidents->find('list', ['limit' => 200]);
        $this->set(compact('sensorDump', 'incidents'));
        $this->set('_serialize', ['sensorDump']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sensor Dump id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sensorDump = $this->SensorDumps->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sensorDump = $this->SensorDumps->patchEntity($sensorDump, $this->request->data);
            if ($this->SensorDumps->save($sensorDump)) {
                $this->Flash->success(__('The sensor dump has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sensor dump could not be saved. Please, try again.'));
            }
        }
        $incidents = $this->SensorDumps->Incidents->find('list', ['limit' => 200]);
        $this->set(compact('sensorDump', 'incidents'));
        $this->set('_serialize', ['sensorDump']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sensor Dump id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sensorDump = $this->SensorDumps->get($id);
        if ($this->SensorDumps->delete($sensorDump)) {
            $this->Flash->success(__('The sensor dump has been deleted.'));
        } else {
            $this->Flash->error(__('The sensor dump could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
