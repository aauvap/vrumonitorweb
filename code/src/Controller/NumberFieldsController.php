<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * NumberFields Controller
 *
 * @property \App\Model\Table\NumberFieldsTable $NumberFields
 */
class NumberFieldsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('QuestionFields');
    }

    /**
     * Index method
     *
     * @return void
     */
    /*public function index()
    {
        $this->paginate = [
            'contain' => ['Questions']
        ];
        $this->set('numberFields', $this->paginate($this->NumberFields));
        $this->set('_serialize', ['numberFields']);
    }*/

    /**
     * View method
     *
     * @param string|null $id Number Field id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    /*public function view($id = null)
    {
        $numberField = $this->NumberFields->get($id, [
            'contain' => ['Questions']
        ]);
        $this->set('numberField', $numberField);
        $this->set('_serialize', ['numberField']);
    }*/

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($question_id = null)
    {
        $this->QuestionFields->add($question_id, $this->NumberFields);
    }

    /**
     * Edit method
     *
     * @param string|null $id Number Field id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->QuestionFields->edit($id, $this->NumberFields);
    }

    /**
     * Delete method
     *
     * @param string|null $id Number Field id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->QuestionFields->delete($id, $this->NumberFields);
    }
}
