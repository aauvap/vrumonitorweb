<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MapAnswers Controller
 *
 * @property \App\Model\Table\MapAnswersTable $MapAnswers
 */
class MapAnswersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Incidents', 'MapFields']
        ];
        $this->set('mapAnswers', $this->paginate($this->MapAnswers));
        $this->set('_serialize', ['mapAnswers']);
    }

    /**
     * View method
     *
     * @param string|null $id Map Answer id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mapAnswer = $this->MapAnswers->get($id, [
            'contain' => ['Incidents', 'MapFields']
        ]);
        $this->set('mapAnswer', $mapAnswer);
        $this->set('_serialize', ['mapAnswer']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mapAnswer = $this->MapAnswers->newEntity();
        if ($this->request->is('post')) {
            $mapAnswer = $this->MapAnswers->patchEntity($mapAnswer, $this->request->data);
            if ($this->MapAnswers->save($mapAnswer)) {
                $this->Flash->success(__('The map answer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The map answer could not be saved. Please, try again.'));
            }
        }
        $incidents = $this->MapAnswers->Incidents->find('list', ['limit' => 200]);
        $mapFields = $this->MapAnswers->MapFields->find('list', ['limit' => 200]);
        $this->set(compact('mapAnswer', 'incidents', 'mapFields'));
        $this->set('_serialize', ['mapAnswer']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Map Answer id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mapAnswer = $this->MapAnswers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mapAnswer = $this->MapAnswers->patchEntity($mapAnswer, $this->request->data);
            if ($this->MapAnswers->save($mapAnswer)) {
                $this->Flash->success(__('The map answer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The map answer could not be saved. Please, try again.'));
            }
        }
        $incidents = $this->MapAnswers->Incidents->find('list', ['limit' => 200]);
        $mapFields = $this->MapAnswers->MapFields->find('list', ['limit' => 200]);
        $this->set(compact('mapAnswer', 'incidents', 'mapFields'));
        $this->set('_serialize', ['mapAnswer']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Map Answer id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mapAnswer = $this->MapAnswers->get($id);
        if ($this->MapAnswers->delete($mapAnswer)) {
            $this->Flash->success(__('The map answer has been deleted.'));
        } else {
            $this->Flash->error(__('The map answer could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
