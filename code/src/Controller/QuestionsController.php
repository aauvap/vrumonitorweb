<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\I18n\I18n;


/**
 * Questions Controller
 *
 * @property \App\Model\Table\QuestionsTable $Questions
 */
class QuestionsController extends AppController
{    
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('QuestionFields');
        $this->loadComponent('QuestionAuth');
        $this->Auth->allow('view');
        $this->Auth->allow('saveAnswers');
    }

    private function setLanguage($questionnaireID) {
      $this->loadModel('Questionnaires');
      $languageCode = $this->Questionnaires->get($questionnaireID)->language;
      $language = null;
      switch ($languageCode) {
        case 'da':
          $language = 'da_DK';
          break;

        case 'sv':
          $language = 'sv_SE';
          break;

        case 'nl':
          $language = 'nl_BE';
          break;

        case 'ca':
          $language = 'ca_ES';
          break;
      }

      I18n::locale($language);
      debug(I18n::locale());
    }


    /**
     * Index method
     *
     * @return void
     */
    /*public function index()
    {
        $this->paginate = [
            'contain' => ['Questionnaires']
        ];
        $this->set('questions', $this->paginate($this->Questions));
        $this->set('_serialize', ['questions']);
       }*/
    
    /**
     * View method
     *
     * @param string|null $id Question id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null, $incidentId = null)
    {
        $json = false;
        if(strpos($this->request->url, 'json')) {
            $id = $this->request->params['questionid'];
            $incidentId = $this->request->params['incidentid'];
            $json = true;
        }
        
        $question = $this->Questions->get($id, [
            'contain' => ['FreeTextFields',
                          'NumberFields',
                          'MultipleChoiceFields',
                          'MultipleChoiceFields.MultipleChoiceOptions',
                          'DateFields',
                          'MapFields']
        ]);

        $this->setLanguage($question->questionnaire_id);

        $fields = array_merge($question->free_text_fields, $question->number_fields, $question->multiple_choice_fields, $question->date_fields, $question->map_fields);
        usort($fields, function($a, $b) {return $a->priority - $b->priority;}); // Sort fields by priority.
        

        $incidentId = $this->QuestionAuth->getIncidentId($this->request, $incidentId);
        $allowAnswer = $this->QuestionAuth->checkAnswerAccess($question, $this->request, $incidentId);

        if($incidentId !== null) {
            $this->loadModel('Incidents');
            $incident = $this->Incidents->get($incidentId, ['contain' => ['SensorDumps']]);
            if($allowAnswer) {
                // Delete earlier answers (if arriving via a Previous Question button)
                $this->loadModel("FreeTextAnswers");
                foreach($question->free_text_fields as $f) {
                    $q = $this->FreeTextAnswers->find()
                              ->where(['free_text_field_id' => $f->id,
                                       'incident_id' => $incidentId]);
                    foreach($q as $x) {
                        $this->FreeTextAnswers->delete($x);
                    }
                }

                $this->loadModel("NumberAnswers");
                foreach($question->number_fields as $f) {
                    $q = $this->NumberAnswers->find()
                              ->where(['number_field_id' => $f->id,
                                       'incident_id' => $incidentId]);
                    foreach($q as $x) {
                        $this->NumberAnswers->delete($x);
                    }
                }

                $this->loadModel("DateAnswers");
                foreach($question->date_fields as $f) {
                    $q = $this->DateAnswers->find()
                              ->where(['date_field_id' => $f->id,
                                       'incident_id' => $incidentId]);
                    foreach($q as $x) {
                        $this->DateAnswers->delete($x);
                    }
                }

                $this->loadModel("MultipleChoiceAnswers");
                foreach($question->multiple_choice_fields as $f) {
                    foreach($f->multiple_choice_options as $o) {
                        $q = $this->MultipleChoiceAnswers->find()
                                  ->where(['multiple_choice_option_id' => $o->id,
                                           'incident_id' => $incidentId]);
                        foreach($q as $x) {
                            $this->MultipleChoiceAnswers->delete($x);
                        }
                    }
                }
                
                $this->loadModel("MapAnswers");
                foreach($question->map_fields as $f) {
                    $q = $this->MapAnswers->find()
                              ->where(['map_field_id' => $f->id,
                                       'incident_id' => $incidentId]);
                    foreach($q as $x) {
                        $this->MapAnswers->delete($x);
                    }
                }
            
                $incident->visited_questions = str_replace($id.",", '', $incident->visited_questions);
                $this->Incidents->save($incident);
            }

            
            if($incident->visited_questions !== null) {
                $previousQuestion = array_slice(explode(",", $incident->visited_questions), -2,1)[0];
                $this->set('previousQuestion', $previousQuestion);
            } else {
                $this->set('previousQuestion', -1);
            }

            // Replace any %something% strings with data from SensorDumps, if it exists:
            if(count($incident->sensor_dumps) > 0) {
                $sensorDump = json_decode($incident->sensor_dumps[0]->data);
                $question->question = $this->insertSensorData($question->question, $sensorDump);
                foreach($fields as $f) {
                    $f->caption = $this->insertSensorData($f->caption, $sensorDump);
                    if(is_array($f->multiple_choice_options)) {
                        foreach($f->multiple_choice_options as $o) {
                            $o->caption = $this->insertSensorData($o->caption, $sensorDump);
                        }
                    }
                }
            }
        }

        $this->set('question', $question);
        $this->set('fields', $fields);
        $this->set('field_types',
                   array_combine(array_map("Cake\Utility\Inflector::pluralize",
                                           array_map("Cake\Utility\Inflector::classify", $this->QuestionFields->fieldTypes())),
                                 array_map("Cake\Utility\Inflector::humanize", $this->QuestionFields->fieldTypes())));
        
        $this->set('allowAnswer', $allowAnswer);
        $this->set('incidentId', $incidentId);
        $this->set('incidentKey', $incident->incident_key);

        // FOR DEBUGGING ONLY!
        //$this->set('allowAnswer', true);
        //$this->set('key', $this->request->query('key'));
        // FOR DEBUGGING ONLY!
        $this->set('_serialize', ['question', 'previousQuestion']);
    }

    private function insertSensorData($subject, $sensorDump) {
        $matches = array();
        preg_match_all('/%([^%]+)%/', $subject, $matches);
        foreach($matches[1] as $m) {
            if(property_exists($sensorDump, $m)) {
                $subject = str_replace('%'.$m.'%', $sensorDump->$m, $subject);
            }
        }
        return $subject;
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($questionnaire_id = null)
    {
        $question = $this->Questions->newEntity();
        
        if ($this->request->is('post')) {
            $question = $this->Questions->patchEntity($question, $this->request->data);
            $this->loadModel('Questionnaires');

            // Override the GET-transmitted id if one has been transmitted via POST:
            if(array_key_exists('questionnaires', $this->request->data)) {
                $questionnaire_id = $this->request->data['questionnaires'];
            }
            
            if (is_null($questionnaire_id)) {
                $this->Flash->error(__('You cannot add a question which is not associated with a questionnaire.'));
            } elseif(!$this->Questionnaires->exists(['Questionnaires.id' => $questionnaire_id])) {
                $this->Flash->error(__('A questionnaire with id {0} does not exist and thus the question cannot be added. Please select a valid questionnaire.', [$questionnaire_id]));
            } else {
                $question->questionnaire_id = $questionnaire_id;
                $priority = $this->Questions
                                 ->find()
                                 ->select('priority')
                                 ->where(['questionnaire_id' => $questionnaire_id])
                                 ->max('priority')['priority'];
                $question->priority = $priority === null ? 0 : $priority+1;
                if ($this->Questions->save($question)) {
                    $this->Flash->success(__('The question has been saved.'));
                    return $this->redirect(['controller' => 'Questions', 'action' => 'edit', $question->id]);
                } else {
                    $this->Flash->error(__('The question could not be saved. Please, try again.'));
                }
            }
        }
        $questionnaires = $this->Questions->Questionnaires->find('list', ['limit' => 200]);
        $this->set(compact('question', 'questionnaires', 'questionnaire_id'));
        $this->set('_serialize', ['question']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Question id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $question = $this->Questions->get($id, [
            'contain' => ['Questionnaires',
                          'Questionnaires.Questions',
                          'Questionnaires.Questions.MultipleChoiceFields.MultipleChoiceOptions',
                          'FreeTextFields',
                          'NumberFields',
                          'MultipleChoiceFields',
                          'DateFields',
                          'MapFields',
                          'QuestionSequences',
                          'QuestionSequences.Questions',
                          'QuestionSequences.SequenceConditions',
                          'QuestionSequences.SequenceConditions.MultipleChoiceOptions',
                          'QuestionSequences.SequenceConditions.MultipleChoiceOptions.MultipleChoiceFields.Questions']
        ]);

        
        $fields = array_merge($question->free_text_fields, $question->number_fields, $question->multiple_choice_fields, $question->date_fields, $question->map_fields);
        usort($fields, function($a, $b) {return $a->priority - $b->priority;}); // Sort fields by priority.

        $connections = $question->question_sequences;
        usort($connections, function($a, $b) {return $a->priority - $b->priority;}); // Sort connections by priority.

        if ($this->request->is(['patch', 'post', 'put'])) {
            $question = $this->Questions->patchEntity($question, $this->request->data);
            if($this->Questions->save($question)) {
                $this->Flash->success(__('The question has been saved.'));
                return $this->redirect(['controller' => 'Questionnaires', 'action' => 'edit', $question->questionnaire_id]);
            } else {
                $this->Flash->error(__('The question could not be saved. Please, try again.'));
            }
        }
        $questionnaires = $this->Questions->Questionnaires->find('list', ['limit' => 200]);
        $this->set(compact('question', 'questionnaires', 'connections', 'fields'));
        $this->set('field_types',
                   array_combine(array_map("Cake\Utility\Inflector::pluralize",
                                           array_map("Cake\Utility\Inflector::classify", $this->QuestionFields->fieldTypes())),
                                 array_map("Cake\Utility\Inflector::humanize", $this->QuestionFields->fieldTypes())));
        $this->set('_serialize', ['question']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Question id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $question = $this->Questions->get($id);
        $questionnaire_id = $question->questionnaire_id;
        if ($this->Questions->delete($question)) {
            $this->Flash->success(__('The question has been deleted.'));
        } else {
            $this->Flash->error(__('The question could not be deleted. Please, try again.'));
        }
        return $this->redirect(['controller' => 'Questionnaires', 'action' => 'edit', $questionnaire_id]);
    }


    public function saveAnswers($question_id = null, $incident_id = null)
    {
        $this->request->allowMethod(['patch', 'post', 'put']);

        $json = false;
        if(strpos($this->request->url, 'json')) {
            $question_id = $this->request->params['questionid'];
            $incident_id = $this->request->params['incidentid'];
            $json = true;
        }
        
        
        $question = $this->Questions->get($question_id);
        if(!$this->QuestionAuth->checkAnswerAccess($question, $this->request, $incident_id)) {
            if($json) {
                $this->set(['message' => __('You do not have access to reply to this question.')]);
                $this->set('_serialize', ['message']);
            } else {
                $this->Flash->error(__('You do not have access to reply to this question.'));
            }
            return;
        }

        $this->setLanguage($question->questionnaire_id);


        //$this->set(['message' => $this->request->data]);
        //$this->set('_serialize', ['message']);
        //return;

        $this->loadModel('FreeTextFields');
        $this->loadModel('MultipleChoiceFields');
        $this->loadModel('MultipleChoiceOptions');
        $this->loadModel('NumberFields');
        $this->loadModel('MapFields');

        //debug($this->request->data);
        
        $allSaved = true;
        $moveOnGuarenteed = false;

        if(array_key_exists('MultipleChoiceFields', $this->request->data)) {
          foreach ($this->request->data['MultipleChoiceFields'] as $f) {
            $options = $f['multiple_choice_option_id'];
            if(is_array($options) || is_array($options)) {
              foreach($options as $o) {
                $option = $this->MultipleChoiceOptions->get($o);
                if($option->allows_move_on) {
                  $moveOnGuarenteed = true;
                }
              }               
            }         
          }
        }

        foreach($this->request->data as $type => $answers) {
            $saveAtEnd = true;
            $model = $this->loadModel(str_replace("Fields", "Answers", $type));
            foreach($answers as $answerData) {
                $answer = $model->newEntity();
                $answer = $model->patchEntity($answer, $answerData);
                $answer->incident_id = $incident_id;
                
                switch($type) {
                    case "FreeTextFields":
                        $answer->text = trim($answer->text);
                        $field = $this->FreeTextFields->get($answer->free_text_field_id);
                        if($field->enforce_unique) {
                            $count = $model->find()->where(['free_text_field_id' => $field->id, 'text' => $answer->text])->count();
                            if($count > 0) {
                                // Not unique. Do not save.
                                $allSaved = false;
                                $saveAtEnd = false;
                                $error = sprintf(__('Sorry, somebody else used the same answer for the text field %d. Answers to this field  must be unique.'), $field->caption);
                                $this->set(['message' => $error]);
                                $this->Flash->error($error);
                            }
                        }
                        if(!$moveOnGuarenteed) {
                          if($field->validate_as_email) {
                            if(strpos($answer->text, '@') === false && strlen($answer->text) > 0) {
                                $allSaved = false;
                                $saveAtEnd = false;
                                $error = sprintf(__('The text field %d must be a valid email.'), $field->caption);
                                $this->set(['message' => $error]);
                                $this->Flash->error($error);
                                break;
                            }
                          }
                          $textLength = strlen($answer->text);
                          if(!is_null($field->min_length)) {
                            if($textLength < $field->min_length) {
                              $allSaved = false;
                              $saveAtEnd = false;
                              $error = sprintf(__("Your answer must be at least %d characters long."), $field->min_length);
                              $this->set(['message' => $error]);
                              $this->Flash->error($error);
                              break;
                            }
                          }
                          if(!is_null($field->max_length)) {
                            if($textLength > $field->max_length) {
                              $allSaved = false;
                              $saveAtEnd = false;
                              $error = sprintf(__("Your answer must have less than %d characters."), $field->max_length);
                              $this->set(['message' => $error]);
                              $this->Flash->error($error);
                              break;
                            }
                          }

                        }
                        if($field->identical_to != '') {
                            $otherAnswer = $model->find()
                                                 ->where(['incident_id' => $incident_id,
                                                          'free_text_field_id' => $field->identical_to])
                                                 ->first();
                            $otherField = $this->FreeTextFields
                                               ->get($field->identical_to);

                            debug($answer);
                            debug($otherAnswer);
                            if($answer->text != $otherAnswer->text) {
                                // Not identical. Do not save.
                                $allSaved = false;
                                $saveAtEnd = false;
                                $error = sprintf(__('Error. You answer in %s must be identical to your answer in %s.'), $field->caption, $otherField->caption);
                                $this->set(['message' => $error]);
                                $this->Flash->error($error);
                            }
                        }
                        break;
                    case "NumberFields":

                          $field = $this->NumberFields->get($answer->number_field_id);
                          $minNumber = !is_null($field->min_number) ? $field->min_number : 0;
                          if($field->dropdown) {
                            $answer->number += $minNumber-1;
                          }
                          $number = $answer->number;
                          if(is_null($number) && !$moveOnGuarenteed) {
                              $allSaved = false;
                              $saveAtEnd = false;
                              $error = sprintf(__('Please write a number'), $field->caption, $field->min_number);
                              $this->set(['message' => $error]);
                              $this->Flash->error($error);
                          }else if($field->min_number != null && !$moveOnGuarenteed) {
                            if($field->min_number > $number) {
                              $allSaved = false;
                              $saveAtEnd = false;
                              $error = sprintf(__('Error. Your answer in %s must be greater than %s.'), $field->caption, $field->min_number);
                              $this->set(['message' => $error]);
                              $this->Flash->error($error);
                            }
                          }
                          if($field->max_number != null && !$moveOnGuarenteed) {
                            if($field->max_number < $number) {
                              $allSaved = false;
                              $saveAtEnd = false;
                              $error = sprintf(__('Error. Your answer in %s must be smaller than %s.'), $field->caption, $field->max_number);
                              $this->set(['message' => $error]);
                              $this->Flash->error($error);
                            }
                          }
                        break;
                    case "DateFields":
                        if($json) {
                            $answer->datetime = $answerData['datetime'];
                            break;
                        }
                        $answer->know_date  = !$answerData['know_date'];
                        $answer->know_time = !$answerData['know_time'];

                        $hasEmptyFields = false;
                        foreach ($answerData['datetime'] as $key => $v) {
                          if($v == '') {
                            $answerData['datetime'][$key] = "00";

                            if(in_array($key, ["day", "month", "year"]) and $answer->know_date) {
                              $hasEmptyFields = true;
                              $error = __('You must give full date');
                              break;
                            } else if(in_array($key, ["minute", "hour"]) and $answer->know_time) {
                              $hasEmptyFields = true;
                              $error = __('You must give full time');
                              break;
                            }
                          }
                        }
                        if($hasEmptyFields) {
                          $allSaved = false;
                          $saveAtEnd = false;
                          $this->set(['message' => $error]);
                          $this->Flash->error($error);
                        } else if(array_key_exists('minute', $answerData['datetime'])) {
                            $answer->datetime = new Time($answerData['datetime']['year']."-".
                                                         $answerData['datetime']['month']."-".
                                                         $answerData['datetime']['day']." ".
                                                         $answerData['datetime']['hour'].":".

                                                         $answerData['datetime']['minute'].":00");
                        } else {
                            $answer->datetime = new Time($answerData['datetime']['year']."-".
                                                         $answerData['datetime']['month']."-".
                                                         $answerData['datetime']['day']);
                        }
                        break;
                    case "MultipleChoiceFields":
                        /*$field = $this->MultipleChoiceOptions
                                      ->get($answer->multiple_choice_field_id, [
                                          'contain' => ['MultipleChoiceFields']
                                      ]);
                        
                        debug($option);*/
                        if($answerData["multiple_choice_option_id"] == '') {
                            // Checkboxes with no selections should not be saved.
                            // Continue to next answer.
                            $saveAtEnd = false;

                            /*if($field->min_num_choices > 0) {
                                // The user must check some answers.
                                $allSaved = false;
                                $error = sprintf(__('Error: At least %d selection(s) are required.'), $field->min_num_choices);
                                $this->set(['message' => $error]);
                                $this->Flash->error($error);
                            }*/
                        }
                        if(is_array($answerData["multiple_choice_option_id"])) {
                            /*$numAnswers = count($answerData["multiple_choice_option_id"]);
                            if($numAnswers < $field->min_num_choices or
                                $numAnswers > $field->max_num_choices) {
                                // The user must check the right number of answers.
                                $saveAtEnd = false;
                                $allSaved = false;
                                $error = sprintf(__('Error: You must select between %d and %d selection(s).'), $field->min_num_choices, $field->max_num_choices);
                                $this->set(['message' => $error]);
                                $this->Flash->error($error);
                            }*/
                        
                            // Checkboxes with multiple selections - we have to save multiple times.
                            foreach($answerData["multiple_choice_option_id"] as $optionId) {
                                if($optionId == '') {
                                    continue;
                                }
                                $answer = $model->newEntity();
                                $answer->incident_id = $incident_id;
                                $answer->multiple_choice_option_id = $optionId;
                                if(!$model->save($answer)) {
                                    $allSaved = false;
                                }
                            }
                            $saveAtEnd = false; // We have just done the saving.
                        }
                        break;
                    case "MapFields":
                        $field = $this->MapFields->get($answer->map_field_id);
                        if($json) {
                            $locations = [];
                            for($i = 0; $i < count($answerData['latitudes']); $i++) {
                                $locations[] = ['latitude' => $answerData['latitudes'][$i],
                                                'longitude' => $answerData['longitudes'][$i]];
                            }
                            $answerData['locations'] = $locations;
                        }
                        if(array_key_exists("locations", $answerData)) {
                            $numAnswers = count($answerData['locations']);
                            if($numAnswers < $field->min_num_marks && !$moveOnGuarenteed) {
                              $allSaved = false;
                              $saveAtEnd = false;
                              $error = sprintf(__('Error. You must set at least %d mark'), $field->min_num_marks);
                              $this->set(['message' => $error]);
                              $this->Flash->error($error);
                            } else if($numAnswers > $field->max_num_marks && !$moveOnGuarenteed) {
                              $allSaved = false;
                              $saveAtEnd = false;
                              $error = sprintf(__('Error. You can only set %d marks'), $field->max_num_marks);
                              $this->set(['message' => $error]);
                              $this->Flash->error($error);
                            } else {
                              foreach($answerData["locations"] as $location) {
                                $answer = $model->newEntity();
                                $answer = $model->patchEntity($answer, $location);
                                $answer->incident_id = $incident_id;
                                $answer->map_field_id = $answerData["map_field_id"];
                                if(!$model->save($answer)) {
                                    $allSaved = false;
                                }
                              }
                              $saveAtEnd = false; // We have just done the saving.
                            }                            
                        } else if ($field->min_num_marks > 0 && !$moveOnGuarenteed) {
                          $allSaved = false;
                          $saveAtEnd = false;
                          $error = sprintf(__('Error. You must set at least %d mark'), $field->min_num_marks);
                          $this->set(['message' => $error]);
                          $this->Flash->error($error);
                        }
                        break;
                }


                        //$this->set(['message' => $saveAtEnd]);
                if($saveAtEnd) {
                    if(!$model->save($answer)) {
                        $allSaved = false;
                    }
                }
            }
        }
        
        //debug($question->nextQuestionId($incident_id));
        //debug($this->request->data);
        if($allSaved) {
            $nextQuestion = $question->nextQuestionId($incident_id);
            $this->loadModel("Incidents");
            $incident = $this->Incidents->get($incident_id);
            $incident->next_question = $nextQuestion;
            $incident->visited_questions .= $question_id.',';
            $this->Incidents->save($incident);
            if($nextQuestion === null) {
                if($json) {
                    $this->set(['nextQuestion' => -1]);
                    $this->set('_serialize', ['nextQuestion']);
                } else {
                    return $this->redirect(['controller' => 'Questionnaires', 'action' => 'end', $question->questionnaire_id]);
                }
            } else {
                if($json) {
                    $this->set(['nextQuestion' => $nextQuestion]);
                    $this->set('_serialize', ['nextQuestion', 'message']);
                } else {
                    debug($nextQuestion);
                    return $this->redirect(['action' => 'view', $nextQuestion, $incident_id]);
                    //return;
                }
            }
        } else {
            $error = __('Sorry, not all answers could be saved.');
            if($json) {
                $this->set(['message' => $error]);
                return;
            } else {
                #$this->Flash->error($error);
                return $this->redirect(['action' => 'view', $question_id]);
            }
        }
        //debug($this->request->data);
        //return $this->redirect(['action' => 'view', $question_id]);
    }

    public function sortFields()
    {
        $this->request->allowMethod(['patch', 'post', 'put']);
        $order = $this->request->data['fieldOrder'];
        if(!is_array($order) || count($order) < 1) {
            return;
        }
        $question_id = explode(':', $order[0])[0];
        foreach($order as $i => $data) {
            $orderData = explode(':', $data); // [1] contains the controller name and [2] contains the field id.
            $this->loadModel($orderData[1]);
            $field = $this->{$orderData[1]}->get($orderData[2], ['contain' => []]);
            $field->priority = $i;
            $this->{$orderData[1]}->save($field);
        }
        return $this->redirect(['controller' => 'Questions', 'action' => 'edit', $question_id]);
    }

    public function sortConnections()
    {
        $this->request->allowMethod(['patch', 'post', 'put']);
        $order = $this->request->data['connectionOrder'];
        if(!is_array($order) || count($order) < 1) {
            return;
        }
        $question_id = explode(':', $order[0])[0];
        foreach($order as $i => $data) {
            $connection_id = explode(':', $data)[1];
            $this->loadModel('QuestionSequences');
            $connection = $this->QuestionSequences->get($connection_id, ['contain' => []]);
            $connection->priority = $i;
            $this->QuestionSequences->save($connection);
        }
        return $this->redirect(['controller' => 'Questions', 'action' => 'edit', $question_id]);
    }

    private function walk_recursive_remove(array $array, callable $callback) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                $array[$k] = $this->walk_recursive_remove($v, $callback);
            } else {
                if ($callback($v, $k)) {
                    unset($array[$k]);
                }
            }
        }
        return $array;
    }

    public function duplicate($id = null)
    {
        $question = $this->Questions->get($id, ['contain' =>
            ['FreeTextFields',
             'NumberFields',
             'DateFields',
             'MapFields',
             'MultipleChoiceFields',
             'MultipleChoiceFields.MultipleChoiceOptions',
            ]]);


        $questionnaire_id = $question->questionnaire_id;        

        $question->question = "Clone: ".$question->question;

        $questionData = $this->walk_recursive_remove($question->toArray(), function($item, $key) {
            return ($key == 'id'
                or substr($key, -3) == '_id'
                or $key == 'created'
                or $key == 'modified');
        });
        
        $newQuestion = $this->Questions->newEntity($questionData, [
            'associated' => [
                'FreeTextFields',
                'NumberFields',
                'DateFields',
                'MapFields',
                'MultipleChoiceFields',
                'MultipleChoiceFields.MultipleChoiceOptions'
            ]]);
        $newQuestion->questionnaire_id = $questionnaire_id;
        $priority = $this->Questions
                         ->find()
                         ->select('priority')
                         ->where(['questionnaire_id' => $questionnaire_id])
                         ->max('priority')['priority'];
        $newQuestion->priority = $priority === null ? 0 : $priority+1;
        
        if($this->Questions->save($newQuestion)) {
            $this->Flash->success(__('The question has been cloned'));
        } else {
            $this->Flash->error(__('The question could not be cloned. Please, try again.'));
        }
        return $this->redirect(['controller' => 'Questionnaires', 'action' => 'edit', $questionnaire_id]);
    }
}
