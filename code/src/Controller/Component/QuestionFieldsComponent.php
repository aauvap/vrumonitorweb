<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Inflector;

class QuestionFieldsComponent extends Component
{
    public function fieldTypes()
    {
        return ['free_text_field', 'number_field', 'multiple_choice_field', 'date_field', 'map_field'];
    }

    public function maxPriority($question_id)
    {
        $connection = ConnectionManager::get('default');

        $query = 'SELECT priority FROM (';
        foreach($this->fieldTypes() as $key => $field) {
            if($key > 0) {
                $query .= 'UNION ';
            }
            $query .= 'SELECT priority FROM '.Inflector::tableize($field).' WHERE question_id = :id ';
        }
        $query .= ') AS x ORDER BY priority DESC LIMIT 1';
        
        $result = $connection->execute($query, ['id' => $question_id])->fetchAll('assoc');

        if(count($result) == 0) {
            return 0;
        } else {
            return intval($result[0]['priority']);
        }
    }

    public function add($question_id, $fieldModel, $redirectController = 'Questions', $redirectAction = 'edit', $redirectId = null)
    {
        $controller = $this->_registry->getController();
        $field = $fieldModel->newEntity();
        //debug($field);
        
        if ($controller->request->is('post')) {
            $field = $fieldModel->patchEntity($field, $controller->request->data);
            $controller->loadModel('Questions');

            // Override the GET-transmitted id if one has been transmitted via POST:
            if(isset($controller->request->data['questions'])) {
                $question_id = $controller->request->data['questions'];
            }

            if(is_null($question_id)) {
                $controller->Flash->error(__('You cannot add a field which is not associated with a question.'));
            } elseif(!$controller->Questions->exists(['Questions.id' => $question_id])) {
                $controller->Flash->error(__('A question with id {0} does not exist and thus the field cannot be added. Please select a valid question.', [$question_id]));
            } else {
                $field->question_id = $question_id;
                
                // Get the next priority value
                $field->priority = $this->maxPriority($question_id)+1; 
                
                if ($fieldModel->save($field)) {
                    if($redirectId == 'newId') {
                        $redirectId = $field->id;
                    } else if(is_null($redirectId)) {
                        $redirectId = $question_id;
                    }
                    
                    $controller->Flash->success(__('The field has been saved.'));
                    return $controller->redirect([
                        'controller' => $redirectController,
                        'action' => $redirectAction,
                        $redirectId
                    ]);
                } else {
                    $controller->Flash->error(__('The field could not be saved. Please, try again.'));
                }
            }
        }
        $controller->set(compact('field', 'question_id'));
        $controller->set('_serialize', ['field']);
    }

    public function edit($id, $fieldModel)
    {
        $controller = $this->_registry->getController();
        
        $field = $fieldModel->get($id, [
            'contain' => ['Questions']
        ]);
        $question_id = $field->question_id;
        
        if ($controller->request->is(['patch', 'post', 'put'])) {
            $field = $fieldModel->patchEntity($field, $controller->request->data);
            if ($fieldModel->save($field)) {
                $controller->Flash->success(__('The field has been saved.'));
                return $controller->redirect([
                    'controller' => 'Questions',
                    'action' => 'edit',
                    $question_id
                ]);
            } else {
                $controller->Flash->error(__('The field could not be saved. Please, try again.'));
            }
        }
        $controller->set(compact('field', 'question_id'));
        $controller->set('_serialize', ['field']);
    }

    public function delete($id, $fieldModel, $redirectController = 'Questions', $redirectId = null)
    {
        $controller = $this->_registry->getController();
        
        $controller->request->allowMethod(['post', 'delete']);
        $field = $fieldModel->get($id);
        if(is_null($redirectId)) {
            $redirectId = $field->question_id;
        }
        if($fieldModel->delete($field)) {
            $controller->Flash->success(__('The field has been deleted.'));
        } else {
            $controller->Flash->error(__('The field could not be deleted. Please, try again.'));
        }
        return $controller->redirect(['controller' => $redirectController, 'action' => 'edit', $redirectId]);
    }
    

}
?>
