<?php 
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

class QuestionAuthComponent extends Component
{
    public function randomKey($length = 16)
    {
        // This is not cryptographically secure, but should be fine for our purposes.
        // See http://stackoverflow.com/a/6101969/53345
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    
    public function checkAnswerAccess($question, $request, $incidentId)
    {
        if($incidentId !== null) {
            $this->Incidents = TableRegistry::get('Incidents');
            $incident = $this->Incidents->get($incidentId, [
                'contain' => ['Participants']
            ]);

            $incidentKey = $this->getIncidentKey($request, $incidentId);

            if($incidentKey == $incident->incident_key &&
                #$incident->participant->activated &&
                $incident->questionnaire_id == $question->questionnaire_id &&
                !$incident->locked){
                return true;
            }
        }

        return false;
    }

    public function checkIncidentAccess($incident, $request) {
        if($this->getIncidentKey($this->request) != $incident->incident_key) {
            return false;
        }
        return true;
    }

    public function checkParticipantAccess($participant, $request) {
        if($this->getParticipantKey($this->request) != $participant->participant_key) {
            return false;
        }
        return true;
    }

    public function getIncidentId($request, $incidentId)
    {
        if($incidentId == null && $request->session()->check('Incident.id')) {
            return $request->session()->read('Incident.id');
        }

        return $incidentId;
    }

    public function getIncidentKey($request)
    {
        $incidentKey = null;
        if($request->query('key') !== null) {
            $incidentKey = $request->query('key');
            $this->request->session()->write('Incident.key', $incidentKey);
        } elseif($request->session()->check('Incident.key')) {
            $incidentKey = $request->session()->read('Incident.key');
        }
        return $incidentKey;
    }

    public function getParticipantKey($request)
    {
        $participantKey = null;
        if($this->request->query('key') !== null) {
            $participantKey = $this->request->query('key');
            $this->request->session()->write('Participant.key', $participantKey);
        } elseif($this->request->session()->check('Participant.key')) {
            $participantKey = $this->request->session()->read('Participant.key');
        }
        return $participantKey;
    }
}
?>
