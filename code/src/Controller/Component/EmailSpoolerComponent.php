<?php 
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;

class EmailSpoolerComponent extends Component
{
    public function activate($numEmails = 1)
    {
        $this->EmailSpool = TableRegistry::get('EmailSpool');
        $emails = $this->EmailSpool
                       ->find()
                       ->where(['sent' => false])
                       ->where(['valid' => true])
                       ->where(function ($exp, $q) {
                           return $exp->isNull('error');
                       })
                       ->limit($numEmails);
        foreach ($emails as $e) {
          $e->tapped = true;
        }

        foreach($emails as $e) {
            try{
                $email = new Email('default');
                $email->from([$e['sender_address'] => $e['sender_name']])
                      ->to($e['recipient'])
                      ->subject($e['subject'])
                      ->send($e['message']);
                $e->sent = true;
                #debug("Sent ".$e['id']);
            } catch (\InvalidArgumentException $ex) {
                $e->valid = false;
                #debug("InvalidArgumentException: " + $e->id);
                #debug($ex);
            } catch (\Exception $ex) {
                $e->error = explode("Stack trace", $ex)[0];
                #debug("Failed sending ".$e['id']);
                #debug($ex);
            }
            $this->EmailSpool->save($e);
        }
    }
}
?>
