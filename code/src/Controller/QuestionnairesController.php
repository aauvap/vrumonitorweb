<?php
namespace App\Controller;

use Cake\Routing\Router;
use Cake\I18n\I18n;
use Cake\I18n\Time;

/**
 * Questionnaires Controller
 *
 * @property \App\Model\Table\QuestionnairesTable $Questionnaires
 */
class QuestionnairesController extends AppController
{
    public function setLanguage($questionnaireID) {
        $languageCode = $this->Questionnaires->get($questionnaireID)->language;
        $language = null;
        switch ($languageCode) {
            case 'da':
              $language = 'da_DK';
              break;

            case 'sv':
              $language = 'sv_SE';
              break;

            case 'nl':
              $language = 'nl_BE';
              break;

            case 'ca':
              $language = 'ca_ES';
              break;
          }

        I18n::locale($language);
    }



    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('International');
        $this->loadComponent('QuestionAuth');

        $this->Auth->allow('view');
        $this->Auth->allow('newParticipant');
        $this->Auth->allow('newIncident');
        $this->Auth->allow('end');
        $this->Auth->allow('inactive');
        $this->Auth->allow('loop');
        
    }
    
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('questionnaires', $this->paginate($this->Questionnaires));
        $this->set('_serialize', ['questionnaires']);
    }

    /**
     * View method
     */
    public function view($incidentId)
    {
        

        $this->loadModel("Incidents");
        $this->loadModel("Participants");
        
        $incident = $this->Incidents->get($incidentId, [
            'contain' => ['Participants']
        ]);
        
        if(!$this->QuestionAuth->checkIncidentAccess($incident, $this->request)) {
            $this->Flash->error(__('You are not authorized to access this questionnaire. Perhaps you answered it already?'));
            return $this->redirect('/');
        }
        if($incident->locked) {
            if($incident->redirect_incident == -1) {
                $this->Flash->error(__('You have answered this questionnaire already.'));
                return $this->redirect('/pages/already_answered');
            } else {
                return $this->redirect(['action' => 'view','key' => $incident->redirect_key, $incident->redirect_incident]);
            }
        }
        
        $this->setLanguage($incident->questionnaire_id);

        $questionnaire = $this->Questionnaires->get($incident->questionnaire_id);


        if(!$questionnaire->active) {
            return $this->redirect(['action' => 'inactive', $questionnaireId]);
        }

        if($this->request->is('post') && $this->request->data['begin']) {
            if($incident->next_question !== null) {
                $questionId = $incident->next_question;
            } else {
                $questionId = $questionnaire->first_question_id;
            }
            return $this->redirect(['controller' => 'questions', 'action' => 'view', $questionId]);
        }

        $this->request->session()->write('Incident.id', $incidentId);
        
        $this->set('questionnaire', $questionnaire);
        $this->set('_serialize', ['questionnaire']);
    }

    public function newIncident($participantId, $bypassIncidentRestriction = false)
    {
        $this->loadModel("Incidents");
        $this->loadModel("Participants");

        
        $participantKey = $this->QuestionAuth->getParticipantKey($this->request);

        $participant = $this->Participants->get($participantId);

        if($participantKey != $participant->participant_key) {
            $error = __('You are not authorized to access this questionnaire.');
            if(strpos($this->request->url, 'json')) {
                $this->set(['error' => $error]);
                $this->set('_serialize', ['error']);
                return;
            } else {
                $this->Flash->error($error);
                return $this->redirect('/');
            }
        }

        $questionnaire = $this->Questionnaires->get($participant->questionnaire_id);

        if(!$questionnaire->active) {
            return $this->redirect(['action' => 'inactive', $questionnaireId]);
        }
        
        if($questionnaire->restrict_incident_creation and !$bypassIncidentRestriction) {
            $error = __('You are not authorized to create new incidents for this questionnaire.');
            if(strpos($this->request->url, 'json')) {
                $this->set(['error' => $error]);
                $this->set('_serialize', ['error']);
                return;
            } else {
                $this->Flash->error($error);
                return $this->redirect('/');
            }
        }

        if($this->request->is('post')) {
            // If this questionnaire reply is not associated with any incident (and thus a participant), we create a new incident associated with an anonymous participant - but only if the questionnaire is publicly accessible.
            $incident = $this->Incidents->newEntity();
            $incident->participant_id = $participant->id;
            $incident->questionnaire_id = $participant->questionnaire_id;
            $incident->created_from = "newIncident";
            $incidentKey = $this->QuestionAuth->randomKey();
            $incident->incident_key = $incidentKey;
            $saveSuccessful = true;
            if(!$this->Incidents->save($incident)) {
                $this->Flash->error(__('Could not save incident.'));
                $saveSuccessful = false;
            }
            $incidentId = $incident->id;

            if($this->request->data != null) {
                $this->loadModel("SensorDumps");
                $data = $this->SensorDumps->newEntity();
                $data->incident_id = $incidentId;
                $data->data = json_encode($this->request->data);
                $this->SensorDumps->save($data);
            }
            
            $this->request->session()->write('Incident.id', $incidentId);
            $this->request->session()->write('Incident.key', $incidentKey);
            if($saveSuccessful) {
                if(strpos($this->request->url, 'json')) {
                    $this->set(['incidentId' => $incidentId,
                                'incidentKey' => $incidentKey,
                                'firstQuestion' => $questionnaire->first_question_id]);
                    $this->set('_serialize', ['incidentId', 'incidentKey', 'firstQuestion']);
                    return;
                } else {
                    return $this->redirect(['controller' => 'questions', 'action' => 'view', $questionnaire->first_question_id]);
                }
            }
        }
        $this->set(['questionnaire' => $questionnaire,
                    'title' => $questionnaire->title,
                    'intro_text' => $questionnaire->intro_text,
        ]);
        $this->set('_serialize', ['title', 'intro_text']);
    }

    public function newParticipant($questionnaireId)
    {
        $this->loadModel("Incidents");
        $this->loadModel("Participants");

        $questionnaire = $this->Questionnaires->get($questionnaireId);

        if(!$questionnaire->active) {
            return $this->redirect(['action' => 'inactive', $questionnaireId]);
        }

        if($questionnaire->restrict_access) {
            $error = __('This questionnaire is only accessible to pre-registered participants.');
            if(strpos($this->request->url, 'json')) {
                $this->set(['error' => $error]);
                $this->set('_serialize', ['error']);
                return;
            } else {
                $this->Flash->error($error);
                return $this->redirect('/');
            }
        }

        $this->setLanguage($questionnaireId);
        
        if($this->request->is('post')) {
            $id = $this->request->query('id');
            // If this questionnaire reply is not associated with any incident (and thus a participant), we create a new incident associated with an anonymous participant - but only if the questionnaire is publicly accessible.
            if($id == null) {
                $participant = $this->Participants->newEntity();
                $participant->email = "anonymous";
                $participant->questionnaire_id = $questionnaireId;
                $participant->activated = true;
                $participantKey = $this->QuestionAuth->randomKey();
                $participant->participant_key = $participantKey;
                $saveSuccessful = true;
                if(!$this->Participants->save($participant)) {
                    $this->Flash->error(__('Could not save participant.'));
                    $saveSuccessful = false;
                }
            } else {
                $participant = $this->Participants->get($id);
                $participantKey = $participant->participant_key;
                $participant->questionnaire_id = $questionnaireId;
                $saveSuccessful = true;
            }
            
            $incident = $this->Incidents->newEntity();
            $incident->participant_id = $participant->id;
            $incident->questionnaire_id = $questionnaireId;
            $incident->created_from = "newParticipant";
            $incidentKey = $this->QuestionAuth->randomKey();
            $incident->incident_key = $incidentKey;
            if(!$this->Incidents->save($incident)) {
                $this->Flash->error(__('Could not save incident.'));
                $saveSuccessful = false;
            }
            $incidentId = $incident->id;
            $this->request->session()->write('Incident.id', $incidentId);
            $this->request->session()->write('Incident.key', $incidentKey);
            if($saveSuccessful) {
                if(strpos($this->request->url, 'json')) {
                    $this->set(['participantId' => $participant->id,
                                'participantKey' => $participantKey,
                                'incidentId' => $incidentId,
                                'incidentKey' => $incidentKey,
                                'firstQuestion' => $questionnaire->first_question_id]);
                    $this->set('_serialize', ['participantId', 'participantKey', 'incidentId', 'incidentKey', 'firstQuestion']);
                    return;
                } else {
                    return $this->redirect(['controller' => 'questions', 'action' => 'view', $questionnaire->first_question_id]);
                }
            }
        }
        $this->set(['questionnaire' => $questionnaire,
                    'title' => $questionnaire->title,
                    'intro_text' => $questionnaire->intro_text,
        ]);
        $this->set('_serialize', ['title', 'intro_text']);
    }
    
    public function end($questionnaireId = null, $incidentId = null)
    {
        $json = false;
        if(strpos($this->request->url, 'json')) {
            $questionnaireId = $this->request->params['questionnaireid'];
            $incidentId = $this->request->params['incidentid'];
            $json = true;
        }
        
        $incidentId = $this->QuestionAuth->getIncidentId($this->request, $incidentId);
        $this->loadModel("Incidents");
        $incident = $this->Incidents->get($incidentId);
        if(!$this->QuestionAuth->checkIncidentAccess($incident, $this->request)) {
            $error = __('You are not authorized to access this questionnaire.');
            if($json) {
                $this->set('error', $error);
                $this->set('_serialize', ['error']);
                return;
            } else {
                $this->Flash->error($error);
                return $this->redirect('/');
            }
        }
        $incident->locked = true;
        $this->Incidents->save($incident);
        $questionnaire = $this->Questionnaires->get($questionnaireId);
        $this->set(['questionnaire' => $questionnaire, "title" => $questionnaire->title, "end_text" => $questionnaire->end_text]);
        
        if($json && $this->request->query('target_questionnaire') !== null
            && $this->request->query('email_field') !== null) {
            //// This is for creating new participants automatically
            //// from the app, if they have answered the sign up
            //// questionnaire fully.
            
            // First we get the e-mail:
            $this->loadModel("FreeTextAnswers");
            $email = $this->FreeTextAnswers
                          ->find()
                          ->where(['incident_id' => $incident->id,
                                   'free_text_field_id' => $this->request->query('email_field')])
                          ->first();
            
            // Then we update the participant to point to the new questionnaire (and to have an e-mail):
            $this->loadModel("Participants");
            $participant = $this->Participants
                 ->find()
                 ->where(['id' => $incident->participant_id])
                 ->first();
            
            $participant->email = $email['text'];
            $participant->questionnaire_id = $this->request->query('target_questionnaire');
            $participant->activated = true;
            $participantKey = $this->QuestionAuth->randomKey();
            $participant->participant_key = $participantKey;
            if(!$this->Participants->save($participant)) {
                $error = __('Could not save participant.');
                $this->set('_serialize', ['error']);
                return;
            }
            /*$incident = $this->Incidents->newEntity();
            $incident->participant_id = $participant->id;
            $incident->questionnaire_id = $this->request->query('target_questionnaire');
            $incidentKey = $this->QuestionAuth->randomKey();
            $incident->incident_key = $incidentKey;
            if(!$this->Incidents->save($incident)) {
                $error = __('Could not save incident.');
                $this->set('_serialize', ['error']);
                return;
            }
            $incidentId = $incident->id;*/
            $this->set(['participantId' => $participant->id,
                        'participantKey' => $participantKey]);
            $this->set('_serialize', ['title', 'end_text', 'participantId', 'participantKey']);
            return;
        }
        $this->set('_serialize', ['title', 'end_text']);
        $this->request->session()->delete('Incident.id');
        $this->request->session()->delete('Incident.key');
        $this->request->session()->delete('Participant.key');
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $questionnaire = $this->Questionnaires->newEntity();
        if ($this->request->is('post')) {
            $questionnaire = $this->Questionnaires->patchEntity($questionnaire, $this->request->data);
            if ($this->Questionnaires->save($questionnaire)) {
                $this->Flash->success(__('The questionnaire has been saved.'));
                return $this->redirect(['action' => 'edit', $questionnaire->id]);
            } else {
                $this->Flash->error(__('The questionnaire could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('questionnaire'));
        $this->set('languageCodes', $this->International->languageCodes());
        $this->set('_serialize', ['questionnaire']);
    }


    private function findEmptyCol($grid, $rowNum) {
        $col = 0;
        while(isset($grid[$rowNum][$col])) {
            $col++;
        }
        return $col;
    }

    private function createRoutes($questions) {
        $gridRowNums = []; // Will contain the table row number of each question id.
        foreach($questions as $i => $q) {
            $gridRowNums[$q->id] = $i;
        }

        $routes = [];
        
        foreach($questions as $currentRow => $question) {
            foreach($question->question_sequences as $s) {
                if(!is_null($s->to_question_id)) {
                    $destinationRow = $gridRowNums[$s->to_question_id];
                } else {
                    $destinationRow = $gridRowNums[$question->id];
                }
                $routes[] = new \ArrayObject(["id" => $s->id, "fromId" => $question->id, "toId" => $s->to_question_id, "fromRow" => $currentRow, "toRow" => $destinationRow, "distance" => ($destinationRow-$currentRow)], \ArrayObject::ARRAY_AS_PROPS);
            }
        }
        return $routes;
    }

    private function findNodeCol($id, $grid, $rowNum) {
        if(!isset($grid[$rowNum])) {
            return 0;
        } else {
            $col = array_search('n'.$id, $grid[$rowNum]);
            if($col === false) {
                $col = $this->findEmptyCol($grid, $rowNum);
            }
            return $col;
        }
    }

    private function drawNode($id, $pos, $class = 'node', $radius = 2, $hoverRadius = 8) {
        $node = '<circle cx="'.$pos[0].'" cy="'.$pos[1].'" r="'.$radius.'" class="'.$class.'" id="node_'.$id.'_'.implode("-",$pos).'" />'.PHP_EOL;
        $node .= '<circle cx="'.$pos[0].'" cy="'.$pos[1].'" r="'.$hoverRadius.'" class="'.$class.' transparent" id="invisibleNode_'.$id.'_'.implode("-",$pos).'" />'.PHP_EOL;
        return $node;
    }

    private function drawPath($r, $fromCol, &$nodeGrid, $origin, $offset) {
        $pathString = '<path id="edge_'.$r->id.'_'.$r->fromId.'-'.$r->toId.'" class="edge" d="M '.($origin[0]+$fromCol*$offset[0]).' '.($origin[1]+$r->fromRow*$offset[1]);
        $prevCol = $fromCol;
        for($row = $r->fromRow+1; $row <= $r->toRow; $row++) { // Fill out control points along the line
            if($row != $r->toRow) {
                $col = $this->findEmptyCol($nodeGrid, $row);
                $nodeGrid[$row][$col] = 'edge';
            } else {
                $col = $this->findNodeCol($r->toId, $nodeGrid, $row);
            }
            if($col != $prevCol) {
                // c 0 17 '.$branchWidth*$offsetX.' 17 '.$branchWidth*$offsetX.' 37
                $pathString .= ' c 0 17 '.(($col-$prevCol)*$offset[0]).' 17 '.(($col-$prevCol)*$offset[0]).' '.$offset[1];
            } else {
                $pathString .= ' l'.(($col-$prevCol)*$offset[0]).' '.$offset[1];
            }
            $prevCol = $col;
        }
        $pathString .= '" />'.PHP_EOL;

        // Duplicate the path to make a transparent hover/click zone:
        $pathString .= str_replace('class="edge"', 'class="hover transparent"',
                                   str_replace('id="e', 'id="invisibleE', $pathString));

        return $pathString;
    }

    private function recursive_array_search($needle,$haystack) {
        foreach($haystack as $key=>$value) {
            $current_key=$key;
            if($needle===$value OR (is_array($value) && $this->recursive_array_search($needle,$value) !== false)) {
                return $current_key;
            }
        }
        return false;
    }
    

    /**
     * Edit method
     *
     * @param string|null $id Questionnaire id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        /*$questionnaire = $this->Questionnaires->get($id, [
            'contain' => ['Questions',
                          'Questions.QuestionSequences',
                          'Questions.QuestionSequences.SequenceConditions',
                          'Questions.QuestionSequences.SequenceConditions.MultipleChoiceOptions',
                          'Questions.QuestionSequences.SequenceConditions.MultipleChoiceOptions.MultipleChoiceFields.Questions'
            ]
        ]);*/
        $questionnaire = $this->Questionnaires->get($id, [
            'contain' => ['Questions.FreeTextFields', 'Questions.MultipleChoiceFields.MultipleChoiceOptions']
        ]);

        $emailFields = ['' => ''];
        foreach($questionnaire->questions as $q) {
            foreach($q['free_text_fields'] as $f) {
                $emailFields["Q".$q->id.": ".$q->question][$f->id] = $f->caption;
            }
        }

        $query = $this->Questionnaires->find()->select(['id', 'title'])->toArray();
        foreach($query as $q) {
            $allQuestionnaires[$q['id']] = $q['id'] . ": " . $q['title'];
        }
        
        $this->loadModel('Questions');
        $questions = $this->Questions->find()->where(["questionnaire_id" => $id])->contain([
            'QuestionSequences',
            'QuestionSequences.SequenceConditions',
            'QuestionSequences.SequenceConditions.MultipleChoiceOptions',
            'QuestionSequences.SequenceConditions.MultipleChoiceOptions.MultipleChoiceFields.Questions'
        ])->order(['priority' => 'ASC']);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $questionnaire = $this->Questionnaires->patchEntity($questionnaire, $this->request->data);
            if ($this->Questionnaires->save($questionnaire)) {
                $this->Flash->success(__('The questionnaire has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The questionnaire could not be saved. Please, try again.'));
            }
        }

        $originX = 5;
        $originY = 56;
        $offsetX = 15;
        $offsetY = 37;
        $radius = 2;
        $nodeGrid = [];
        $routes = []; // Will contain routes formatted as [fromRow, toRow, distance]
        $svg = '';

        // Create grid maps, start with the longest routes.
        $routes = $this->createRoutes($questions);
        usort($routes, function($a, $b) { return $a['distance'] < $b['distance']; });

        foreach($routes as $r) {
            $fromCol = $this->findNodeCol($r->fromId, $nodeGrid, $r->fromRow);
            // Create our departure node, unless it exists already:
            if(!isset($nodeGrid[$r->fromRow][$fromCol])) {
                $nodeGrid[$r->fromRow][$fromCol] = 'n'.$r->fromId;
            }
            // If we have a terminating node, we'll just draw that and carry on:
            if(is_null($r->toId)) {
                $col = $this->findEmptyCol($nodeGrid, $r->fromRow);
                $svg .=  '<path d="M'.($originX+$fromCol*$offsetX).' '.($originY+$r->fromRow*$offsetY).' L '.($originX+$col*$offsetX).' '.($originY+$r->fromRow*$offsetY).'" class="edge" id="edge__'.$r->fromId.'-end" />'.PHP_EOL;
                $svg .= $this->drawNode('null', [$originX+$col*$offsetX, $originY+$r->fromRow*$offsetY], "node endNode");
                continue;
            }

            // Draw the path
            $svg .=  $this->drawPath($r, $fromCol, $nodeGrid, [$originX, $originY], [$offsetX, $offsetY]);

            // Create the destination node
            $col = $this->findNodeCol($r->toId, $nodeGrid, $r->toRow);
            if(!isset($nodeGrid[$r->toRow][$col])) {
                $nodeGrid[$r->toRow][$col] = 'n'.$r->toId;
            }
        }


        // Add nodes for questions without any connections:
        foreach($questions as $i => $q) {
            $nodeId = 'n'.$q->id;
            if($this->recursive_array_search($nodeId, $nodeGrid) === false) {
                $col = $this->findEmptyCol($nodeGrid, $i);
                $nodeGrid[$i][$col] = $nodeId;
            }
        }
        
        // Draw all nodes (we wait until now to make sure they are on top of the paths)
        foreach($nodeGrid as $rowNum => $row) {
            foreach($row as $colNum => $value) {
                if($value != "edge") {
                    $id = substr($value, 1);
                    $classes = "node";
                    
                    // We do not allow branching if the question has no multiple choice fields:
                    $q = $this->Questions->get($id, [
                        'contain' => [
                            'MultipleChoiceFields',
                            'QuestionSequences'
                        ]]);
                    if(count($q->multiple_choice_fields) == 0
                        && count($q->question_sequences) > 0) {
                        $classes .= " noBranching";
                    }
                    
                    $svg .= $this->drawNode($id, [$originX+$colNum*$offsetX, $originY+$rowNum*$offsetY], $classes);
                }
            }
        }

        $treeWidth = 0;
        if(!empty($nodeGrid)) {
            $treeWidth = 2*$originX + $offsetX*max(array_map(function($row){return count($row);}, $nodeGrid));
        }

        $this->loadModel('QuestionSequences');
        $questionSequence = $this->QuestionSequences->newEntity();

        $this->loadModel('Participants');
        $numParticipants = $this->Participants
                                ->find()
                                ->where(['questionnaire_id' => $questionnaire->id])
                                ->count();
        $numActivations = $this->Participants
                               ->find()
                               ->where(['questionnaire_id' => $questionnaire->id,
                                         'activated' => true])
                               ->count();
        $this->loadModel('Incidents');
        $numAnswered = $this->Incidents
                            ->find()
                            ->where(['questionnaire_id' => $questionnaire->id,
                                     'locked' => true])
                            ->count();
        
        $this->set(compact('questionnaire', 'questions', 'svg', 'treeWidth', 'questionSequence', 'questionSequences', 'emailFields', 'allQuestionnaires', 'numParticipants', 'numActivations', 'numAnswered'));
        $this->set('languageCodes', $this->International->languageCodes());
        $this->set('_serialize', ['questionnaire']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Questionnaire id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id)
    {
        $this->request->allowMethod(['post', 'delete']);
        $questionnaire = $this->Questionnaires->get($id);
        if ($this->Questionnaires->delete($questionnaire)) {
            $this->Flash->success(__('The questionnaire has been deleted.'));
        } else {
            $this->Flash->error(__('The questionnaire could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    private function walk_recursive_remove(array $array, callable $callback) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                $array[$k] = $this->walk_recursive_remove($v, $callback);
            } else {
                if ($callback($v, $k)) {
                    unset($array[$k]);
                }
            }
        }
        return $array;
    }

    private function newQuestionId($oldId, $questionnaire, $newQuestionnaire) {
        foreach($questionnaire->questions as $qKey => $question) {
            if($question->id == $oldId) {
                return $newQuestionnaire->questions[$qKey]->id;
            }
        }
    }

    private function newOptionId($oldId, $questionnaire, $newQuestionnaire) {
        foreach($questionnaire->questions as $qKey => $question) {
            foreach($question->multiple_choice_fields as $fKey => $field) {
                foreach($field->multiple_choice_options as $oKey => $option) {
                    if($option->id == $oldId) {
                        return $newQuestionnaire->questions[$qKey]->multiple_choice_fields[$fKey]->multiple_choice_options[$oKey]->id;
                    }
                }
            }
        }
    }
    
    public function duplicate($id = null)
    {
        $questionnaire = $this->Questionnaires->get($id, ['contain' =>
            ['Questions',
             'Questions.FreeTextFields',
             'Questions.NumberFields',
             'Questions.DateFields',
             'Questions.MapFields',
             'Questions.MultipleChoiceFields',
             'Questions.MultipleChoiceFields.MultipleChoiceOptions',
             'Questions.QuestionSequences',
             'Questions.QuestionSequences.SequenceConditions'
            ]]);

        

        $questionnaire->title = "Clone: ".$questionnaire->title;

        $questionnaireData = $this->walk_recursive_remove($questionnaire->toArray(), function($item, $key) {
            return ($key == 'id'
                or substr($key, -3) == '_id'
                or $key == 'created'
                or $key == 'modified');
        });
        
        $newQuestionnaire = $this->Questionnaires->newEntity($questionnaireData, [
            'associated' => [
                'Questions' => ['associated' => [
                    'FreeTextFields',
                    'NumberFields',
                    'DateFields',
                    'MapFields',
                    'MultipleChoiceFields',
                    'MultipleChoiceFields.MultipleChoiceOptions',
                    'QuestionSequences',
                    'QuestionSequences.SequenceConditions'
                ]]
            ]]);
        
        if($this->Questionnaires->save($newQuestionnaire)) {
            // Update the to_question_id in sequences to correspond to the new id.
            foreach($questionnaire->questions as $qKey => $question) {
                foreach($question->question_sequences as $sKey => $sequence) {
                    $newQuestionnaire->questions[$qKey]->question_sequences[$sKey]->to_question_id = $this->newQuestionId($sequence->to_question_id, $questionnaire, $newQuestionnaire);
                    $newQuestionnaire->questions[$qKey]->dirty('question_sequences', true);
                }
            }
            $newQuestionnaire->dirty('questions', true); // Mark dirty so the change is saved.

            // Update the multiple_choice_option_id in conditions to correspond to the new id.
            foreach($questionnaire->questions as $qKey => $question) {
                foreach($question->question_sequences as $sKey => $sequence) {
                    foreach($sequence->sequence_conditions as $cKey => $condition) {
                        $newQuestionnaire->questions[$qKey]->question_sequences[$sKey]->sequence_conditions[$cKey]->multiple_choice_option_id = $this->newOptionId($condition->multiple_choice_option_id, $questionnaire, $newQuestionnaire);
                        $newQuestionnaire->questions[$qKey]->question_sequences[$sKey]->dirty('sequence_conditions', true);
                        $newQuestionnaire->questions[$qKey]->dirty('question_sequences', true);
                    }
                }
            }
            $newQuestionnaire->dirty('questions', true); // Mark dirty so the change is saved.
            
            if($this->Questionnaires->save($newQuestionnaire)) {
                $this->Flash->success(__('The questionnaire has been cloned'));
            } else {
                $this->Flash->success(__('Due to an unknown error, the question sequences could not be cloned. The rest of the questionnaire has been cloned.'));
            }
            return $this->redirect(['action' => 'edit', $newQuestionnaire->id]);
        } else {
            $this->Flash->error(__('The questionnaire could not be cloned. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    private function sendRoutine($questionnaireID, $participants) {
        foreach($participants as $p) {
            $incident = $this->Incidents->newEntity();
            $incident->participant_id = $p->id;
            $incident->created_from = "sendIncident";
            $incident->incident_key = $this->QuestionAuth->randomKey();
            $incident->questionnaire_id = $questionnaireID;
            $this->Incidents->save($incident);
                
            $email = $this->EmailSpool->newEntity();
            $email->recipient = $p['email'];
            $email->sender_address = trim($this->request->data['email_sender_address']);
            $email->sender_name = $this->request->data['email_sender_name'];
            $email->subject = $this->request->data['email_subject'];
            $email->message = str_replace('%link%',
                               Router::url(['controller' => 'Questionnaires',
                                            'action' => 'view',
                                            $incident->id,
                                            '?' => ['key' => $incident->incident_key]], true)." ",
                                              $this->request->data['email']);
            $this->EmailSpool->save($email);
        }
    }

    public function sendIncidentSpecific($questionnaireId) {
        
        if ($this->request->is('post')) {
            $success = false;
            $confirm = $this->request->data['confirm'];
            if(strpos($this->request->data['email'], '%link%') === false) {
                $this->Flash->error(__('The e-mail must contain the placehoder %link%.'));
                return $this->redirect([$questionnaireId]);
            }

            $this->loadModel('EmailSpool');
            $this->loadModel('Incidents');
            $this->loadModel('Participants');

            $participants = [];
            $participantsEmails = [];
            $sendToQuestionnaire = $this->request->data['which_questionnaire'];
            if($confirm) {
                $participants = explode(" ", $this->request->data['participants']);
                debug($participants);
                foreach ($participants as $key => $p) {
                    $participants[$key] = $this->Participants->get($p);
                }
                $this->sendRoutine($sendToQuestionnaire, $participants);
                $success = true;
            } else {
                $confirm = true;
                #Convert from full text to ids.
                foreach (explode(PHP_EOL, $this->request->data['participantIds']) as $id) {
                    if($id != "") {
                        $id = intval($id);
                        $participants[] = $id;
                        $participantsEmails[$id] = $this->Participants->get($id)->email;
                    }
                }
            }
            if($success) {
                return $this->redirect(['action' => 'edit', $questionnaireId]);
            }
        } else {
            $confirm = false;
        }
        $this->set('sampleSubject',
                   __('Request for participation in study'));
        $this->set('sampleSenderName',
                   __('John Doe, Famous University'));
        $this->set('sampleSenderAddress',
                   __('john.doe@famuni.edu'));
        $this->set('sampleEmail',
                   __('Hi,
Here is a questionnaire. Please click the link %link% to answer.
Thank you!'));
        $this->set(compact('questionnaireId', 'confirm', 'participants', 'participantsEmails'));

    }

    public function sendIncident($questionnaireId)
    {
        $this->loadModel('Participants');
        $participants = $this->Participants
                             ->find()
                             ->where(['questionnaire_id' => $questionnaireId,
                                      'activated' => true]);
        
        if ($this->request->is('post')) {
            if(strpos($this->request->data['email'], '%link%') === false) {
                $this->Flash->error(__('The e-mail must contain the placehoder %link%.'));
                return $this->redirect([$questionnaire_id]);
            }

            $this->loadModel('EmailSpool');
            $this->loadModel('Incidents');

            if($this->request->data['include_non_activated']) {
                $participants = $this   -> Participants
                                        ->find()
                                        ->where(['questionnaire_id' => $questionnaireId]);
            }
            $sendToQuestionnaire = $this->request->data['which_questionnaire'];
            $this->sendRoutine($sendToQuestionnaire, $participants);
            
            if($participants->count() == 0) {
                $this->Flash->error(__('No participants passed the given conditions. No new incidents created.'));
            } else {
                $this->Flash->success(__(sprintf('%d incident(s) created.', $participants->count())));
                return $this->redirect(['action' => 'edit', $questionnaireId]);
            }
        }

        $this->set("numIncidents", $participants->count());
        $this->set("totalIncidents", $this->Participants
                             ->find()
                             ->where(['questionnaire_id' => $questionnaireId])
                             ->count());
        $this->set('sampleSubject',
                   __('Request for participation in study'));
        $this->set('sampleSenderName',
                   __('John Doe, Famous University'));
        $this->set('sampleSenderAddress',
                   __('john.doe@famuni.edu'));
        $this->set('sampleEmail',
                   __('Hi,
Here is a questionnaire. Please click the link %link% to answer.
Thank you!'));
        $this->set(compact('questionnaireId'));
    }    

    public function sendReminder($questionnaireId)
    {
        $this->loadModel('Incidents');
        $incidents = $this->Incidents
                          ->find()
                          ->contain('Participants')
                          ->where(['Incidents.questionnaire_id' => $questionnaireId,
                                   'locked' => false]);
        
        if ($this->request->is('post')) {
            if(strpos($this->request->data['email'], '%link%') === false) {
                $this->Flash->error(__('The e-mail must contain the placehoder %link%.'));
                return $this->redirect([$questionnaire_id]);
            }

            $this->loadModel('EmailSpool');
            foreach($incidents as $i) {
                $email = $this->EmailSpool->newEntity();
                $email->recipient = $i['participant']['email'];
                $email->sender_address = trim($this->request->data['email_sender_address']);
                $email->sender_name = $this->request->data['email_sender_name'];
                $email->subject = $this->request->data['email_subject'];
                $email->message = str_replace('%link%',
                                   Router::url(['controller' => 'Questionnaires',
                                                'action' => 'view',
                                                $i->id,
                                                '?' => ['key' => $i->incident_key]], true)." ",
                                                  $this->request->data['email']);
                $this->EmailSpool->save($email);
            }
            
            if($incidents->count() == 0) {
                $this->Flash->error(__('No participants passed the given conditions. No e-mails sent.'));
            } else {
                $this->Flash->success(__(sprintf('%d e-mail(s) put in queue.', $incidents->count())));
                return $this->redirect(['action' => 'index']);
            }
        }

        $this->set("numReminders", $incidents->count());
        $this->set('sampleSubject',
                   __('Reminder about study'));
        $this->set('sampleSenderName',
                   __('John Doe, Famous University'));
        $this->set('sampleSenderAddress',
                   __('john.doe@famuni.edu'));
        $this->set('sampleEmail',
                   __('Hi,
You recently signed up for a questionnaire, but never finished answering it. Please click the link %link% to do so.
Thank you!'));
        $this->set(compact('questionnaireId'));
    }

    public function sendActivationReminder($questionnaireId)
    {
        $this->loadModel('Participants');
        $participants = $this->Participants
                             ->find()
                             ->where(['questionnaire_id' => $questionnaireId,
                                      'activated' => false]);
        
        if ($this->request->is('post')) {
            if(strpos($this->request->data['email'], '%link%') === false) {
                $this->Flash->error(__('The e-mail must contain the placehoder %link%.'));
                return $this->redirect([$questionnaire_id]);
            }

            $this->loadModel('EmailSpool');
            foreach($participants as $p) {
                $email = $this->EmailSpool->newEntity();
                $email->recipient = $p['email'];
                $email->sender_address = trim($this->request->data['email_sender_address']);
                $email->sender_name = $this->request->data['email_sender_name'];
                $email->subject = $this->request->data['email_subject'];
                $email->message = str_replace('%link%',
                                              Router::url(['controller' => 'Participants',
                                                           'action' => 'activate',
                                                           $p['id'],
                                                           '?' => ['key' => $p['participant_key']]], true)." ",
                                              $this->request->data['email']);
                $this->EmailSpool->save($email);
            }
            
            if($participants->count() == 0) {
                $this->Flash->error(__('No participants passed the given conditions. No new e-mail created.'));
            } else {
                $this->Flash->success(__(sprintf('%d e-mail(s) put in queue.', $participants->count())));
                return $this->redirect(['action' => 'index']);
            }
        }

        $this->set("numReminders", $participants->count());
        $this->set('sampleSubject',
                   __('Reminder about study'));
        $this->set('sampleSenderName',
                   __('John Doe, Famous University'));
        $this->set('sampleSenderAddress',
                   __('john.doe@famuni.edu'));
        $this->set('sampleEmail',
                   __('Hi,
You recently signed up for a questionnaire, but never activated your participation. Please click the link %link% to do so.
Thank you!'));
        $this->set(compact('questionnaireId'));
    }

    public function sortQuestions()
    {
        $this->request->allowMethod(['patch', 'post', 'put']);
        $order = $this->request->data['questionOrder'];
        if(!is_array($order) || count($order) < 1) {
            return;
        }
        $questionnaire_id = explode(':', $order[0])[0];
        $orderedIds = [];
        foreach($order as $i => $data) {
            $orderedIds[] = explode(':', $data)[1];
        }
        $this->loadModel('Questions');
        $this->loadModel('QuestionSequences');
        
        // Check if the new order is legal
        foreach($orderedIds as $i => $qid) {
            
            //debug($qid." as ".$i);
            $fromConnections = $this->QuestionSequences
                                    ->find()
                                    ->select('question_id')
                                    ->where(['to_question_id' => $qid]);
            foreach($fromConnections as $c) {
                if(array_search($c['question_id'], $orderedIds) > $i) {
                    //debug($qid." cannot be in position ".$i." as it is has a connection from ".$c['question_id']);
                    $this->Flash->error("Q".$qid." cannot be positioned before Q".$c['question_id']." due to connections.");
                    return $this->redirect(['controller' => 'Questionnaires', 'action' => 'edit', $questionnaire_id]);
                }
            }

            
            
            $toConnections = $this->QuestionSequences
                                  ->find()
                                  ->select('to_question_id')
                                  ->where(['question_id' => $qid]);
            foreach($toConnections as $c) {
                if($c['to_question_id'] === null) {
                    continue;
                }
                if(array_search($c['to_question_id'], $orderedIds) < $i) {
                    //debug($qid." cannot be in position ".$i." as it is has a connection to ".$c['to_question_id']);
                    $this->Flash->error("Q".$qid." cannot be positioned after Q".$c['to_question_id']." due to connections.");
                    return $this->redirect(['controller' => 'Questionnaires', 'action' => 'edit', $questionnaire_id]);
                }
            }
        }
        
        
        foreach($order as $i => $data) {
            $question_id = explode(':', $data)[1];
            $question = $this->Questions->get($question_id);
            $question->priority = $i;
            if(!$this->Questions->save($question)) {
                $this->Flash->error("Unknown error: Could not save order.");
            }
        }
        return $this->redirect(['controller' => 'Questionnaires', 'action' => 'edit', $questionnaire_id]);
    }
    private $maxEntriesPerOutput = 5000;
    public function viewAnswers($questionnaireId, $page = 0)
    {
        ini_set('memory_limit', '-1'); 
        $questionnaire = $this->Questionnaires->get($questionnaireId, [
            'contain' => [
                'Questions',
                'Questions.FreeTextFields',
                'Questions.NumberFields',
                'Questions.DateFields',
                'Questions.MultipleChoiceFields',
                'Questions.MultipleChoiceFields.MultipleChoiceOptions',
                'Questions.MapFields'
            ]
        ]);
        $this->loadModel('Incidents');
        $incidents = $this->Incidents->find()
              ->where(['Incidents.questionnaire_id' => $questionnaireId])
              ->contain([
                  'Participants',
                  'FreeTextAnswers',
                  'NumberAnswers',
                  'DateAnswers',
                  'MultipleChoiceAnswers',
                  'MultipleChoiceAnswers.MultipleChoiceOptions',
                  'MapAnswers',
              ])
              ->limit($this->maxEntriesPerOutput)
              ->page($page);#MBC was here 170629
        
        $this->set(compact('questionnaireId', 'questionnaire', 'incidents'));

        $this->response->type('Content-Type: text/csv');
        $this->viewBuilder()->layout('csv');
        $this->response->download("q".$questionnaireId . " at " . Time::now()->i18nFormat("yyyy-MM-dd HH'h'") . " page " . $page . ".txt");
    }

    public function viewAnswersDetailed($questionnaireId) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            return $this->redirect([
                'controller' => 'Questionnaires', 
                'action' => 'viewAnswers', 
                $questionnaireId, 
                $this->request->data['include_non_locked'],
                $this->request->data['test_mode']
            ]);
        }
        $this->loadModel('Incidents');
        $incidents = $this->Incidents->find()
              ->where(['Incidents.questionnaire_id' => $questionnaireId]);
        $count = $incidents->count();
        $maxEntriesPerOutput = $this->maxEntriesPerOutput;
        $this->set(compact('questionnaireId', 'count', "maxEntriesPerOutput"));

    }

    public function viewParticipants($questionnaireId) {
        ini_set('memory_limit', '-1'); 
        $this->loadModel('Incidents');
        $incidents = $this->Incidents->find()
              ->where(['Incidents.questionnaire_id' => $questionnaireId]);
        $links = array();
        foreach ($incidents as $id => $incident) {
            $link = Router::url(['controller' => 'Questionnaires',
                                                'action' => 'view',
                                                $incident->id,
                                                '?' => ['key' => $incident->incident_key]], true);
            
            $links[$id] = $link;
        }
        $this->set(compact('questionnaireId',  'incidents', 'links'));

        $this->response->type('Content-Type: text/csv');
        $this->viewBuilder()->layout('csv');
        $this->response->download($questionnaireId . 'participants.txt');
    }

    public function inactive($questionnaireId)
    {
        $questionnaire = $this->Questionnaires->get($questionnaireId);
        $this->set('questionnaire', $questionnaire);
    }

    public function createLinks($questionnaireId) {
        $this->loadModel('Participants');
        $this->loadModel('Incidents');
        $this->set(compact('questionnaireId'));
        $textArea = '';
        if ($this->request->is(['patch', 'post', 'put'])) {
            $textArea = '<textarea rows = "'.$this->request->data['number'].'" cols = "50">';

            for($i = 0; $i < $this->request->data['number']; $i++) {
                $participant = $this->Participants->newEntity();
                $participant->email = "anonymous";
                $participant->participant_key = $this->QuestionAuth->randomKey();
                $participant->questionnaire_id = $questionnaireId;
                if($this->Participants->save($participant)) {
                    $participantId = $participant->id;
                } else {
                    debug("Error Saving Participant");
                    $textArea = '';
                    break;
                }

                $incident = $this->Incidents->newEntity();
                $incident->participant_id = $participantId;
                $incident->created_from = "createLinks";
                $incident->questionnaire_id = $questionnaireId;
                $incidentKey = $this->QuestionAuth->randomKey();
                $incident->incident_key = $incidentKey;
                if($this->Incidents->save($incident)) {
                    $incidentId = $incident->id;
                } else {
                    debug("Error Saving Incident");
                    $textArea = '';
                    break;
                }

                $textArea.=Router::url(['controller' => 'Questionnaires',
                                                'action' => 'view',
                                                $incident->id,
                                                '?' => ['key' => $incident->incident_key]], true) . '&#13;&#10;';
            }

            $textArea.= '</textarea>';
        }

        $this->set('textArea', $textArea);
    }

    public function clearIncidents($questionnaireId) {
        $this->loadModel('Incidents');
        $incidents = $this->Incidents->find()
                ->where(['Incidents.questionnaire_id' => $questionnaireId])
                ->where(['Incidents.locked' => false]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            foreach ($incidents as $key => $incident) {
                $incident->locked = true;
                if(!$this->Incidents->save($incident)) {
                    $this->Flash->error(__("couldn't clear all incidents"));
                    return;                
                }
            }            
        }
        $this->set('incidentNumber', $incidents->count());
        $this->set('questionnaireId', $questionnaireId);
    }

    public function loop($oldIncidentID) {
        $this->loadModel('Incidents');
        $oldIncident = $this->Incidents->get($oldIncidentID);
        if($this->request->query['key'] != $oldIncident->incident_key) {
            debug("wrong incident key");
            return;
        }

        $incident = $this->Incidents->newEntity();
        $incident->participant_id = $oldIncident->participant_id;
        $incident->created_from = "loop";
        $incident->questionnaire_id = $oldIncident->questionnaire_id;
        $incident->incident_key = $this->QuestionAuth->randomKey();
        if(!$this->Incidents->save($incident)) {
            debug("Error Saving Incident (loopQuestionnaire)");
            return;
        } else {
            $oldIncident->locked = true;
            $oldIncident->redirect_incident = $incident->id;
            $oldIncident->redirect_key = $incident->incident_key;
            if(!$this->Incidents->save($oldIncident)) {
                debug("Error Saving oldIncident (loopQuestionnaire)");
                return;
            }
            return $this->redirect([
                'controller' => 'Questionnaires', 
                'action' => 'view', 
                '?' => [
                    'key' => $incident->incident_key
                ],
                $incident->id]);
        }
    }

    public function viewQuestions($questionnaireId) {
        #debug($includeNonLocked);
        ini_set('memory_limit', '-1'); 
        $questionnaire = $this->Questionnaires->get($questionnaireId, [
            'contain' => [
                'Questions',
                'Questions.FreeTextFields',
                'Questions.NumberFields',
                'Questions.DateFields',
                'Questions.MultipleChoiceFields',
                'Questions.MultipleChoiceFields.MultipleChoiceOptions',
                'Questions.MapFields'
            ]
        ]);
        
        $this->set(compact('questionnaireId', 'questionnaire'));

        $this->response->type('Content-Type: text/plain');
        $this->viewBuilder()->layout('csv');
        $this->response->download($questionnaireId . 'questions.txt');

    }
}

