<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DateAnswers Controller
 *
 * @property \App\Model\Table\DateAnswersTable $DateAnswers
 */
class DateAnswersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Incidents', 'DateFields']
        ];
        $this->set('dateAnswers', $this->paginate($this->DateAnswers));
        $this->set('_serialize', ['dateAnswers']);
    }

    /**
     * View method
     *
     * @param string|null $id Date Answer id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dateAnswer = $this->DateAnswers->get($id, [
            'contain' => ['Incidents', 'DateFields']
        ]);
        $this->set('dateAnswer', $dateAnswer);
        $this->set('_serialize', ['dateAnswer']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dateAnswer = $this->DateAnswers->newEntity();
        if ($this->request->is('post')) {
            $dateAnswer = $this->DateAnswers->patchEntity($dateAnswer, $this->request->data);
            if ($this->DateAnswers->save($dateAnswer)) {
                $this->Flash->success(__('The date answer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The date answer could not be saved. Please, try again.'));
            }
        }
        $incidents = $this->DateAnswers->Incidents->find('list', ['limit' => 200]);
        $dateFields = $this->DateAnswers->DateFields->find('list', ['limit' => 200]);
        $this->set(compact('dateAnswer', 'incidents', 'dateFields'));
        $this->set('_serialize', ['dateAnswer']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Date Answer id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dateAnswer = $this->DateAnswers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dateAnswer = $this->DateAnswers->patchEntity($dateAnswer, $this->request->data);
            if ($this->DateAnswers->save($dateAnswer)) {
                $this->Flash->success(__('The date answer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The date answer could not be saved. Please, try again.'));
            }
        }
        $incidents = $this->DateAnswers->Incidents->find('list', ['limit' => 200]);
        $dateFields = $this->DateAnswers->DateFields->find('list', ['limit' => 200]);
        $this->set(compact('dateAnswer', 'incidents', 'dateFields'));
        $this->set('_serialize', ['dateAnswer']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Date Answer id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dateAnswer = $this->DateAnswers->get($id);
        if ($this->DateAnswers->delete($dateAnswer)) {
            $this->Flash->success(__('The date answer has been deleted.'));
        } else {
            $this->Flash->error(__('The date answer could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
