<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MultipleChoiceFields Controller
 *
 * @property \App\Model\Table\MultipleChoiceFieldsTable $MultipleChoiceFields
 */
class MultipleChoiceFieldsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('QuestionFields');
    }

    /**
     * Index method
     *
     * @return void
     */
    /*public function index()
    {
        $this->paginate = [
            'contain' => ['Questions']
        ];
        $this->set('multipleChoiceFields', $this->paginate($this->MultipleChoiceFields));
        $this->set('_serialize', ['multipleChoiceFields']);
    }*/

    /**
     * View method
     *
     * @param string|null $id Multiple Choice Field id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    /*public function view($id = null)
    {
        $multipleChoiceField = $this->MultipleChoiceFields->get($id, [
            'contain' => ['Questions', 'MultipleChoiceOptions']
        ]);
        $this->set('multipleChoiceField', $multipleChoiceField);
        $this->set('_serialize', ['multipleChoiceField']);
    }*/

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($question_id = null)
    {
        $this->QuestionFields->add($question_id, $this->MultipleChoiceFields, 'MultipleChoiceFields', 'edit', 'newId');
    }

    /**
     * Edit method
     *
     * @param string|null $id Multiple Choice Field id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->QuestionFields->edit($id, $this->MultipleChoiceFields);
        $this->loadModel('MultipleChoiceOptions');
        $options = $this->MultipleChoiceOptions->find()->where(['multiple_choice_field_id' => $id])->order(['priority' => 'ASC'])->all();
        $this->set('options', $options);
    }

    /**
     * Delete method
     *
     * @param string|null $id Multiple Choice Field id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->QuestionFields->delete($id, $this->MultipleChoiceFields);
    }

    public function sortOptions()
    {
        $this->request->allowMethod(['patch', 'post', 'put']);
        $order = $this->request->data['optionOrder'];
        if(!is_array($order) || count($order) < 1) {
            return;
        }
        $field_id = explode(':', $order[0])[0];
        foreach($order as $i => $data) {
            $option_id = explode(':', $data)[1];
            $this->loadModel('MultipleChoiceOptions');
            $option = $this->MultipleChoiceOptions->get($option_id, ['contain' => []]);
            $option->priority = $i;
            $this->MultipleChoiceOptions->save($option);
        }
    }
}


