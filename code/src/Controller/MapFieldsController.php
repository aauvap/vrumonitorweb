<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MapFields Controller
 *
 * @property \App\Model\Table\MapFieldsTable $MapFields
 */
class MapFieldsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('QuestionFields');
    }

    /**
     * Index method
     *
     * @return void
     */
    /*public function index()
    {
        $this->paginate = [
            'contain' => ['Questions']
        ];
        $this->set('mapFields', $this->paginate($this->MapFields));
        $this->set('_serialize', ['mapFields']);
    }*/

    /**
     * View method
     *
     * @param string|null $id Map Field id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    /*public function view($id = null)
    {
        $mapField = $this->MapFields->get($id, [
            'contain' => ['Questions']
        ]);
        $this->set('mapField', $mapField);
        $this->set('_serialize', ['mapField']);
    }*/

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($question_id = null)
    {
        $this->QuestionFields->add($question_id, $this->MapFields);
    }

    /**
     * Edit method
     *
     * @param string|null $id Map Field id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->QuestionFields->edit($id, $this->MapFields);
    }

    /**
     * Delete method
     *
     * @param string|null $id Map Field id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->QuestionFields->delete($id, $this->MapFields);
    }
}
