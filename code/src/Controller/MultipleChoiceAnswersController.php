<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MultipleChoiceAnswers Controller
 *
 * @property \App\Model\Table\MultipleChoiceAnswersTable $MultipleChoiceAnswers
 */
class MultipleChoiceAnswersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Incidents', 'MultipleChoiceOptions']
        ];
        $this->set('multipleChoiceAnswers', $this->paginate($this->MultipleChoiceAnswers));
        $this->set('_serialize', ['multipleChoiceAnswers']);
    }

    /**
     * View method
     *
     * @param string|null $id Multiple Choice Answer id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $multipleChoiceAnswer = $this->MultipleChoiceAnswers->get($id, [
            'contain' => ['Incidents', 'MultipleChoiceOptions']
        ]);
        $this->set('multipleChoiceAnswer', $multipleChoiceAnswer);
        $this->set('_serialize', ['multipleChoiceAnswer']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $multipleChoiceAnswer = $this->MultipleChoiceAnswers->newEntity();
        if ($this->request->is('post')) {
            $multipleChoiceAnswer = $this->MultipleChoiceAnswers->patchEntity($multipleChoiceAnswer, $this->request->data);
            if ($this->MultipleChoiceAnswers->save($multipleChoiceAnswer)) {
                $this->Flash->success(__('The multiple choice answer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The multiple choice answer could not be saved. Please, try again.'));
            }
        }
        $incidents = $this->MultipleChoiceAnswers->Incidents->find('list', ['limit' => 200]);
        $multipleChoiceOptions = $this->MultipleChoiceAnswers->MultipleChoiceOptions->find('list', ['limit' => 200]);
        $this->set(compact('multipleChoiceAnswer', 'incidents', 'multipleChoiceOptions'));
        $this->set('_serialize', ['multipleChoiceAnswer']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Multiple Choice Answer id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $multipleChoiceAnswer = $this->MultipleChoiceAnswers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $multipleChoiceAnswer = $this->MultipleChoiceAnswers->patchEntity($multipleChoiceAnswer, $this->request->data);
            if ($this->MultipleChoiceAnswers->save($multipleChoiceAnswer)) {
                $this->Flash->success(__('The multiple choice answer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The multiple choice answer could not be saved. Please, try again.'));
            }
        }
        $incidents = $this->MultipleChoiceAnswers->Incidents->find('list', ['limit' => 200]);
        $multipleChoiceOptions = $this->MultipleChoiceAnswers->MultipleChoiceOptions->find('list', ['limit' => 200]);
        $this->set(compact('multipleChoiceAnswer', 'incidents', 'multipleChoiceOptions'));
        $this->set('_serialize', ['multipleChoiceAnswer']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Multiple Choice Answer id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $multipleChoiceAnswer = $this->MultipleChoiceAnswers->get($id);
        if ($this->MultipleChoiceAnswers->delete($multipleChoiceAnswer)) {
            $this->Flash->success(__('The multiple choice answer has been deleted.'));
        } else {
            $this->Flash->error(__('The multiple choice answer could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
