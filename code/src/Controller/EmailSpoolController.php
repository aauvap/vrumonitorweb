<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * EmailSpool Controller
 *
 * @property \App\Model\Table\EmailSpoolTable $EmailSpool
 */
class EmailSpoolController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $queueLength = $this->EmailSpool
                            ->find()
                            ->where(['sent' => false])
                            ->where(function ($exp, $q) {
                                return $exp->isNull('error');
                            })
                            ->count();
        $this->set('emailSpool', $this->paginate($this->EmailSpool));
        $this->set('queueLength', $queueLength);
        $this->set('_serialize', ['emailSpool']);
    }

    /**
     * View method
     *
     * @param string|null $id Email Spool id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $emailSpool = $this->EmailSpool->get($id, [
            'contain' => []
        ]);
        $this->set('emailSpool', $emailSpool);
        $this->set('_serialize', ['emailSpool']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    /*public function add()
    {
        $emailSpool = $this->EmailSpool->newEntity();
        if ($this->request->is('post')) {
            $emailSpool = $this->EmailSpool->patchEntity($emailSpool, $this->request->data);
            if ($this->EmailSpool->save($emailSpool)) {
                $this->Flash->success(__('The email spool has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The email spool could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('emailSpool'));
        $this->set('_serialize', ['emailSpool']);
    }*/

    /**
     * Edit method
     *
     * @param string|null $id Email Spool id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $emailSpool = $this->EmailSpool->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $emailSpool = $this->EmailSpool->patchEntity($emailSpool, $this->request->data);
            if(strlen($emailSpool->error) == 0) {
                $emailSpool->error = null;
            }
            if ($this->EmailSpool->save($emailSpool)) {
                $this->Flash->success(__('The email has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The email could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('emailSpool'));
        $this->set('_serialize', ['emailSpool']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Email Spool id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $emailSpool = $this->EmailSpool->get($id);
        if ($this->EmailSpool->delete($emailSpool)) {
            $this->Flash->success(__('The email has been deleted.'));
        } else {
            $this->Flash->error(__('The email could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function sendMany() {
        if($this->request->is('post')) {
            $this->loadComponent('EmailSpooler');
            $this->EmailSpooler->activate($this->request->data['num_emails']);
        }
        
        $queueLength = $this->EmailSpool
                            ->find()
                            ->where(['sent' => false])
                            ->where(['valid' => true])
                            ->where(function ($exp, $q) {
                                return $exp->isNull('error');
                            })
                            ->count();
        $this->set(compact('queueLength'));
    }
}
