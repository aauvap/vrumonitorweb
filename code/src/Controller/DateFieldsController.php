<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DateFields Controller
 *
 * @property \App\Model\Table\DateFieldsTable $DateFields
 */
class DateFieldsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('QuestionFields');
    }

    /**
     * Index method
     *
     * @return void
     */
    /*public function index()
    {
        $this->paginate = [
            'contain' => ['Questions']
        ];
        $this->set('dateFields', $this->paginate($this->DateFields));
        $this->set('_serialize', ['dateFields']);
    }*/

    /**
     * View method
     *
     * @param string|null $id Date Field id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    /*public function view($id = null)
    {
        $dateField = $this->DateFields->get($id, [
            'contain' => ['Questions']
        ]);
        $this->set('dateField', $dateField);
        $this->set('_serialize', ['dateField']);
    }*/

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($question_id = null)
    {
        $this->QuestionFields->add($question_id, $this->DateFields);
    }

    /**
     * Edit method
     *
     * @param string|null $id Date Field id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->QuestionFields->edit($id, $this->DateFields);
    }

    /**
     * Delete method
     *
     * @param string|null $id Date Field id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->QuestionFields->delete($id, $this->DateFields);
    }
}
