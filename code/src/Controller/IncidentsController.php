<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Incidents Controller
 *
 * @property \App\Model\Table\IncidentsTable $Incidents
 */
class IncidentsController extends AppController
{    
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('QuestionAuth');
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Participants']
        ];
        $this->set('incidents', $this->paginate($this->Incidents));
        $this->set('_serialize', ['incidents']);
    }

    /**
     * View method
     *
     * @param string|null $id Incident id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $incident = $this->Incidents->get($id, [
            'contain' => ['Participants', 'DateAnswers', 'FreeTextAnswers', 'MapAnswers', 'MultipleChoiceAnswers', 'NumberAnswers', 'SensorDumps']
        ]);
        $this->set('incident', $incident);
        $this->set('_serialize', ['incident']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($participantId = null)
    {
        $participantKey = null;
        if($this->request->query('key') !== null) {
            $participantKey = $this->request->query('key');
        } elseif($this->request->session()->check('Participant.key')) {
            $participantKey = $this->request->session()->read('Participant.key');
        }
        
        if(is_null($participantId) or is_null($participantKey)) {
            $this->Flash->error(__('The incident could not be saved. Insufficent user credentials. Please, try again.'));
            return $this->redirect(['action' => 'index']);            
        }
        
        $incident = $this->Incidents->newEntity();
        $incident->participant_id = $participantId;
        $incident->created_from = "add";
        if ($this->request->is('post')) {
            $this->loadModel("Participants");
            $participant = $this->Participants->get($participantId);
            if($participant->participant_key != $participantKey) {
                $this->Flash->error(__('The incident could not be saved. Insufficent user credentials. Please, try again.'));
                return $this->redirect(['action' => 'index']);            
            }
            $incident->questionnaire_id = $participant->questionnaire_id;
            $incident = $this->Incidents->patchEntity($incident, $this->request->data);
            $incident->incident_key = $this->QuestionAuth->randomKey();
            if ($this->Incidents->save($incident)) {
                $this->Flash->success(__('The incident has been saved.'));
                return $this->redirect(['action' => 'view', $incident->id]);
            } else {
                $this->Flash->error(__('The incident could not be saved. Please, try again.'));
            }
        }
        //$participants = $this->Incidents->Participants->find('list', ['limit' => 200]);
        $this->set(compact('incident', 'participantId', 'participantKey'));
        $this->set('_serialize', ['incident']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Incident id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    /*public function edit($id = null)
    {
        $incident = $this->Incidents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $incident = $this->Incidents->patchEntity($incident, $this->request->data);
            if ($this->Incidents->save($incident)) {
                $this->Flash->success(__('The incident has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The incident could not be saved. Please, try again.'));
            }
        }
        $participants = $this->Incidents->Participants->find('list', ['limit' => 200]);
        $this->set(compact('incident', 'participants'));
        $this->set('_serialize', ['incident']);
    }*/

    /**
     * Delete method
     *
     * @param string|null $id Incident id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $incident = $this->Incidents->get($id);
        if ($this->Incidents->delete($incident)) {
            $this->Flash->success(__('The incident has been deleted.'));
        } else {
            $this->Flash->error(__('The incident could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
