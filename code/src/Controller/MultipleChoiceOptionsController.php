<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * MultipleChoiceOptions Controller
 *
 * @property \App\Model\Table\MultipleChoiceOptionsTable $MultipleChoiceOptions
 */
class MultipleChoiceOptionsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('QuestionFields');
    }

    /**
     * Index method
     *
     * @return void
     */
    /*public function index()
    {
        $this->paginate = [
            'contain' => ['MultipleChoiceFields']
        ];
        $this->set('multipleChoiceOptions', $this->paginate($this->MultipleChoiceOptions));
        $this->set('_serialize', ['multipleChoiceOptions']);
    }*/

    /**
     * View method
     *
     * @param string|null $id Multiple Choice Option id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $multipleChoiceOption = $this->MultipleChoiceOptions->get($id, [
            'contain' => ['MultipleChoiceFields']
        ]);
        $this->set('multipleChoiceOption', $multipleChoiceOption);
        $this->set('_serialize', ['multipleChoiceOption']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($field_id = null)
    {
        $option = $this->MultipleChoiceOptions->newEntity();
        //debug($option);
        
        if ($this->request->is('post')) {
            $option = $this->MultipleChoiceOptions->patchEntity($option, $this->request->data);
            $this->loadModel('MultipleChoiceFields');

            //debug("tru");
            // Override the GET-transmitted id if one has been transmitted via POST:
            if(isset($this->request->data['field_id'])) {
                $field_id = $this->request->data['field_id'];
            }

            if(is_null($field_id)) {
                $this->Flash->error(__('You cannot add an option which is not associated with a field.'));
            } elseif(!$this->MultipleChoiceFields->exists(['id' => $field_id])) {
                $this->Flash->error(__('A field with id {0} does not exist and thus the option cannot be added. Please select a valid field.', [$field_id]));
            } else {
                $option->multiple_choice_field_id = $field_id;
                
                // Get the next priority value
                $priorityQuery = $this->MultipleChoiceOptions->find();
                //$option->priority
                $option->priority = $priorityQuery->where(['multiple_choice_field_id' => $field_id])
                                                  ->select(['max' => $priorityQuery->func()->max('priority')])
                                                  ->first()['max']+1;
//                debug($priority);
                
                if ($this->MultipleChoiceOptions->save($option)) {
                    $this->Flash->success(__('The option has been saved.'));
                    return $this->redirect([
                        'controller' => 'MultipleChoiceFields',
                        'action' => 'edit',
                        $field_id
                    ]);
                } else {
                    $this->Flash->error(__('The option could not be saved. Please, try again.'));
                }
            }
        }
        $multipleChoiceFields = $this->MultipleChoiceOptions->MultipleChoiceFields->find('list', ['limit' => 200]);
        if(!is_null($field_id)) {
            $this->set('multiple_choice_field', $this->MultipleChoiceOptions->MultipleChoiceFields->get($field_id));
        }
        $this->set(compact('option', 'multipleChoiceFields', 'field_id'));
        $this->set('_serialize', ['option']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Multiple Choice Option id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $option = $this->MultipleChoiceOptions->get($id, [
            'contain' => ['MultipleChoiceFields']
        ]);
        $field_id = $option->multiple_choice_field_id;
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            #debug($this->request);
            #debug($this->request->data['picture']['name']);
            #debug($option->picture_dir);
            if($this->request->data['keepPicture'] == 'false' || (!empty($this->request->data['picture']['name']) && !is_null($option->picture_dir))) {
                $dir = new Folder(WWW_ROOT.'files'.DS.'multiplechoiceoptions'.DS.'picture'.DS.$option->picture_dir);
                #debug($dir);
                $files = $dir->find('.*'.$option->picture.'.*');
                foreach ($files as $filename) {
                    $file = new File($dir->pwd().DS.$filename);
                    $file->delete();
                    $file->close();
                    
                }
                if(count($dir->find('.*')) == 0) {
                    $dir->delete(); // Delete the directory if empty.
                }
                $option->picture = '';
                $option->picture_dir = '';
            }

            $option = $this->MultipleChoiceOptions->patchEntity($option, $this->request->data);
            
            if ($this->MultipleChoiceOptions->save($option)) {
                //$this->Flash->success(__('The field has been saved.'));
                return $this->redirect([
                    'controller' => 'MultipleChoiceFields',
                    'action' => 'edit',
                    $field_id
                ]);
            } else {
                $this->Flash->error(__('The field could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('option', 'field_id'));
        $this->set('_serialize', ['field']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Multiple Choice Option id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $option = $this->MultipleChoiceOptions->get($id, [
            'contain' => []
        ]);
        $field_id = $option->multiple_choice_field_id;
        $this->QuestionFields->delete($id, $this->MultipleChoiceOptions, 'MultipleChoiceFields', $field_id);
    }
}
