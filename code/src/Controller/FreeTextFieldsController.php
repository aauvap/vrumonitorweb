<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FreeTextFields Controller
 *
 * @property \App\Model\Table\FreeTextFieldsTable $FreeTextFields
 */
class FreeTextFieldsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('QuestionFields');
    }

    /**
     * Index method
     *
     * @return void
     */
    /*public function index()
    {
        $this->paginate = [
            'contain' => ['Questions']
        ];
        $this->set('freeTextFields', $this->paginate($this->FreeTextFields));
        $this->set('_serialize', ['freeTextFields']);
    }*/

    /**
     * View method
     *
     * @param string|null $id Free Text Field id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    /*public function view($id = null)
    {
        $freeTextField = $this->FreeTextFields->get($id, [
            'contain' => ['Questions']
        ]);
        $this->set('freeTextField', $freeTextField);
        $this->set('_serialize', ['freeTextField']);
    }*/

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($question_id = null)
    {
        $freeTextFields = $this->FreeTextFields
                               ->find('list', [
                                   'keyField' => 'id',
                                   'valueField' => 'caption'
                               ])
                               ->where(['question_id' => $question_id])
                               ->toArray();
        //$freeTextFields = array_merge(['' => '[None]'], $freeTextFields);
        $this->QuestionFields->add($question_id, $this->FreeTextFields);
        $this->set(compact('freeTextFields'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Free Text Field id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $field = $this->FreeTextFields->get($id);
        $question_id = $field->question_id;
        $freeTextFields = $this->FreeTextFields
                               ->find('list', [
                                   'keyField' => 'id',
                                   'valueField' => 'caption'
                               ])
                               ->where(['question_id' => $question_id,
                                        'priority <' => $field->priority])
                               ->toArray();
        $this->QuestionFields->edit($id, $this->FreeTextFields);
        $this->set(compact('freeTextFields'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Free Text Field id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->QuestionFields->delete($id, $this->FreeTextFields);
    }
}
