<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AppSettings Controller
 *
 * @property \App\Model\Table\AppSettingsTable $AppSettings
 */
class AppSettingsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow('index');
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $appSettings = $this->AppSettings->find()->first()['settings'];
        $this->set(compact('appSettings'));
        $this->set('_serialize', ['appSettings']);
    }

    /**
     * View method
     *
     * @param string|null $id App Setting id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    /*public function view($id = null)
    {
        $appSetting = $this->AppSettings->find()->first();
        $this->set('_serialize', ['appSetting']);
    }*/

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    /*public function add()
    {
        $appSetting = $this->AppSettings->newEntity();
        if ($this->request->is('post')) {
            $appSetting = $this->AppSettings->patchEntity($appSetting, $this->request->data);
            if ($this->AppSettings->save($appSetting)) {
                $this->Flash->success(__('The app setting has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The app setting could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('appSetting'));
        $this->set('_serialize', ['appSetting']);
    }*/

    /**
     * Edit method
     *
     * @param string|null $id App Setting id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit()
    {
        $appSetting = $this->AppSettings->find()->first();
        if($appSetting === null) {
            $appSetting = $this->AppSettings->newEntity();
            $this->AppSettings->save($appSetting);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $appSetting = $this->AppSettings->patchEntity($appSetting, $this->request->data);
            if ($this->AppSettings->save($appSetting)) {
                $this->Flash->success(__('The app setting has been saved.'));
            } else {
                $this->Flash->error(__('The app setting could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('appSetting'));
        $this->set('_serialize', ['appSetting']);
    }

    /**
     * Delete method
     *
     * @param string|null $id App Setting id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    /*public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $appSetting = $this->AppSettings->get($id);
        if ($this->AppSettings->delete($appSetting)) {
            $this->Flash->success(__('The app setting has been deleted.'));
        } else {
            $this->Flash->error(__('The app setting could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }*/
}
