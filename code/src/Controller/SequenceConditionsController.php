<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SequenceConditions Controller
 *
 * @property \App\Model\Table\SequenceConditionsTable $SequenceConditions
 */
class SequenceConditionsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['QuestionSequences', 'MultipleChoiceOptions']
        ];
        $this->set('sequenceConditions', $this->paginate($this->SequenceConditions));
        $this->set('_serialize', ['sequenceConditions']);
    }

    /**
     * View method
     *
     * @param string|null $id Sequence Condition id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sequenceCondition = $this->SequenceConditions->get($id, [
            'contain' => ['QuestionSequences', 'MultipleChoiceOptions']
        ]);
        $this->set('sequenceCondition', $sequenceCondition);
        $this->set('_serialize', ['sequenceCondition']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sequenceCondition = $this->SequenceConditions->newEntity();
        if ($this->request->is('post')) {
            $sequenceCondition = $this->SequenceConditions->patchEntity($sequenceCondition, $this->request->data);
            if($this->request->data['conditionType'] == 'or') {
                $sequenceCondition->or_condition = true;
            }
            $this->SequenceConditions->updateAll(['or_condition' => $sequenceCondition->or_condition], ['question_sequence_id' => $sequenceCondition->question_sequence_id]);
            if ($this->SequenceConditions->save($sequenceCondition)) {
                $this->Flash->success(__('The sequence condition has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sequence condition could not be saved. Please, try again.'));
            }
        }
        $questionSequences = $this->SequenceConditions->QuestionSequences->find('list', ['limit' => 200]);
        $multipleChoiceOptions = $this->SequenceConditions->MultipleChoiceOptions->find('list', ['limit' => 200]);
        $this->set(compact('sequenceCondition', 'questionSequences', 'multipleChoiceOptions'));
        $this->set('_serialize', ['sequenceCondition']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sequence
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sequenceCondition = $this->SequenceConditions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sequenceCondition = $this->SequenceConditions->patchEntity($sequenceCondition, $this->request->data);
            if ($this->SequenceConditions->save($sequenceCondition)) {
                $this->Flash->success(__('The sequence condition has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sequence condition could not be saved. Please, try again.'));
            }
        }
        $questionSequences = $this->SequenceConditions->QuestionSequences->find('list', ['limit' => 200]);
        $multipleChoiceOptions = $this->SequenceConditions->MultipleChoiceOptions->find('list', ['limit' => 200]);
        $this->set(compact('sequenceCondition', 'questionSequences', 'multipleChoiceOptions'));
        $this->set('_serialize', ['sequenceCondition']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sequence Condition id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sequenceCondition = $this->SequenceConditions->get($id, [
            'contain' => ["QuestionSequences"]
        ]);
        $question_id = $sequenceCondition->question_sequence->question_id;
        if ($this->SequenceConditions->delete($sequenceCondition)) {
            $this->Flash->success(__('The sequence condition has been deleted.'));
        } else {
            $this->Flash->error(__('The sequence condition could not be deleted. Please, try again.'));
        }
        return $this->redirect(['controller' => 'Questions', 'action' => 'edit', $question_id]);
    }
}
