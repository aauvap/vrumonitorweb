<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * NumberAnswers Controller
 *
 * @property \App\Model\Table\NumberAnswersTable $NumberAnswers
 */
class NumberAnswersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Incidents', 'NumberFields']
        ];
        $this->set('numberAnswers', $this->paginate($this->NumberAnswers));
        $this->set('_serialize', ['numberAnswers']);
    }

    /**
     * View method
     *
     * @param string|null $id Number Answer id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $numberAnswer = $this->NumberAnswers->get($id, [
            'contain' => ['Incidents', 'NumberFields']
        ]);
        $this->set('numberAnswer', $numberAnswer);
        $this->set('_serialize', ['numberAnswer']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $numberAnswer = $this->NumberAnswers->newEntity();
        if ($this->request->is('post')) {
            $numberAnswer = $this->NumberAnswers->patchEntity($numberAnswer, $this->request->data);
            if ($this->NumberAnswers->save($numberAnswer)) {
                $this->Flash->success(__('The number answer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The number answer could not be saved. Please, try again.'));
            }
        }
        $incidents = $this->NumberAnswers->Incidents->find('list', ['limit' => 200]);
        $numberFields = $this->NumberAnswers->NumberFields->find('list', ['limit' => 200]);
        $this->set(compact('numberAnswer', 'incidents', 'numberFields'));
        $this->set('_serialize', ['numberAnswer']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Number Answer id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $numberAnswer = $this->NumberAnswers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $numberAnswer = $this->NumberAnswers->patchEntity($numberAnswer, $this->request->data);
            if ($this->NumberAnswers->save($numberAnswer)) {
                $this->Flash->success(__('The number answer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The number answer could not be saved. Please, try again.'));
            }
        }
        $incidents = $this->NumberAnswers->Incidents->find('list', ['limit' => 200]);
        $numberFields = $this->NumberAnswers->NumberFields->find('list', ['limit' => 200]);
        $this->set(compact('numberAnswer', 'incidents', 'numberFields'));
        $this->set('_serialize', ['numberAnswer']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Number Answer id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $numberAnswer = $this->NumberAnswers->get($id);
        if ($this->NumberAnswers->delete($numberAnswer)) {
            $this->Flash->success(__('The number answer has been deleted.'));
        } else {
            $this->Flash->error(__('The number answer could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
