var map = L.map('map').setView([0, 0], 1);
var markers = new Array();

if(map_bounding_box != null) {
    southWest = new L.LatLng(parseFloat(map_bounding_box[1]), parseFloat(map_bounding_box[0])),
    northEast = new L.LatLng(parseFloat(map_bounding_box[3]), parseFloat(map_bounding_box[2])),
    map.fitBounds(new L.LatLngBounds(southWest, northEast));
}

/*L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'aauvap.cif9gpfpn001mtaknsa83p45o',
    accessToken: 'pk.eyJ1IjoiYWF1dmFwIiwiYSI6ImNpZjlncGZ3ZzAwMWp1Nmx5Zjc2MDAydTAifQ.0E8e6TjgnWBeWBy7CFJy8A'
}).addTo(map);*/

var ggl = new L.Google('ROADMAP');
var ggl2 = new L.Google('SATELLITE');
map.addLayer(ggl);
map.addControl(new L.Control.Layers( {'Google':ggl, 'Google Terrain':ggl2}, {}));

$(document).ready(function(){
    if($("input[name='mapSettings']").length) { // Edit field page
        if(!$('#mapsettings-defined').is(':checked')) {
            $("#map").hide();
        }
    
        $("#mapsettings-auto").change(function(){
            $("#map").hide("fast");
        });
        $("#mapsettings-defined").change(function(){
            $("#map").show("fast", function(){
                map.invalidateSize();
            });
        });
    }
    
    $('input[name=map_bounding_box]').closest('form').submit(function(){
        if($('#mapsettings-defined').is(':checked')) {
            $('input[name=map_bounding_box]').val(map.getBounds().toBBoxString());
        } else {
            $('input[name=map_bounding_box]').val('');
        }
    });


    if($("#answerForm").length) { // Save answer page
        var max_num_marks = $("input[name='max_num_marks']").val();
        if(!$.isNumeric(max_num_marks)) {
            max_num_marks = null;
        }
        console.log(max_num_marks);
        
        function editMarkers(e) {
            if(max_num_marks !== null && max_num_marks == 1 && markers.length == 1) {
                markers[0].setLatLng(e.latlng);
                return;
            }
            if(max_num_marks !== null && markers.length >= max_num_marks) {
                return;
            }
            markers.push(L.marker(e.latlng, {draggable: true}).addTo(map));
            markers[markers.length-1].on('click', function(e) {
                for (i = 0; i < markers.length; i++) {
                    if(markers[i]._leaflet_id == this._leaflet_id) {
                        break;
                    }
                }
                markers.splice(i, 1);
                map.removeLayer(this);
            });
        }
        map.on('click', editMarkers);

        $("#answerForm").submit(function(event) {
            for(i = 0; i < markers.length; i++) {
                $('<input type="hidden">').attr({
                    name: 'MapFields[0][locations][' + i + '][latitude]',
                    value: markers[i].getLatLng().lat,
                }).appendTo('form');
                $('<input type="hidden">').attr({
                    name: 'MapFields[0][locations][' + i + '][longitude]',
                    value: markers[i].getLatLng().lng,
                }).appendTo('form');
            }
        });
    }
});
