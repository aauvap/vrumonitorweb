function removePicture(confirmMessage) {
    if(!confirm(confirmMessage)) {
        return false;
    }
    $('input[name=keepPicture]').val("false");
    $('img#optionPicture').hide();
    $('a#removePicture').hide();
}
