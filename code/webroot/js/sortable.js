$(document).ready(function(){
    // Make sortable lists sortable:
    for(index = 0; index < sortableSettings.length; ++index) {
        (function() {
            var i = index; // Saving the i-value in another closure so it is in scope when this function is activated (fixing closure issue, see http://stackoverflow.com/a/13106365/53345).
            if(!("updateFunction" in sortableSettings[i])) {
                sortableSettings[i].updateFunction = 
                    function (event, ui) {
                        var data = $(this).sortable('serialize');
                        // POST to server using $.post or $.ajax
                        $.ajax({
                            data: data,
                            type: 'POST',
                            url: sortableSettings[i].url
                        });   
                    };
            }
            $(sortableSettings[i].uiElement).sortable({
                axis: 'y',
                helper : 'clone',
                update: sortableSettings[i].updateFunction
            });
        })(); // Direct function execution (fixing closure issue)
    }
});
