$(document).ready(function(){
    // Clear all other checkboxes when checking an exclusive one
    $("input.exclusive").change(function() {
        if($(this).is(':checked')) {
            $("input[name='" + $(this).attr("name") + "']").not($(this)).attr('checked', false);
        }
    });

    // Clear all exclusive checkboxes when checking anything else
    $("input[type='checkbox']").not(".exclusive").change(function() {
        if($(this).is(':checked')) {
            $("input[name='" + $(this).attr("name") + "'].exclusive").attr('checked', false);
        }
    });
});
