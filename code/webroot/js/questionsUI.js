$(document).ready(function(){
    $("#add_field_select").change(function(){
        var fieldName = $(this).val();
        if(fieldName.length > 0) {
            window.location = appBaseUrl + fieldName + "/add/" + question_id;
        }
    }); 

    $('.hasTooltip').each(function() { // Notice the .each() loop, discussed below
        $(this).qtip({
            content: {
                text: $(this).next('div') // Use the "div" element next to this for the content
            },
            style: 'qtip-light',
            position: {
                my: 'bottom left',
                at: 'top right'
            },
            show: {
                delay: 0
            },
            hide: {
                delay: 0
            }
        });
    });
    
    $("tr[id^='connectionOrder']").click(function(e) {
        if(!$(e.target).is("td")) {
            return;
        }

        console.log('click!');
        
        var isVisible = $(this).find("td.displayConnections").is(":visible");
        $(".editConnections").hide();
        $(".displayConnections").show();
        if(isVisible) {
            $(this).find(".displayConnections").hide();
            $(this).find(".editConnections").show();
        } else {
            $(this).find(".displayConnections").show();
            $(this).find(".editConnections").hide();
        }
    });

    // Bind the forms for new connection conditions to Ajax:
    $('div.addConnection form').ajaxForm(function() { 
        location.reload(); // Refresh page so a new connection tree is shown.
    });

    // Clear all other checkboxes when checking an exclusive one
    $("input").change(function() {
        
        console.log("Checked exclusive!");
        if($(this).is(':checked')) {
            console.log("Checked exclusive!");
        }
    });
});
