/*function ajaxFormSubmit(name) {
    $("form[name='" + name + "']").ajaxSubmit();
    location.reload();
}*/

$(document).ready(function(){
    $("svg#branchingTree").height($("table#questionsTable").height());
    $("svg#branchingTree").width(branchingTreeWidth);

    // Bind the form for new connection creation to Ajax:
    /*$('form#addQuestionSequence').ajaxForm(function() { 
        location.reload(); // Refresh page so a new connection tree is shown.
    });*/ 


    // Snap for interactive branching tree
    var s = Snap("#branchingTree");
    var currentLine = null;
    var currentEndNode = null;
    var svgPosition = $("svg#branchingTree").position();
    var qtipOptions = {
        style: 'qtip-light',
        position: {
            my: 'bottom left',
            at: 'center'
        },
        show: {
            when: 'click',
            ready: true,
            delay: 0
        },
        hide: {
            delay: 200
        },
        events: {
            hide: function () {
                $(this).qtip('destroy');
            }
        }
    };
    
    $("svg#branchingTree").on('click', 'circle.transparent', function() {
        console.log("Test");
        // Start drawing if branching is allowed and there is no line in progress:
        if(currentLine === null && $(this).attr("class").search("noBranching") == -1  && $(this).attr("class").search("endNode") == -1) {
            var startNode = $(this).attr('id').split("_");
            var startPoint = startNode[2].split("-");
            currentLine = {
                fromId: startNode[1],
                line: s.line(startPoint[0], startPoint[1], startPoint[0], startPoint[1])
            }
            currentLine.line.attr({
                class: "highlight-gray currentLine"
            });
            currentEndNode = {
                node: s.circle(parseInt(startPoint[0])+15, startPoint[1], 2),
                invisibleNode: s.circle(parseInt(startPoint[0])+15, startPoint[1], 8)
            }
            currentEndNode.node.attr({
                class: "node endNode",
                id: "node_end_" + (parseInt(startPoint[0])+15) + "-" + startPoint[1]
            });
            currentEndNode.invisibleNode.attr({
                class: "node transparent",
                id: "invisibleNode_end_" + (parseInt(startPoint[0])+15) + "-" + startPoint[1]
            });
        } else if(currentLine !== null) {
            // If a line is in progress, stop drawing and save the connection, if it is legal
            var toId = $(this).attr('id').split('_')[1];
            if(toId != 'end') {
                toId = parseInt(toId);
            }
            var lineId = 'path[id$="_' + currentLine.fromId + '-' + toId + '"]';
            var existingLine = s.select(lineId);
            var tooltip = qtipOptions;
            //if((toId == 'end' || (toId > currentLine.fromId && !isNaN(toId))) && existingLine === null) {

            if((toId == 'end' || (parseInt($(this).attr('cy')) > parseInt(currentLine.line.attr('y1')) && !isNaN(toId))) && existingLine === null) {
                // Save!!
                $('form#addQuestionSequence > input[name="question_id"]').val(currentLine.fromId);
                toId = (toId == 'end' ? '' : toId);
                $('form#addQuestionSequence > input[name="to_question_id"]').val(toId);
                //$("form#addQuestionSequence").submit();
                $("form#addQuestionSequence").ajaxSubmit();
                location.reload(); // Refresh page so a new connection tree is shown.
            } else if(existingLine !== null) {
                tooltip.content = {text: 'This connections exists already.'};
                $(this).qtip(tooltip);
                currentLine.line.remove();
            } else {
                tooltip.content = {text: 'Questions cannot be linked to earlier questions.'};
                $(this).qtip(tooltip);
                currentLine.line.remove();
            }
            currentLine = null;
            currentEndNode.node.remove();
            currentEndNode.invisibleNode.remove();
            currentEndNode = null;
        } else if($(this).attr("class").search("noBranching") != -1){
            // Branching not allowed
            var tooltip = qtipOptions;
            tooltip.content = {text: 'This question does not have a multiple choice field and thus cannot have more than one connection.'};
            $(this).qtip(tooltip);

        }
    });

    $("svg#branchingTree").on('mouseenter', 'circle.transparent', function() {
        var nodeId = "#node_" + $(this).attr('id').substring($(this).attr('id').indexOf('_')+1);
        s.select(nodeId).addClass("hover");

        if(currentLine !== null) {
            var toId = $(this).attr('id').split('_')[1];
            if(toId != 'end') {
                toId = parseInt(toId);
            }
            var lineId = 'path[id$="_' + currentLine.fromId + '-' + toId + '"]';
            
            //if(toId != 'end' && (toId <= currentLine.fromId || isNaN(toId))) {
            //console.log($(this).attr('cy'));
            //console.log(currentLine.line.attr('y1'));
            if(toId != 'end' && (parseInt($(this).attr('cy')) <= parseInt(currentLine.line.attr('y1')) || isNaN(toId))) {
                currentLine.line.addClass("transparent");
            }

            var end = $(this).attr('id').split("_")[2].split("-");
            currentLine.line.attr({
                x2: end[0],
                y2: end[1]
            });
            
            var existingLine = s.select(lineId);
            if(existingLine != null) {
                existingLine.addClass("highlight-red");
                currentLine.line.addClass("transparent")
            } else {
                currentLine.line.removeClass("highlight-gray");
                currentLine.line.addClass("highlight-green");
            }
        }
    });
    $("svg#branchingTree").on('mouseleave', 'circle.transparent', function() {
        var nodeId = "#node_" + $(this).attr('id').substring($(this).attr('id').indexOf('_')+1);
        s.select(nodeId).removeClass("hover");

        if(currentLine !== null) {
            /*var toId = $(this).attr('id').split('_')[1];
            var lineId = 'path[id$="_' + currentLine.fromId + '-' + toId + '"]';
            var existingLine = s.select(lineId);
            if(existingLine !== null) {
                existingLine.removeClass("highlight-red");
            }*/
            currentLine.line.removeClass("highlight-green");
            currentLine.line.removeClass("transparent");
            currentLine.line.addClass("highlight-gray");
        }

        if(s.select("path.highlight-red") !== null) {
            s.select("path.highlight-red").removeClass("highlight-red");
        }
    });

    
    $("svg#branchingTree").mousemove(function(e) {
        if(currentLine !== null && !currentLine.line.hasClass("highlight-green")) {
            var endPoint = [e.pageX-svgPosition.left-1, e.pageY-svgPosition.top-1];
            currentLine.line.attr({
                x2: endPoint[0],
                y2: endPoint[1]
            });
        }
	});

    /***********************
      Events on paths
    ************************/
    $("svg#branchingTree > path.transparent").mouseenter(function() {
        var edgeId = "#edge_" + $(this).attr('id').substring($(this).attr('id').indexOf('_')+1);
        s.select(edgeId).addClass("highlight-blue");
    }).mouseleave(function() {
        var edgeId = "#edge_" + $(this).attr('id').substring($(this).attr('id').indexOf('_')+1);
        s.select(edgeId).removeClass("highlight-blue");
    });
    
    $("svg#branchingTree > path.transparent").click(function(e) {
        var edgeId =  $(this).attr('id').split("_")[1];
        console.log(edgeId);
        var tooltip = qtipOptions;
        tooltip.position = {
            my: 'bottom left',
            target: [e.pageX, e.pageY],
        }
        tooltip.hide = {
             event: 'unfocus'
        }
        text = connectionConditionsTextStart;
        text += connectionConditions[edgeId].join('');
        text += connectionPriority[edgeId];
        text += connectionEditLink[edgeId];
        text += connectionConditionsTextEnd.split('%edgeId%').join(edgeId);
        tooltip.content = {
            title: connectionConditionsTitle,
            text: text
        };
        $(this).qtip(tooltip);
    });

    // Bind edge-delete forms to Ajax:
    /*window.debug_elem = $('form[name^="delete_edge_"]');
    console.log(window.debug_elem);
    $('form[name^="delete_edge_"]').ajaxForm(function() { 
        location.reload(); // Refresh page so a new connection tree is shown.
    }); */


    var KEYCODE_ESC = 27;
    $(document).keyup(function(e) {
        if(e.keyCode == KEYCODE_ESC) {
            currentLine.line.remove();
            currentLine = null;
            currentEndNode.node.remove();
            currentEndNode.invisibleNode.remove();
            currentEndNode = null;
        }
    });
});
