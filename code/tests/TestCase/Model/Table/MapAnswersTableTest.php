<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MapAnswersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MapAnswersTable Test Case
 */
class MapAnswersTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.map_answers',
        'app.incidents',
        'app.participants',
        'app.date_answers',
        'app.date_fields',
        'app.questions',
        'app.questionnaires',
        'app.free_text_answers',
        'app.free_text_fields',
        'app.multiple_choice_answers',
        'app.multiple_choice_options',
        'app.number_answers',
        'app.number_fields',
        'app.sensor_dumps',
        'app.map_fields'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MapAnswers') ? [] : ['className' => 'App\Model\Table\MapAnswersTable'];
        $this->MapAnswers = TableRegistry::get('MapAnswers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MapAnswers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
