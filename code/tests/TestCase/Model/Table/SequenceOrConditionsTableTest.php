<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SequenceOrConditionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SequenceOrConditionsTable Test Case
 */
class SequenceOrConditionsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sequence_or_conditions',
        'app.question_sequences',
        'app.questions',
        'app.questionnaires',
        'app.to_questions',
        'app.multiple_choice_options'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SequenceOrConditions') ? [] : ['className' => 'App\Model\Table\SequenceOrConditionsTable'];
        $this->SequenceOrConditions = TableRegistry::get('SequenceOrConditions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SequenceOrConditions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
