<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DateAnswersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DateAnswersTable Test Case
 */
class DateAnswersTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.date_answers',
        'app.incidents',
        'app.participants',
        'app.free_text_answers',
        'app.free_text_fields',
        'app.questions',
        'app.questionnaires',
        'app.map_answers',
        'app.map_fields',
        'app.multiple_choice_answers',
        'app.multiple_choice_options',
        'app.number_answers',
        'app.number_fields',
        'app.sensor_dumps',
        'app.date_fields'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DateAnswers') ? [] : ['className' => 'App\Model\Table\DateAnswersTable'];
        $this->DateAnswers = TableRegistry::get('DateAnswers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DateAnswers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
