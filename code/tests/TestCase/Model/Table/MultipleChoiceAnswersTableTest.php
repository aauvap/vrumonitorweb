<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MultipleChoiceAnswersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MultipleChoiceAnswersTable Test Case
 */
class MultipleChoiceAnswersTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.multiple_choice_answers',
        'app.incidents',
        'app.participants',
        'app.date_answers',
        'app.date_fields',
        'app.questions',
        'app.questionnaires',
        'app.free_text_answers',
        'app.free_text_fields',
        'app.map_answers',
        'app.map_fields',
        'app.number_answers',
        'app.number_fields',
        'app.sensor_dumps',
        'app.multiple_choice_options'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MultipleChoiceAnswers') ? [] : ['className' => 'App\Model\Table\MultipleChoiceAnswersTable'];
        $this->MultipleChoiceAnswers = TableRegistry::get('MultipleChoiceAnswers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MultipleChoiceAnswers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
