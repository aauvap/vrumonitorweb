<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SequenceAndConditionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SequenceAndConditionsTable Test Case
 */
class SequenceAndConditionsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sequence_and_conditions',
        'app.question_sequences',
        'app.questions',
        'app.questionnaires',
        'app.to_questions',
        'app.multiple_choice_options'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SequenceAndConditions') ? [] : ['className' => 'App\Model\Table\SequenceAndConditionsTable'];
        $this->SequenceAndConditions = TableRegistry::get('SequenceAndConditions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SequenceAndConditions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
