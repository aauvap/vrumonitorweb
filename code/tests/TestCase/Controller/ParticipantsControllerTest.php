<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ParticipantsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ParticipantsController Test Case
 */
class ParticipantsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.participants',
        'app.incidents',
        'app.date_answers',
        'app.date_fields',
        'app.questions',
        'app.questionnaires',
        'app.free_text_answers',
        'app.free_text_fields',
        'app.map_answers',
        'app.map_fields',
        'app.multiple_choice_answers',
        'app.multiple_choice_options',
        'app.number_answers',
        'app.number_fields',
        'app.sensor_dumps'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
