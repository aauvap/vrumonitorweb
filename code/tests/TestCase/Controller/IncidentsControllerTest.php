<?php
namespace App\Test\TestCase\Controller;

use App\Controller\IncidentsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\IncidentsController Test Case
 */
class IncidentsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.incidents',
        'app.participants',
        'app.date_answers',
        'app.date_fields',
        'app.questions',
        'app.questionnaires',
        'app.map_fields',
        'app.free_text_fields',
        'app.multiple_choice_fields',
        'app.multiple_choice_options',
        'app.number_fields',
        'app.question_sequences',
        'app.sequence_conditions',
        'app.free_text_answers',
        'app.map_answers',
        'app.multiple_choice_answers',
        'app.number_answers',
        'app.sensor_dumps'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
