<?php
namespace App\Test\TestCase\Controller;

use App\Controller\MultipleChoiceFieldsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\MultipleChoiceFieldsController Test Case
 */
class MultipleChoiceFieldsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.multiple_choice_fields',
        'app.questions',
        'app.questionnaires',
        'app.map_fields',
        'app.free_text_fields',
        'app.date_fields',
        'app.multiple_choice_options',
        'app.number_fields'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
