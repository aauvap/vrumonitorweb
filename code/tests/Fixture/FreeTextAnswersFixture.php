<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FreeTextAnswersFixture
 *
 */
class FreeTextAnswersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'incident_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'free_text_field_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'text' => ['type' => 'string', 'length' => 2000, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_free_text_answers_incidents1_idx' => ['type' => 'index', 'columns' => ['incident_id'], 'length' => []],
            'fk_free_text_answers_free_text_fields1_idx' => ['type' => 'index', 'columns' => ['free_text_field_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_free_text_answers_free_text_fields1' => ['type' => 'foreign', 'columns' => ['free_text_field_id'], 'references' => ['free_text_fields', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'fk_free_text_answers_incidents1' => ['type' => 'foreign', 'columns' => ['incident_id'], 'references' => ['incidents', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'incident_id' => 1,
            'free_text_field_id' => 1,
            'text' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-09-23 09:06:04',
            'modified' => '2015-09-23 09:06:04'
        ],
    ];
}
