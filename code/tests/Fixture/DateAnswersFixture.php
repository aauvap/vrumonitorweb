<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DateAnswersFixture
 *
 */
class DateAnswersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'incident_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'date_field_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'date' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_date_answers_incidents1_idx' => ['type' => 'index', 'columns' => ['incident_id'], 'length' => []],
            'fk_date_answers_date_fields1_idx' => ['type' => 'index', 'columns' => ['date_field_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_date_answers_date_fields1' => ['type' => 'foreign', 'columns' => ['date_field_id'], 'references' => ['date_fields', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'fk_date_answers_incidents1' => ['type' => 'foreign', 'columns' => ['incident_id'], 'references' => ['incidents', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'incident_id' => 1,
            'date_field_id' => 1,
            'date' => '2015-09-23',
            'created' => '2015-09-23 09:06:04',
            'modified' => '2015-09-23 09:06:04'
        ],
    ];
}
