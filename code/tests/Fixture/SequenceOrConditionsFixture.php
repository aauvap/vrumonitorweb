<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SequenceOrConditionsFixture
 *
 */
class SequenceOrConditionsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'question_sequence_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'multiple_choice_option_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_sequence_or_conditions_question_sequence1_idx' => ['type' => 'index', 'columns' => ['question_sequence_id'], 'length' => []],
            'fk_sequence_or_conditions_multiple_choice_options1_idx' => ['type' => 'index', 'columns' => ['multiple_choice_option_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id', 'multiple_choice_option_id'], 'length' => []],
            'fk_sequence_or_conditions_multiple_choice_options1' => ['type' => 'foreign', 'columns' => ['multiple_choice_option_id'], 'references' => ['multiple_choice_options', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'fk_sequence_or_conditions_question_sequence1' => ['type' => 'foreign', 'columns' => ['question_sequence_id'], 'references' => ['question_sequences', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'question_sequence_id' => 1,
            'multiple_choice_option_id' => 1,
            'created' => '2015-09-23 09:06:04',
            'modified' => '2015-09-23 09:06:04'
        ],
    ];
}
